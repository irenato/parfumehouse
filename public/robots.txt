User-agent: *

Disallow: /register

Disallow: /login

Disallow: /admin/

Disallow: /user/

Disallow: /order

Disallow: /search
Disallow: *?filter_*
Disallow: *?sort=*
Disallow: *?page=*
Crawl-delay: 10
Host: parfumhouse.com.ua

Sitemap: http://parfumhouse.com.ua/sitemap.xml
