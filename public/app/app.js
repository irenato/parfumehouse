// God save the Dev
'use strict';

if (process.env.NODE_ENV !== 'production') {
  require('./assets/templates/layouts/index.html');
}

// Depends
var $ = require('jquery');
require('bootstrap');

// Modules
var Forms = require('_modules/forms');
var Slider = require('_modules/slider');
var Popup = require('_modules/popup');
var Fancy_select = require('_modules/fancyselect');
// var LightGallery = require('_modules/lightgallery');
var Jslider = require('_modules/jslider');
var Fancybox = require('_modules/fancybox');
var Chosen = require('_modules/chosen');

// Stylesheet entrypoint

require('_stylesheets/app.scss');

// Are you ready?
$(function() {
  new Forms();
  new Popup();
  new Fancy_select();
  new Jscrollpane();
  // new LightGallery();
  new Slider();
  new Jslider();
  new Fancybox();
  new Chosen();

  // главный слайдер
  setTimeout(function() {
    $('.main-slider').not('.slick-initialized').slick({
      speed: 700,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: 'linear',
      fade: true,
      arrows: true,
      nextArrow: '<div class="arrow-right icon-arr"></div>',
      prevArrow: '<div class="arrow-left icon-arr"></div>'
    });
  }, 1000);

  // слайдер новинки

  $('.new-items').not('.slick-initialized').slick({
    speed: 700,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '<div class="arrow-right icon-arr"></div>',
    prevArrow: '<div class="arrow-left icon-arr"></div>',
    responsive: [
      {
        breakpoint: 1680,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.tab-items').not('.slick-initialized').slick({
    speed: 700,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '<div class="arrow-right icon-arr"></div>',
    prevArrow: '<div class="arrow-left icon-arr"></div>',
    responsive: [
      {
        breakpoint: 1680,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  // слайдер объем

  $('.volume-slider').not('.slick-initialized').slick({
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '<div class="arrow-right icon-arr"></div>',
    prevArrow: '<div class="arrow-left icon-arr"></div>'
  });

  // слайдер статьи

  $('.articles-slider').not('.slick-initialized').slick({
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '60px',
    arrows: true,
    nextArrow: '<div class="arrow-right icon-arr"></div>',
    prevArrow: '<div class="arrow-left icon-arr"></div>'
  });

  // Прокрутка к якорю
  $('.go_to').each(function() {
    var $this = $(this);
    $this.click(function() {
      var scroll_el = $($this.data('destination'));
      if ($(scroll_el).length != 0) {
        $('html, body').animate({
          scrollTop: $(scroll_el).offset().top - 65
        }, 500);
      }
      return false;
    });
  });

  /* табы */
  (function($) {
    $(function() {
      $('ul.menu-tabs_caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active');
      });
    });
  })(jQuery);

  /* скроллинг */

  jQuery(document).ready(function($) {
    $('a[href^="#"]').bind('click.smoothscroll', function(e) {
      e.preventDefault();
      var target = this.hash;
      var $target = $(target);

      $('html, body').stop().animate( {
        'scrollTop': $target.offset().top - 65
      }, 1000, 'swing', function() {
        // window.location.hash = target;
      } );
    } );
  } );

  $('.up').click(function() {
    $(window).animate({ scrollTop: 0 }, 'slow');
  });

  (function($) {
    $(function() {
      $('ul.product-menu').on('click', 'li:not(.active)', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('main.main-container').find('div.product-content').removeClass('active').eq($(this).index()).addClass('active');
      });
    });
  })(jQuery);

  $('.bottles-tab:not(.active)').one('click', function() {
    $('.volume-slider').slick('destroy');
    setTimeout(function() {
      $('.tab-items').slick('reinit');
    }, 10);
    setTimeout(function() {
      $('.volume-slider').slick({
        speed: 700,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        nextArrow: '<div class="arrow-right icon-arr"></div>',
        prevArrow: '<div class="arrow-left icon-arr"></div>'
      });
    }, 50);
  });

  /* фиксация меню */

  window.onscroll = function() {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (($(window).width() <= 1920) && (scrolled > 788)) {
      $('.menu-tabs').addClass('fixed');
      $('.up').show('slow');
    }
    else if (($(window).width() < 1200) && (scrolled > 887)) {
      $('.menu-tabs').addClass('fixed');
      $('.up').show('slow');
    }
    else if (($(window).width() < 991)) {
      $('.menu-tabs').removeClass('fixed');
      $('.up').hide('slow');
    }
    else {
      $('.menu-tabs').removeClass('fixed');
      $('.up').hide('slow');
    }
  };

  /* изменение значка меню */
  var toggles = document.querySelectorAll('.toggle-switch');

  for (var i = toggles.length - 1; i >= 0; i--) {
    var toggle = toggles[i];
    toggleHandler(toggle);
  }

  function toggleHandler(toggle) {
    toggle.addEventListener( 'click', function(e) {
      e.preventDefault();
      (this.classList.contains('active') === true) ? this.classList.remove('active') : this.classList.add('active');
    });
  }

  /* изменение значка поиска */
  var toggles_search = document.querySelectorAll('.search-btn');

  for (var i = toggles_search.length - 1; i >= 0; i--) {
    var toggle_s = toggles_search[i];
    toggleHandler(toggle_s);
  }

  function toggleHandler(toggle_s) {
    toggle_s.addEventListener( 'click', function(e) {
      e.preventDefault();
      (this.classList.contains('active') === true) ? this.classList.remove('active') : this.classList.add('active');
    });
  }

  /* выдвижное меню - стартовые пакеты */

  $('.packages-menu__btn').click(function(e) {
    if ($(this).hasClass('active')) {
      $('.packages-menu').addClass('active');
      $('.main-container').addClass('active');
      $('.main-footer').addClass('active');
      $('.main-search').removeClass('active');
      $('.search-btn__main').removeClass('active');
    }
    else {
      $('.packages-menu').removeClass('active');
      $('.main-search').removeClass('active');
      $('.main-container').removeClass('active');
      $('.main-footer').removeClass('active');
      $('.search-btn__main').removeClass('active');
    }
  });

  /* выдвижное меню на главной */

  $('.main-menu__btn').click(function(e) {
    if ($(this).hasClass('active')) {
      $('.main-menu').addClass('active');
      $('.main-container').addClass('active');
      $('.main-footer').addClass('active');
      $('.main-search').removeClass('active');
      $('.search-btn__main').removeClass('active');
    }
    else {
      $('.main-menu').removeClass('active');
      $('.main-search').removeClass('active');
      $('.main-container').removeClass('active');
      $('.main-footer').removeClass('active');
      $('.search-btn__main').removeClass('active');
    }
  });

  /* поиск */

  $('.search-btn__main').click(function(e) {
    if ($(this).hasClass('active')) {
      $('.main-search').addClass('active');
      $('.main-container').addClass('active');
      $('.main-footer').addClass('active');
      $('.main-menu').removeClass('active');
      $('.main-menu__btn').removeClass('active');
      $('.packages-menu').removeClass('active');
    }
    else {
      $('.main-search').removeClass('active');
      $('.main-menu').removeClass('active');
      $('.main-menu__btn').removeClass('active');
      $('.main-container').removeClass('active');
      $('.main-footer').removeClass('active');
      $('.packages-menu').removeClass('active');
    }
  });

  /* корзина */

  $('.cart-wrapper').click(function(e) {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('.main-cart').removeClass('active');
      $('header').css('filter', 'none');
      $('main').css('filter', 'none');
      $('footer').css('filter', 'none');
    }
    else {
      $(this).addClass('active');
      $('.main-cart').addClass('active');
      $('header').css('filter', 'blur(4px)');
      $('main').css('filter', 'blur(4px)');
      $('footer').css('filter', 'blur(4px)');
    }
  });

  $('.main-cart__close').click(function(e) {
    if ($(this).hasClass('active')) {
      $(this).addClass('active');
      $('.main-cart').addClass('active');
      $('header').css('filter', 'blur(4px)');
      $('main').css('filter', 'blur(4px)');
      $('footer').css('filter', 'blur(4px)');
      $('.cart-wrapper').addClass('active');
    }
    else {
      $(this).removeClass('active');
      $('.main-cart').removeClass('active');
      $('header').css('filter', 'none');
      $('main').css('filter', 'none');
      $('footer').css('filter', 'none');
      $('.cart-wrapper').removeClass('active');
    }
  });

  $(document).click(function() {
    if ($('.main-cart').hasClass('active')) {
      $('.cart-wrapper').removeClass('active');
      if ($('.cart-wrapper.active').length < 1) {
        $('.main-cart').removeClass('active');
        $('header').css('filter', 'none');
        $('main').css('filter', 'none');
        $('footer').css('filter', 'none');
      }
    }
  });

  $('.cart-wrapper').click(function(e) {
    e.stopPropagation();
  });

  $('.main-cart').click(function(e) {
    e.stopPropagation();
  });

  /* удаление из корзины */

  if ($('.main-cart__order').length != 0) {
    $('.main-cart__orders-list').show();
  }
  else {
    $('.main-cart__orders-list').hide();
    $('.empty-cart').css('display', 'flex');
  }

  $('.main-cart__del-order').click(function() {
    $(this).parent('li').slideUp('slow').promise().done(function() {
      $(this).remove();
      if ($('.main-cart__order').length != 0) {
        $('.main-cart__orders-list').show();
      }
      else {
        $('.main-cart__orders-list').hide();
        $('.empty-cart').css('display', 'flex');
      }
    });
  });

  // размытие фона
  if ($('.popup-btn').length > 0) {
    $('.popup-btn').magnificPopup({
      callbacks: {
        open: function() {
          $('header').css('filter', 'blur(4px)');
          $('main').css('filter', 'blur(4px)');
          $('footer').css('filter', 'blur(4px)');
          $.magnificPopup.instance.close = function() {
            $('header').css('filter', 'none');
            $('main').css('filter', 'none');
            $('footer').css('filter', 'none');
            $('.log-reg-wrapper').removeAttr('style');
            $('.regisration-wrapper').removeAttr('style');
            $.magnificPopup.proto.close.call(this);
          };
        }
      }
    });
  }

  /* табы личный кабинет */

  (function($) {
    $(function() {
      $('ul.user-room__tabs-list').on('click', 'li:not(.active)', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('section.user-room').find('div.user-room__tabs-content').removeClass('active').eq($(this).index()).addClass('active');
      });
    });
  })(jQuery);


  /* изменение личных данных */

  $('.change-data__btn').click(function() {
    if ($('.user-room__input').is(':disabled')) {
      $('.user-room__input').removeAttr('disabled');
      $('.user-room__select').removeAttr('disabled');
      $('.change-data__btn').text('Сохранить');
    }
    else {
      $('.user-room__input').attr('disabled', 'disabled');
      $('.user-room__select').attr('disabled', 'disabled');
      $('.change-data__btn').text('Изменить личные данные');
    }
  });

  /* удаление из истории заказов */

  if ($('.user-room__order-history-items').length != 0) {
    $('.user-room__order-history').show();
  }
  else {
    $('.user-room__order-history').hide();
    $('.user-room__order-history__empty').css('display', 'flex');
  }

  $('.user-room__del-order').click(function() {
    $(this).parent('li').slideUp('slow').promise().done(function() {
      $(this).remove();
      if ($('.user-room__order-history-items').length != 0) {
        $('.user-room__order-history').show();
      }
      else {
        $('.user-room__order-history').hide();
        $('.user-room__order-history__empty').css('display', 'flex');
      }
    });
  });

  /* карта */

  $('#Map area').hover(function() {
    var cls = $(this).data('class');
    if (typeof cls !== 'undefined') {
      $('#mapdiv > #map-cont').removeAttr('class').addClass(cls);
    }
  });
  $('#Map area').click(function(e) {
    e.preventDefault();
    var cls = $(this).data('class');
    if (typeof cls !== 'undefined') {
      $('#mapdiv > #map-cont').removeAttr('class').addClass(cls);
      $('#filia').html('Региональный менеджер в ' + $(this).data('name') + '<br>' + $(this).data('manager') + '<br>' + 'тел. +38 (067) 514-18-18, +38 (050) 364-23-80');
    }
  });

  /* табы вакансии */

  (function($) {
    $(function() {
      $('ul.vacancies-list').on('click', 'li:not(.active)', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('nav.vacancies-tabs').find('div.vacancies-content').removeClass('active').eq($(this).index()).addClass('active');
      });
    });
  })(jQuery);

  /* переключение логин/регистрация */

  $('.reg-btn').click(function() {
    $('.regisration-wrapper').slideDown('slow').promise().done(function() {
      $(this).show();
    });
    $('.log-reg-wrapper').slideDown('slow').promise().done(function() {
      $(this).hide();
    });
  });
});

