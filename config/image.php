<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    /**
     *  Размеры изображений
     */
    'sizes' => [
        'product_list' => [
            'description' => 'Размер изображения товара в категории',
            'width' => 254,
            'height' => 301
        ],
        'product' => [
            'description' => 'Размер главного изображения в карточке товара',
            'width' => 490,
            'height' => 570
        ],
        'blog_list' => [
            'description' => 'Размер изображения блога',
            'width' => 555,
            'height' => 182
        ],
        'blog' => [
            'description' => 'Размер изображения блога',
            'width' => 1140,
            'height' => 424
        ],
        'slide' => [
            'description' => 'Размер изображения слайда',
            'width' => 1170,
            'height' => 383
        ]
    ],

    /**
     *  Типы изображений
     */
    'types' => [
        'default' => [
            'description' => 'Тип по умолчанию',
        ],
        'product' => [
            'description' => 'Изображение для продукта',
            'sizes' => [
                'product',
                'product_list'
            ]
        ],
        'post' => [
            'description' => 'Изображение для постов',
            'sizes' => [
                'blog_list',
                'blog'
            ]
        ],
        'slide' => [
            'description' => 'Изображение для слайда',
            'sizes' => [
                'slide'
            ]
        ]
    ]

);
