<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = 'orders';
    protected $fillable = [
        'user_id',
        'products_sum',
        'products_quantity',
        'status_id',
        'order_data'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->hasMany('App\ProductsInOrder','order_id','id');
    }

    public function status()
    {
        return $this->belongsTo('App\OrderStatus', 'status_id');
    }

}
