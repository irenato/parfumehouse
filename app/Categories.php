<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Products;

class Categories extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'url_alias',
        'related_attribute_id',
        'parent_id',
        'sort_order',
        'status'
    ];

    protected $dates = ['deleted_at'];

    protected $table = 'categories';

    public function products()
    {
        return $this->hasMany('App\Products', 'product_category_id', 'id');
    }

    public function related_attributes()
    {
        return $this->hasMany('App\AttributeValues', 'attribute_id', 'related_attribute_id');
    }

    public function get_products($category_id, $filter, $capacity = [], $sort, $take = false)
    {

        if ($sort == 'popularity') {
            $orderBy = 'rating';
            $route = 'desc';
        } elseif ($sort == 'date') {
            $orderBy = 'updated_at';
            $route = 'desc';
        } elseif ($sort == 'name') {
            $orderBy = 'name';
            $route = 'asc';
        }

        $products = Products::select('products.*');

        if ($category_id == 'bestsellers') {
            $products->join('module_bestsellers', 'products.id', '=', 'module_bestsellers.product_id');
        } elseif ($category_id == 'new') {
            $products->join('module_new_products', 'products.id', '=', 'module_new_products.product_id');
        } elseif($category_id == 'sale') {
            $products->whereNotNull('sale');
        } elseif ($category_id) {
            $products->where('product_category_id', $category_id);
			$products->orderBy('label', 'desc');
        }

        if (!empty($filter)) {

            foreach ($filter as $key => $attribute) {

                $products->join('product_attributes AS attr' . $key, 'products.id', '=', 'attr' . $key . '.product_id');
                $products->where('stock', 1);
                $products->where('attr' . $key . '.attribute_id', $key);
                $products->where(function($query) use($attribute, $key){

                    foreach ($attribute as $attribute_value) {
                        $query->orWhere('attr' . $key . '.attribute_value_id', $attribute_value);
                    }
                });

            }
        }

        if(!empty($capacity)){
            $products->whereIn('capacity', $capacity);
        }

        $products->orderBy($orderBy, $route);
        $products->groupBy('products.id');

        if (!$take) {
            return $products->paginate(18);
        } else {
            return $products->take($take)->inRandomOrder()->get();
        }

    }

    public function get_products_count($category_id, $filter, $capacity = []){

        $products = Products::select('products.*');

        if ($category_id == 'bestsellers') {
            $products->join('module_bestsellers', 'products.id', '=', 'module_bestsellers.product_id');
        } elseif ($category_id == 'new') {
            $products->join('module_new_products', 'products.id', '=', 'module_new_products.product_id');
        }elseif ($category_id) {
            $products->where('product_category_id', $category_id);
        }

        if (!empty($filter)) {

            foreach ($filter as $key => $attribute) {

                $products->join('product_attributes AS attr' . $key, 'products.id', '=', 'attr' . $key . '.product_id');
                $products->where('stock', 1);
                $products->where('attr' . $key . '.attribute_id', $key);
                $products->where(function($query) use($attribute, $key){

                    foreach ($attribute as $attribute_value) {
                        $query->orWhere('attr' . $key . '.attribute_value_id', $attribute_value);
                    }
                });

            }
        }

        if(!empty($capacity)){
            $products->whereIn('capacity', $capacity);
        }

        return $products->count();
    }

    /**
     * Список объёмов
     *
     * @param $category_id
     * @return int
     */
    public function get_all_capacities($category_id){
        $product = Products::select('products.capacity');
        $result = $product->where('product_category_id', $category_id)
            ->orderBy('products.capacity', 'asc')
            ->groupBy('products.capacity')
            ->get();

        return $result;
    }
}
