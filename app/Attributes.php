<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attributes extends Model
{
    protected $fillable = [
        'name',
        'enable_image_overlay',
        'image_overlay_settings'
    ];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'attributes';

    public function values()
    {
        return $this->hasMany('App\AttributeValues', 'attribute_id');
    }

    public function get_current_product_attributes($category_id, $take = 6) {

        if ($category_id == 'bestsellers') {
            $products = Products::select('products.id')->join('module_bestsellers', 'products.id', '=', 'module_bestsellers.product_id')->get();
        } elseif ($category_id == 'new') {
            $products = Products::select('products.id')->join('module_new_products', 'products.id', '=', 'module_new_products.product_id')->get();
        } elseif($category_id == 'sale') {
            $products = Products::select('products.id')->whereNotNull('sale')->get();
        } else {
            $products = Products::select('products.id')->where('product_category_id', $category_id)->get();
        }

        $product_attributes = ProductAttributes::select('attribute_id')->whereIn('product_id', $products)->distinct()->get();
        return $this->whereIn('id', $product_attributes)->get();
    }

}
