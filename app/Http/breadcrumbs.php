<?php

/**
 * Home
 */
Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('Главная', url('/'));
});

/**
 * User
 */
Breadcrumbs::register('user', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Личный кабинет', url('/user'));
    $breadcrumbs->push('Личные данные');
});

Breadcrumbs::register('history', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Личный кабинет', url('/user'));
    $breadcrumbs->push('История заказов');
});

Breadcrumbs::register('wishlist', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Личный кабинет', url('/user'));
    $breadcrumbs->push('Список желаний');
});

Breadcrumbs::register('change_user', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Личный кабинет', url('/user'));
    $breadcrumbs->push('Изменение личных данных');
});

/**
 * Categories
 */
Breadcrumbs::register('categories', function($breadcrumbs, $category) {
    $breadcrumbs->parent('home');
    if (is_object($category)) {
        $name = $category->name;
        $alias = $category->url_alias;
    } elseif (is_array($category)) {
        $name = $category['name'];
        $alias = $category['url_alias'];
    }
    $breadcrumbs->push($name, url('/categories/' . $alias));
});

/**
 * Articles
 */
Breadcrumbs::register('articles', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Статьи', url('/articles'));
});

Breadcrumbs::register('articles_item', function($breadcrumbs, $article) {
    $breadcrumbs->parent('articles');
    $breadcrumbs->push($article->title);
});

/**
 * HTML Pages
 */
Breadcrumbs::register('html', function($breadcrumbs, $page) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push($page->name);
});

/**
 * Login and register
 */
Breadcrumbs::register('login', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Авторизация', url('/login'));
});

Breadcrumbs::register('register', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Регистрация');
});

Breadcrumbs::register('forgotten', function($breadcrumbs) {
    $breadcrumbs->parent('login');
    $breadcrumbs->push('Восстановление пароля');
});

/**
 * Products
 */
Breadcrumbs::register('product', function($breadcrumbs, $product, $category) {
    $breadcrumbs->parent('categories', $category);
    $breadcrumbs->push($product->name);
});

/**
 * Search
 */

Breadcrumbs::register('search', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Поиск', url('/search'));
});