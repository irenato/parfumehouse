<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Mail;
use App\Settings;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Order;
use App\Products;
use App\Modules;
use App\OrderStatus;
use App\ProductsInOrder;
use App\User;
use App\UserData;
use App\Cart;
use App\Newpost;
use App\Http\Controllers\Liqpay;
use App\ProductsCart;
use Carbon\Carbon;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{

    public function index()
    {
        $orders = Order::orderBy('id', 'desc')->get();
        return view('admin.orders.index')->with('orders', $orders)->with('title', 'Список Заказов');
    }

    public function edit($id)
    {
        $about_array = [
            'registration' => 'Пользователь зарегистрирован',
            'city' => 'Город',
            'delivery' => 'Доставка',
            'department' => 'Отделение Новой Почты',
            'street' => 'Улица',
            'house' => 'Дом',
            'flat' => 'Квартира',
            'payment' => 'Способ оплаты',
            'comment' => 'Комментарий',
        ];

        $order = Order::find($id);
        $order->date = $order->updated_at->format('d.m.Y');
        $order->time = $order->updated_at->format('H:i');
        $order_info = json_decode($order->order_data, true);

        $settings = Settings::find(1);

        return view('admin.orders.edit')
            ->with('order', $order)
            ->with('order_info', $order_info)
            ->with('orders_statuses', OrderStatus::all())
            ->with('about_order', $about_array)
            ->with('rate', $settings->rate);
    }

    public function show(Newpost $newpost)
    {
        if(Session::has('user_cart_id')) {
            $user_cart_id = Session::get('user_cart_id');
            $current_cart = Cart::where('user_cart_id', $user_cart_id)->first();
            if(is_null($current_cart)){
                return redirect('/');
            }

            $settings = Settings::find(1);

//            if($current_cart->products_sum < 30){
//                return redirect('/');
//            }
        }

        $regions = $newpost->getRegions();

        $info = [];

        if(isset($_COOKIE['user_info'])) {
            foreach (explode('&', $_COOKIE['user_info']) as $param) {
                $r = explode('=', $param);
                if ($r[0] != '_token' && count($r) == 2) {
                    $info[$r[0]] = urldecode($r[1]);
                }
            }
        }

        if(!empty($info['region'])){
            $region = $newpost->getRegionRef($info['region']);
            $cities = $newpost->getCities($region->region_id);
        }else{
            $cities = [];
        }

        if(!empty($info['city'])){
            $city = $newpost->getCityRef($info['city']);
            $warehouses = $newpost->getWarehouses($city->city_id);
        }else{
            $warehouses = [];
        }

        return view('public.order', ['regions' => $regions, 'cities' => $cities, 'warehouses' => $warehouses, 'info' => $info]);
    }

    public function update($id, Request $request)
    {
        $status_id = $request->status;
        $order = Order::find($id)->update([
            'status_id' => $status_id
        ]);
//        return dd($order);
        $orders = Order::all();
        return redirect('/admin/orders')
            ->with('message-success', 'Заказ № ' . $id . ' успешно обновлен.');
    }

    /**
     * Загрузка списка городов Новой Почты
     *
     * @param Request $request
     * @param Newpost $newpost
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCities(Request $request, Newpost $newpost)
    {
        $region = $newpost->getRegionRef($request->region_id);

        if ($region) {
            $cities = $newpost->getCities($region->region_id);
        } else {
            return response()->json(['error' => 'При загрузке городов произошла ошибка. Пожалуйста, попробуйте еще раз!']);
        }

        if ($cities) {
            return response()->json(['success' => $cities]);
        } else {
            return response()->json(['error' => 'При загрузке городов произошла ошибка. Пожалуйста, попробуйте еще раз!']);
        }
    }

    /**
     * Загрузка списка отделений Новой Почты
     *
     * @param Request $request
     * @param Newpost $newpost
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWarehouses(Request $request, Newpost $newpost)
    {
        $city = $newpost->getCityRef($request->city_id);

        if (!is_null($city)) {
            $warehouses = $newpost->getWarehouses($city->city_id);
        } else {
            return response()->json(['error' => 'При загрузке отделений произошла ошибка. Пожалуйста, попробуйте еще раз!']);
        }

        if ($warehouses) {
            return response()->json(['success' => $warehouses]);
        } else {
            return response()->json(['error' => 'При загрузке отделений произошла ошибка. Пожалуйста, попробуйте еще раз!']);
        }
    }

    public function createOrder(Request $request, UserData $user_data, User $users)
    {
        $rules = [
            'first_name' => 'required',
            'phone'     => 'required|regex:/^[0-9\-! ,\'\"\/+@\.:\(\)]+$/',
        ];

        $messages = [
            'first_name.required' => 'Вы не указали имя!',
            'phone.required'    => 'Вы не указали телефон!',
            'phone.regex'       => 'Некорректный номер телефона!',
        ];

        if (isset($request->registration) && $request->registration == 'on') {
            $rules['email'] = 'required|email';
            $rules['password']  = 'required|min:6|confirmed';
            $rules['password_confirmation'] = 'min:6';

            $messages['password.required'] = 'Вы не указали пароль!';
            $messages['password.min'] = 'Пароль должен быть не менее 6 символов!';
            $messages['password.confirmed'] = 'Введенные пароли не совпадают!';
            $messages['email.required'] = 'Вы не указали e-mail!';
            $messages['email.email'] = 'Некорректный email-адрес!';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return response()->json(['error' => $validator->messages()]);
        }

        if (isset($request->registration) && $request->registration == 'on') {
            $register = new LoginController();

            $response = $register->store($request, $user_data);

            if ($response == 'error') {
                return response()->json(['error' => 'При регистрации произошла ошибка. Попробуйте оформить заказ без регистрации.']);
            } elseif ($response == 'email error') {
                return response()->json(['error' => ['email' => 'Пользователь с таким e-mail адресом уже зарегистрирован!']]);
            }
        }

        $user = Sentinel::check();

        if (!$user) {
            $existed_user = $users->checkIfUnregistered($request->phone);
            if(!is_null($existed_user)) {
                $user = $existed_user;
            } else {
                $register = new LoginController();
                $user = $register->storeAsUnregistered($request);
            }
        }

        $user_cart_id = Session::get('user_cart_id');
        $current_cart = Cart::where('user_cart_id', $user_cart_id)->first();

        $new_order_data = [
            'user_id' => $user->id,
            'products_sum' => $current_cart->products_sum,
            'products_quantity' => $current_cart->products_quantity,
            'status_id' => 0,
            'order_data' => json_encode([
                'user_name' => $request->first_name,
                'phone'     => $request->phone
            ])
        ];

        $order_id = !is_null(Cookie::get('order_id')) ? Cookie::get('order_id') : $request->order_id;

        if (is_null($order_id)) {
            $order = Order::create($new_order_data);
            Cookie::queue(Cookie::forever('order_id', $order->id));

        } else {
            $order = Order::find($order_id);
            $order->update($new_order_data);
        }

        $this->updateOrderProducts($order->id, $current_cart);
        return response()->json(['success' => $order->id]);

    }

    public function orderConfirmation(Request $request, Newpost $newpost)
    {
        $errors = [];

        $rules = [
            'delivery' => 'required',
            'payment' => 'required',
        ];

        if ($request->delivery == 'courier') {
            $rules['street'] = 'required';
            $rules['house'] = 'required';
        } elseif ($request->delivery == 'newpost') {
            $rules['region'] = 'required|not_in:0';
            $rules['city'] = 'required|not_in:0';
            $rules['department'] = 'required|not_in:0';
        } else {
            $errors[]['delivery'] = 'Не выбран способ доставки!';
        }

        $messages = [
            'delivery.required'     => 'Не выбран способ доставки!',
            'payment.required'      => 'Не выбран способ оплаты!',
            'street.required'       => 'Укажите улицу!',
            'house.required'        => 'Укажите № дома!',
            'region.required'       => 'Не выбрана область!',
            'city.required'         => 'Не выбран город!',
            'department.required'   => 'Не выбрано отделение НП!',
            'department.not_in'     => 'Не выбрано отделение НП!',
            'city.not_in'           => 'Не выбран город!',
            'region.not_in'         => 'Не выбрана область!',
        ];

        if ($request->payment != 'card' && $request->payment != 'cash')
            $errors[]['payment'] = 'Не выбран способ оплаты!';

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails() || !empty($errors)){
            return response()->json(['error' => $validator->messages(), 'custom_error' => $errors]);
        }

        $delivery = [];
        if($request->delivery == 'courier') {
            $delivery = [
                'method' => 'Доставка курьером по Харькову',
                'street' => $request->street,
                'house'  => $request->house,
                'flat'   => $request->flat
            ];
        } elseif ($request->delivery == 'newpost') {
            $delivery = [
                'method'    => 'Доставка Новой Почтой',
                'region'    => $newpost->getRegionRef($request->region)->name,
                'city'      => $newpost->getCityRef($request->city)->name_ru,
                'department' => $newpost->getWarehouse($request->department)->address_ru
            ];
        }
        $payment = '';
        if ($request->payment == 'card') {
            $payment = 'Онлайн-оплата картой';
        } elseif ($request->payment == 'cash') {
            $payment = 'Оплата наличными при получении';
        }

        $order_id = !is_null(Cookie::get('order_id')) ? Cookie::get('order_id') : $request->order_id;
        if (is_null($order_id))
            return response()->json(['error' => ['checkout' => 'Не удалось оформить заказ!']]);

        $order = Order::find($order_id);

        $order_data = json_decode($order->order_data, true);
        $order_data['delivery'] = $delivery;
        $order_data['payment'] = $payment;
        $order_data['comment'] = $request->comment;

        $user_cart_id = Session::get('user_cart_id');
        $current_cart = Cart::where('user_cart_id', $user_cart_id)->first();

        $this->updateOrderProducts($order->id, $current_cart);

        $order->update([
            'order_data' => json_encode($order_data),
            'products_sum' => $current_cart->products_sum,
            'products_quantity' => $current_cart->products_quantity,
            'created_at' => Carbon::now()
        ]);

        $rate = Settings::find(1)->value('rate');
        if ($request->payment == 'card') {
            $public_key = config('liqpay.public_key');
            $private_key = config('liqpay.private_key');
            $liqpay = new LiqPay($public_key, $private_key);
            $checkout = $liqpay->cnb_form([
                'action'    => 'pay',
                'amount'    => $current_cart->products_sum * $rate,
                'currency'  => 'UAH',
                'description'   => 'Оплата заказа №' . $order->id . ' на сайте Parfum House',
                'order_id'  => $order->id,
                'sandbox'   => 1,
                'version'   => 3,
                'result_url' => url('/checkout/complete?order_id=' . $order->id)
            ]);

            return response()->json(['success' => 'liqpay', 'liqpay' => $checkout]);
        } else if ($request->payment == 'cash') {
            return response()->json(['success' => 'redirect', 'order_id' => $order->id]);
        }
    }

    public function liqpayCallback(Request $request){
        $incoming = json_decode(base64_decode($request->data), true);
        $public_key = config('liqpay.public_key');
        $private_key = config('liqpay.private_key');

        if (isset($incoming['err_code'])) {
            Log::info('Liqpay returned an error', ['code' => $incoming['err_code'], 'description' => $incoming['err_description']]);
            return;
        }

        if ($incoming['public_key'] == $public_key) {
            $liqpay = new LiqPay($public_key, $private_key);
            $status_id = $liqpay->returnOrderStatus($incoming['status']);
            Order::find($incoming['order_id'])->update(['status_id' => $status_id]);
        }

    }

    public function updateOrderProducts($order_id, $current_cart){
        ProductsInOrder::where('order_id', $order_id)->delete();
        foreach ($current_cart->products_cart as $products) {
            $products_in_cart['product_id'] = $products->product->id;
            $products_in_cart['product_quantity'] = $products->product_quantity;

            if ($products->product->sale) {
                $sale = ($products->product->price / 100) * $products->product->sale;
                $product_price = $products->product->price - $sale;
            } else {
                $product_price = $products->product->price;
            }

            $products_in_cart['product_sum'] = $products->product_quantity * $product_price;

            $new_products_cart = new ProductsInOrder($products_in_cart);
            Order::find($order_id)->products()->save($new_products_cart);
        }
    }

    public function orderComplete(Request $request)
    {
        $order_id = !is_null(Cookie::get('order_id')) ? Cookie::get('order_id') : $request->order_id;
        $order = Order::find($order_id);

        if($order->status_id)
            return redirect('/');

        $order_data = json_decode($order->order_data, true);

        if ($order_data['payment'] == 'Онлайн-оплата картой') {
            $order->update(['status_id' => 12]);
        } else {
            $order->update(['status_id' => 1]);
        }

        $user_cart_id = Session::get('user_cart_id');
        $current_cart = Cart::where('user_cart_id', $user_cart_id)->first();
        $current_cart->delete();

        Cookie::queue(Cookie::forget('order_id'));

        $new_user_cart_id = md5(rand(0,100500));
        Session::put('user_cart_id', $new_user_cart_id);

        $settings = Settings::find(1);
        $emails = json_decode($settings['notify_emails']);

        Mail::send('emails.order', ['data' => json_decode($order->order_data, true), 'order' => $order, 'rate' => $settings->rate], function($msg) use ($emails){
            $msg->from('parfumhouse@parfumhouse.com.ua', 'Интернет-магазин Parfum House');
            $msg->to($emails);
            $msg->subject('Новый заказ');
        });

        $user_data = User::find($order->user_id);

        if ($order_data['delivery']['method'] == 'Доставка Новой Почтой') {
            $address = 'Область: ' . $order_data['delivery']['region'];
            $address .= '; Город: ' . $order_data['delivery']['city'];
            $address .= '; Отделение: ' . $order_data['delivery']['department'];
        } else {
            $address = 'Улица: ' . $order_data['delivery']['street'];
            $address .= '; № дома: ' . $order_data['delivery']['house'];
            if (isset($order_data['delivery']['flat'])) { $address .= '; № квартиры: ' . $order_data['delivery']['flat']; }
        }

        /*
         * Экспорт в битрикс24
         */
        $products_data = '';
        foreach($order->products as $product){
            foreach ($product->product->attributes as $atributes) {
                if ($atributes->attribute_id == 1) {
                    $value = $atributes->value->name;
                }
            }
            $products_data .= 'Артикул: ';
            $products_data .= isset($product->product->articul) ? $product->product->articul : 'не указан';
            $products_data .= ' Товар: ';
            $products_data .= isset($product->product->name) ? $product->product->name : 'не указан';
            $products_data .= ' Производитель: ';
            $products_data .= isset($value) ? $value : 'не указан';
            $products_data .= '. Количество: ';
            $products_data .= isset($product->product_quantity) ? $product->product_quantity : 'не указан';
            $products_data .= '. Объем: ';
            $products_data .= isset($product->product->capacity) ? $product->product->capacity : 'не указан';
            $products_data .= ' ';
            $products_data .= isset($product->product->measures->name) ? $product->product->measures->name : 'не указан';
            $products_data .= '. Сумма: ';
            $products_data .= isset($product->product_sum) ? $product->product_sum . '$' : '';
            $products_data .= '<br>';
        }
        $bitrix_data = [
            'phone' => $user_data->phone,
            'mail_skype' => isset($user_data->email) ? $user_data->email : 'null',
            'name' => $user_data->last_name.' '.$user_data->first_name,
            'content' => $user_data,
            'delivery' => $order_data['delivery']['method'],
            'address' => $address,
            'order' => 'Сумма: '. $order->products_sum .', количество товаров: '. $order->products_quantity,
            'products' => $products_data,
            'payment' => $order_data['payment']
        ];

        $export = new ExportDataController();
        $export->toBitrix($bitrix_data, 'checkout');

        $modules_settings = Modules::all();

        foreach ($modules_settings as $module_setting) {
            if ($module_setting->alias_name == 'latest') {
                $latest_settings = json_decode($module_setting->settings);
            }
        }
        $latest_products = Products::orderBy('created_at', 'desc')->take($latest_settings->quantity)->get();
        return view('public.thanks')->with('latest_products', $latest_products);
    }
}
