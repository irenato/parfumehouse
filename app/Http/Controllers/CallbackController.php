<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;
use Validator;
use Mail;
use App\User;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Carbon\Carbon;

use App\Http\Requests;

class CallbackController extends Controller
{
    public function index(Request $request)
    {
        $rules = [
            'name' => 'required',
            'phone'     => 'required|regex:/^[0-9\-! ,\'\"\/+@\.:\(\)]+$/',
        ];

        $messages = [
            'name.required'     => 'Вы не указали имя!',
            'phone.required'    => 'Вы не указали телефон!',
            'phone.regex'       => 'Некорректный номер телефона!',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return response()->json(['error' => $validator->messages()]);
        }
        $data = $request->only(['name', 'phone']);

        $emails = json_decode(Settings::find(1)->value('notify_emails'));
        Mail::send('emails.callback', ['data' => $data], function($msg) use ($emails){
            $msg->from('parfumhouse@parfumhouse.com.ua', 'Интернет-магазин Parfum House');
            $msg->to($emails);
            $msg->subject('Заказ обратного звонка');
        });

        $export = new ExportDataController();
        $export->toBitrix($data, 'callback');

        return response()->json(['success' => 'success']);
    }
	
	 public function quest(Request $request){
        $emails = json_decode(Settings::find(1)->value('notify_emails'));

        $user = Sentinel::getUser();

        if(!empty($user)) {
            Mail::send('emails.quest', ['data' => $request->all(), 'user' => $user], function ($msg) use ($emails) {
                $msg->from('parfumhouse@parfumhouse.com.ua', 'Интернет-магазин Parfum House');
                $msg->to($emails);
                $msg->subject('Опрос');
            });

            User::where('id', $user->id)->update(['quest_time' => Carbon::now()]);
        }

    }
}
