<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Measures;
use Validator;
use App\Http\Requests;

class MeasuresController extends Controller
{

    protected $rules = [
        'name' => 'required|unique:measures',
    ];
    protected $messages = [
        'name.required' => 'Поле должно быть заполнено!',
        'name.unique' => 'Значение должно быть уникальным!',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.measures.index')
            ->with('measures', Measures::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.measures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Measures $measures)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $measures->fill($request->except('_token'));
        $measures->save();

        return redirect('/admin/measures')
            ->with('measures', $measures->paginate(10))
            ->with('message-success', 'Единица измерения ' . $measures->name . ' успешно добавлена.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.measures.edit')
            ->with('measure', Measures::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->rules;
        $rules['name'] = 'required|unique:measures,name,'.$id;

        $validator = Validator::make($request->all(), $rules, $this->messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $measure = Measures::find($id);
        $measure->fill($request->except('_token'));
        $measure->save();

        return redirect('/admin/measures')
            ->with('measures', $measure->paginate(10))
            ->with('message-success', 'Страница ' . $measure->name . ' успешно обновлена.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $measure = Measures::find($id);
        $measure->delete();

        return redirect('/admin/measures')
            ->with('products', $measure->paginate(10))
            ->with('message-success', 'Страница ' . $measure->name . ' успешно удалена.');
    }
}
