<?php

namespace App\Http\Controllers;

use App\Attributes;
use App\AttributeValues;
use App\Product;
use App\ProductDescription;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Object_;
use Validator;

use App\Http\Requests;
use App\Categories;
use App\Settings;
use App\Image;
use App\Measures;
use App\Products;
use App\Modules;
use App\ModuleRecommended;

class ProductsController extends Controller
{

    private $products;
    private $rules = [
        'name' => 'required|unique:products',
        'price' => 'required|numeric',
        'sale'  => 'numeric',
        //'articul' => 'required|unique:products',
        'quantity' => 'numeric',
        'capacity' => 'numeric',
        'product_category_id' => 'required|not_in:0',
        'meta_title' => 'required|max:75',
        'meta_description' => 'max:180',
        'meta_keywords' => 'max:180',
        'url_alias' => 'required|unique:products|unique:categories',
    ];

    private $messages = [
        'name.required' => 'Поле должно быть заполнено!',
        'name.unique' => 'Название товара должно быть уникальным!',
        'price.required' => 'Поле должно быть заполнено!',
        'price.numeric' => 'Значение должно быть числовым!',
        'sale.numeric' => 'Значение должно быть числовым!',
        'articul.required' => 'Поле должно быть заполнено!',
        'articul.unique' => 'Артикул товара должен быть уникальным!',
        'quantity.numeric' => 'Значение должно быть числовым!',
        'capacity.numeric' => 'Значение должно быть числовым!',
        'product_category_id.not_in' => 'Не выбрана категория товара!',
        'meta_title.required' => 'Поле должно быть заполнено!',
        'meta_title.max' => 'Максимальная длина заголовка не может превышать 75 символов!',
        'meta_description.max' => 'Максимальная длина описания не может превышать 180 символов!',
        'meta_keywords.max' => 'Максимальная длина ключевых слов не может превышать 180 символов!',
        'url_alias.required' => 'Поле должно быть заполнено!',
        'url_alias.unique' => 'Значение должно быть уникальным для каждой категории!',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Categories $categories)
    {

        $category_id = false;
        $stock = false;

        if (isset($request->category)) {
            $category_id = $request->category;
        }
        if (isset($request->stock)) {
            $stock = $request->stock;
        }

        $products = Products::when($category_id, function($query) use ($category_id){
            return $query->where('product_category_id', $category_id);
        })
            ->when(($stock !== false), function($query) use ($stock){
                return $query->where('stock', $stock);
            })
            ->orderBy('created_at', 'DESC')
            ->paginate(1000);


        return view('admin.products.index')
            ->with('products', $products)
            ->with('categories', $categories->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create')
            ->with('categories', Categories::all())
            ->with('attributes', Attributes::all())
			->with('labels', ['' => '-', 'new' => 'Новинка', 'exclusive' => 'Эксклюзивный аромат', 'exclusive_new' => 'Эксклюзивная новинка', '5_present' => 'Пятый флакон в подарок', '11_present' => '11-й флакон в подарок', 'flakon_present' => 'флакон в подарок', 'sprey_present' => 'Спрей в подарок', 'black_friday' => 'Чёрная пятница'])
            ->with('measures', Measures::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Image $image, Products $products, ProductDescription $description)
    {
        $attributes_error = $this->validate_attributes($request->product_attributes);

        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        $image_href = $image->find($request->original_image_id)->href;

        if($request->product_description_image_id) {
            $description_image_href = $image->find($request->product_description_image_id)->href;
        } else {
            $description_image_href = 'no_image.jpg';
        }

        $request->merge(['href' => $image_href, 'description_href' => $description_image_href]);

        if ($validator->fails() || $attributes_error) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator)
                ->with('attributes_error', $attributes_error);
        }


        $data = $request->all();
        $data['sale'] = $request->sale ? $request->sale : null;
        if (!empty($request->product_attributes)) {
            $data['product_attributes'] = [];
            foreach ($request->product_attributes as $attribute) {
                $data['product_attributes'][] = [
                    'attribute_id' => $attribute['id'],
                    'attribute_value_id' => $attribute['value']
                ];
            }
        }
        $products->insert_product($data);


//        $product_table_fill = $request->only([
//            'name',
//            'original_image_id',
//            'price',
//            'articul',
//            'quantity',
//            'measure_id',
//            'stock',
//            'meta_title',
//            'meta_description',
//            'meta_keywords',
//            'url_alias',
//            'product_category_id',
//            'product_related_category_id'
//        ]);
//
//        $products->fill($product_table_fill);
//        $products->save();
//
//        $product_id = $products->id;
//
//        $product_description_table_fill = $request->only([
//            'product_description',
//            'product_description_image_id',
//            'upper_note',
//            'heart_note',
//            'base_note',
//            'character'
//        ]);
//
//        foreach ($product_description_table_fill as $key => $item) {
//            $description->{$key} = $item ? $item : null;
//        }
//
//        $description->product_id = $product_id;
//        $description->save();
//
//        if (!empty($request->product_attributes)) {
//            foreach ($request->product_attributes as $attribute) {
//                $product_attributes[] = [
//                    'product_id' => $product_id,
//                    'attribute_id' => $attribute['id'],
//                    'attribute_value_id' => $attribute['value']
//                ];
//            }
//
//            $products->attributes()->createMany($product_attributes);
//            $products->create_product_thumnail($products->id);
//        }

        return redirect('/admin/products')
            ->with('message-success', 'Товар ' . $products->name . ' успешно добавлен.');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Products::find($id);
        if($product->description->image == null){
            $description_image = 'no_image.jpg';
        } else {
            $description_image = $product->description->image->href;
        }

        return view('admin.products.edit')
            ->with('product', $product)
            ->with('categories', Categories::all())
            ->with('attributes', Attributes::all())
            ->with('measures', Measures::all())
			->with('labels', ['' => '-', 'new' => 'Новинка', 'exclusive' => 'Эксклюзивный аромат', 'exclusive_new' => 'Эксклюзивная новинка', '5_present' => 'Пятый флакон в подарок', '11_present' => '11-й флакон в подарок', 'flakon_present' => 'флакон в подарок', 'sprey_present' => 'Спрей в подарок', 'black_friday' => 'Чёрная пятница'])
            ->with('description_image', $description_image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Image $image, ProductDescription $description)
    {
        $rules = $this->rules;
        $rules['name'] = 'required|unique:products,name,'.$id;
        //$rules['articul'] = 'required|unique:products,articul,'.$id;
        $rules['url_alias'] = 'required|unique:categories|unique:products,url_alias,'.$id;

        $attributes_error = $this->validate_attributes($request->product_attributes);

        $validator = Validator::make($request->all(), $rules, $this->messages);

        $image_href = $image->find($request->original_image_id)->href;

        if($request->product_description_image_id) {
            $description_image_href = $image->find($request->product_description_image_id)->href;
        } else {
            $description_image_href = 'no_image.jpg';
        }

        $gallery = '';

        if(!empty($request->gallery)) {
            $gallery = json_encode($request->gallery);
        }

        $request->merge(['href' => $image_href, 'description_href' => $description_image_href, 'gallery' => $gallery]);

        if ($validator->fails() || $attributes_error) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator)
                ->with('attributes_error', $attributes_error);
        }

        $product_table_fill = $request->only([
            'name',
            'original_image_id',
            'price',
            'quantity',
            'measure_id',
            'articul',
            'capacity',
            'stock',
            'meta_title',
            'meta_description',
            'meta_keywords',
            'url_alias',
            'product_category_id',
            'product_related_category_id',
            'label'
        ]);

        $product_table_fill['sale'] = $request->sale ? $request->sale : null;

        $product = Products::find($id);

        $product->fill($product_table_fill);

        $product_description_table_fill = $request->only([
            'product_description',
            'product_description_image_id',
            'upper_note',
            'heart_note',
            'base_note',
            'character',
            'gallery'
        ]);

        foreach ($product_description_table_fill as $key => $item) {
            $product->description->{$key} = $item ? $item : null;
        }

        $product->push();

        if (!empty($request->product_attributes)) {
            foreach ($request->product_attributes as $attribute) {
                $product_attributes[] = [
                    'product_id' => $product->id,
                    'attribute_id' => $attribute['id'],
                    'attribute_value_id' => $attribute['value']
                ];
            }

            $product->attributes()->delete();
            $product->attributes()->createMany($product_attributes);
            $product->create_product_thumnail($product->id);
        }

        return redirect('/admin/products')
            ->with('message-success', 'Товар ' . $product->name . ' успешно отредактирован.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Products::find($id);
        $product->delete();

        return redirect()->back()
            ->with('message-success', 'Товар ' . $product->name . ' успешно удален.');
    }

    /**
     * Получение списка всех атрибутов
     * @param Attributes $attributes
     * @return string|void
     */
    public function getAttributes(Attributes $attributes)
    {

        $attr = $attributes->all();
        $response = [];

        if(!empty($attr)){
            foreach ($attr as $attribute) {
                $response[] = [
                    'attribute_id'    => $attribute->id,
                    'attribute_name'  => $attribute->name
                ];
            }
        }

        return json_encode($response);

    }

    /**
     * Получение списка значений переданного атрибута
     * @param Attributes $attributes
     * @param Request $request
     * @return string|void
     */
    public function getAttributeValues(Attributes $attributes, Request $request)
    {

        $attribute = $attributes->find((int)$request->attribute_id);
        $response = [];

        if ($attribute !== null) {
            foreach ($attribute->values as $value) {
                $response[] = [
                    'attribute_value_id'    => $value->id,
                    'attribute_value'       => $value->name
                ];
            }
        }

        return json_encode($response);

    }

    /**
     * Живой поиск для модулей
     * @param Request $request
     * @param Products $products
     * @return string|void
     */
    public function livesearch(Request $request, Products $products)
    {
        $results = $products
            ->where('name', 'like', '%' . $request->search . '%')
            ->orWhere('articul', 'like', '%' . $request->search . '%')
            ->paginate(5);

        foreach ($results as $result) {

            $name = $result->articul ? $result->articul . ' / ' : '';
            $name .= $result->name;

            if ($result) {
                $json[] = [
                    'product_id' => $result->id,
                    'name'       => $name
                ];
            }
        }

        if (!empty($json)) {
            return json_encode($json);
        } else {
            return json_encode([['empty' => 'Ничего не найдено!']]);
        }
    }

    /**
     * @param $alias
     * @return mixed
     */
    public function show($alias)
    {
        $product = Products::where('url_alias', $alias)->first();
        if (is_null($product)){
            abort(404);
        }
        setlocale(LC_TIME, 'RU');
        $reviews = $product->reviews()
            ->where('published', 1)
            ->orderBy('updated_at', 'desc')
            ->paginate(4);
        foreach ($reviews as $review) {
            $review->date = iconv("cp1251", "UTF-8", $review->updated_at->formatLocalized('%d %b %Y'));
        }

        $recommended_module = Modules::where('alias_name', 'recommended')->first();
        $recommended_status = $recommended_module->status;

        if ($recommended_status) {
            $recommended_settings = json_decode($recommended_module->settings);
            $recommended_products = ModuleRecommended::where('product_category', $product->product_category_id)
                ->take($recommended_settings->quantity)->get();

            if (!$recommended_products->isEmpty()) {
                foreach ($recommended_products as $recommended_product) {
                    $recommended[] = Products::where('id', $recommended_product->product_id)->first();
                }
            } else {
                $recommended = [];
            }

        } else {
            $recommended = [];
        }

        $related_category_products = Products::where('product_category_id', $product->product_related_category_id)->inRandomOrder()->take(20)->get();

        foreach ($related_category_products as $related_product) {
            $related_product->reviews = $related_product->reviews->where('published', 1);
        }
        $related_category = Categories::where('id', $product->product_related_category_id)->first();

        $product_attributes = [];

        if(!$product->attributes->isEmpty()) {
            foreach ($product->attributes as $attribute) {
                if (isset($product_attributes[$attribute->info->name])) {
                    $product_attributes[$attribute->info->name] .= ', ' . $attribute->value->name;
                } else {
                    $product_attributes[$attribute->info->name] = $attribute->value->name;
                }
            }
        }

        if($product->sale){
            $sale = ($product->price / 100) * $product->sale;
            $product->price = $product->price - $sale;
        }

        $settings = Settings::find(1);

        $gallery = $product->gallery_images();
        if(count($gallery) == 0){
            $gallery = collect([$product->get_product_image]);
        }

        return view('public.product')
            ->with('product', $product)
            ->with('reviews', $reviews)
            ->with('related_products', $related_category_products)
            ->with('related_category', $related_category)
            ->with('recommended', $recommended)
            ->with('product_attributes', $product_attributes)
            ->with('gallery', $gallery)
            ->with('rate', $settings->rate);
    }

    /**
     * Страница поиска
     * @param Products $products
     * @param Request $request
     * @return mixed
     */
    public function search(Products $products, Request $request)
    {
        $search_text = $request->input('text');

        $id = $request->get('page', 1);

        $data = $products->search($search_text, $id, 8);

        return view('public.search')->with('products', $data)
            ->with('search_text', $search_text);
    }

    /**
     * Валидация атрибутов товара на одинаковые значения
     * @param $attributes
     * @return bool|string
     */
    public function validate_attributes($attributes) {
        $attributes_error = false;

        if (!empty($attributes)) {
            foreach ($attributes as $product_attribute) {
                $product_attribute_values[] = $product_attribute['value'];
            }

            foreach (array_count_values($product_attribute_values) as $count_value) {
                if ($count_value > 1) {
                    $attributes_error = 'Значения атрибутов не могут быть одинаковы!';
                    break;
                }
            }
        }

        return $attributes_error;
    }

    /**
     * Импорт товаров
     * @param Request $request
     * @param Products $products
     * @return mixed
     */
    public function upload(Request $request, Products $products)
    {
        $data = $request->input('products');
        $update = $request->input('update');
        $parsed_data = '';
        $errors = [];
        if(!empty($data)){
            $parsed_data = $this->products_parser($data);
            foreach($parsed_data as $product) {
                if($update)
                    $products->update_product($product);
                else
                    $products->insert_product($product);

                if(isset($product['errors']))
                    $errors[] = $product;
            }
        }

        return view('admin.products.upload')
            ->with('data', print_r($parsed_data, true))
            ->with('errors', $errors);
    }

    /**
     * Парсер csv товаров
     * @param $data
     * @return array
     */
    private function products_parser($data)
    {
        $image = new Image;
        $attribute_values = AttributeValues::all();
        $rows = explode(PHP_EOL, $data);
        $products = [];
        $keys = [
            'articul',
            'name',
            'product_description',
            'price',
            'sale',
            'capacity',
            'measure_id',
            'group',
            'character',
            'species',
            'material',
            'upper_note',
            'heart_note',
            'base_note',
            'diameter',
            'quantity',
            'color',
            'size',
            'stock',
            'manufacturer',
            'product_category_id',
            'product_related_category_id',
            'sex',
            'href',
            'brand'
        ];

        $attributes_ids = [
            'sex' => [
                'id' => 3,
                'values' => [
                    'женский' => 7,
                    'мужской' => 8
                ]
            ],
            'measure_id' => [
                'values' => [
                    'мл' => 1,
                    'л' => 2,
                    'шт' => 3
                ]
            ],
            'manufacturer' => [
                'id' => 1,
                'values' => []
            ],
            'product_category_id' => [
                'values' => [
                    'парфюмерия' => 1,
                    'флаконы' => 2,
                    'аксессуары' => 3,
                    'другая продукция' => 4
                ]
            ],
            'product_related_category_id' => [
                'values' => [
                    'парфюмерия' => 1,
                    'флаконы' => 2,
                    'аксессуары' => 3,
                    'другая продукция' => 4
                ]
            ],
            'species' => [
                'id' => 2,
                'values' => []
            ],
            'group' => [
                'id' => 4,
                'values' => []
            ],
            'color' => [
                'id' => 7,
                'values' => []
            ],
            'material' => [
                'id' => 5,
                'values' => []
            ],
            'brand' => [
                'id' => 6,
                'values' => []
            ]
        ];

        foreach ($attributes_ids as $key => $val){
            if(isset($val['id'])){
                foreach ($attribute_values as $value){
                    if($value->attribute_id == $val['id']) {
                        $attributes_ids[$key]['values'][mb_strtolower($value->name)] = $value->id;
                    }
                }
            }
        }

        foreach ($rows as $row){
            $row = explode('	', $row);
            if(count($keys) == count($row)) {
                array_walk($row, array($this, 'trim_value'));
                $product = array_combine($keys, $row);
                $product['capacity'] = str_replace(array('мл', 'л', ' '), '', $product['capacity']);
                $product['group'] = explode(',', preg_replace('/(^"|"$|;$|\.$|,$)/', '', $product['group']));
                $product['product_attributes'] = [];

                if(empty($product['manufacturer']) || empty($product['articul']))
                    $product['url_alias'] = mb_strtolower($this->rus2lat($product['name']) . '_' . $product['capacity']);
                else
                    $product['url_alias'] = mb_strtolower($product['manufacturer'] . '_' . $product['articul']);

                $product['original_image_id'] = $image->get_id_by_title($product['href']);

                if(empty($product['original_image_id'])){
                    $product['original_image_id'] = 1;
                    if(!isset($product['errors']))
                        $product['errors'] = [];
                    $product['errors'][] = 'Не найдено изображение "'.$product['href'].'"';
                }

                if($product['stock'] == '')
                    $product['stock'] = 1;

                foreach ($product as $key => $val) {
                    if(is_string($val)){
                        $product[$key] = preg_replace('/(^"|"$|;$|\.$|,$|,\s?,)/', '', preg_replace('@^\s*|\s*$@u', '', $val));
                    }

                    if (isset($attributes_ids[$key])) {
                        if (isset($attributes_ids[$key]['id']) && is_array($val)) {
                            foreach ($val as $attr) {
                                $attr = preg_replace('@^\s*|\s*$@u', '', $attr);
                                if(isset($attributes_ids[$key]['values'][mb_strtolower($attr)])) {
                                    $product['product_attributes'][] = [
                                        'attribute_id' => $attributes_ids[$key]['id'],
                                        'attribute_value_id' => $attributes_ids[$key]['values'][mb_strtolower($attr)]
                                    ];
                                }elseif(!empty($attr)){
                                    if(!isset($product['errors']))
                                        $product['errors'] = [];
                                    $product['errors'][] = 'Не распознан атрибут "'.$attr.'"';
                                }
                            }
                            unset($product[$key]);
                        } else {
                            $val = preg_replace('@^\s*|\s*$@u', '', $val);
                            if (isset($attributes_ids[$key]['id'])) {
                                if(isset($attributes_ids[$key]['values'][mb_strtolower($val)])) {
                                    $product['product_attributes'][] = [
                                        'attribute_id' => $attributes_ids[$key]['id'],
                                        'attribute_value_id' => $attributes_ids[$key]['values'][mb_strtolower($val)]
                                    ];
                                }elseif(!empty($val)){
                                    if(!isset($product['errors']))
                                        $product['errors'] = [];
                                    $product['errors'][] = 'Не распознан атрибут "'.$val.'"';
                                }
                                unset($product[$key]);
                            } elseif (isset($attributes_ids[$key]['values']) && isset($attributes_ids[$key]['values'][mb_strtolower($val)])) {
                                $product[$key] = $attributes_ids[$key]['values'][mb_strtolower($val)];
                            }elseif(!empty($val)){
                                if(!isset($product['errors']))
                                    $product['errors'] = [];
                                $product['errors'][] = 'Не распознан атрибут "'.$val.'"';
                            }
                        }
                    }
                }

                $product['meta_title'] = $product['name'];
                $product['meta_description'] = $product['product_description'];
                $product['meta_keywords'] = $product['upper_note'].', '.$product['heart_note'].', '.$product['base_note'];

                $products[] = $product;
            }
        }
        return $products;
    }

    /**
     * Удаление нежелательных символов
     * @param $value
     */
    function trim_value(&$value)
    {
        if(is_string($value)) {
            $value = preg_replace('/(^"|"$|;$|\.$|,$|,\s?,)/', '', preg_replace('@^\s*|\s*$@u', '', $value));
        }
    }

    /**
     * Транслит
     * @param $string
     * @return mixed
     */
    public function rus2lat($string)
    {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => "",  'ы' => 'y',   'ъ' => "",
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => "",  'Ы' => 'Y',   'Ъ' => "",
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }

    public function table(Categories $category)
    {
        $products = $category->get_products(2, [], 'name', 100);
        return view('admin.products.table', ['products' => $products]);
    }
}