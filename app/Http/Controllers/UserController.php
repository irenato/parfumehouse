<?php

namespace App\Http\Controllers;

use App\Reviews;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Validator;

use App\User;
use App\Settings;
use App\Image;
use App\UserData;
use App\Order;
use App\ProductsInOrder;
use App\Wishlist;
use App\Products;
use App\Categories;
use App\Cart;
use App\ProductsCart;
use Breadcrumbs;

use Cartalyst\Sentinel\Roles\EloquentRole as Roles;

class UserController extends Controller
{
    private $users;
    public $settings;

    protected $rules = [
        'email' => 'required|unique:users'
    ];
    protected $messages = [
        'email.required' => 'Поле должно быть заполнено!',
        'email.unique' => 'Поле должно быть уникальным!'
    ];

    public function __construct()
    {
        //
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::join('role_users', function ($join) {
            $join->on('users.id', '=', 'role_users.user_id')
                ->whereIn('role_users.role_id', [3,4]);
        })
            ->get();

        return view('admin.users.index')->with('users', $users)->with('title', 'Список пользователей');
    }

    /**
     * @return mixed
     */
    public function managers()
    {
        $user_role = Sentinel::findRoleBySlug('manager');
        $users = $user_role->users()->with('roles')->get();

        return view('admin.users.index')->with('users', $users)->with('title', 'Список менеджеров');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $user = Sentinel::check();

        $user = User::find($user->id);

        $address = '';
        if(!is_null($user->user_data->adress)) {
            if (json_decode($user->user_data->adress) != null){
                $address = implode(', ', json_decode($user->user_data->adress, true));
            } else {
                $address = $user->user_data->adress;
            }
        }

        return view('users.index')->with('user', $user)->with('address', $address);
    }
    public function history()
    {
        $user = Sentinel::check();
        $orders = Order::where('user_id',$user->id)->orderBy('created_at','desc')->get();
//        return dd($orders);
        return view('users.history')->with('user', $user)->with('orders', $orders);
    }
    public function wishList()
    {
        $user = Sentinel::check();
        $wish_list = Wishlist::where('user_id',$user->id)->get();
        return view('users.wishlist')->with('user', $user)->with('wish_list', $wish_list);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = Settings::find(1);

        $image_size = [
            'width' => $settings->category_image_width,
            'height' => $settings->category_image_height
        ];
        $user = User::find($id);
        $user_data = UserData::where('user_id', $id)->first();
        return view('admin.users.edit')
            ->with('user', $user)
            ->with('user_data', $user_data)
            ->with('image_size', $image_size);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->rules;
        $rules['email'] = 'required|unique:users,email,'.$id.'';

        $messages = $this->messages;

        $validator = Validator::make($request->all(), $rules, $messages);
        $user = User::find($id);
        $user_data = UserData::where('user_id', $id)->first();

        $image_id = $request->image_id ? $request->image_id : $user_data->image_id;
        $href = Image::find($image_id)->href;

        $request->merge(['href' => $href, 'user_id' => $id]);
//return dd($request);
        if($validator->fails()){
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $request_user = $request->only(['first_name', 'last_name', 'email', 'phone']);
        $user->fill($request_user);
        $user->save();
        if($request->image_id < 1){
            $request_user_data = $request->only(['user_id', 'adress', 'company', 'other_data']);
        }else {
            $request_user_data = $request->only(['user_id', 'adress', 'company', 'other_data', 'image_id']);
        }
        $user_data->update($request_user_data);


        /*
         *  надо поменять сентинел на что то другое!
         *
         *
         */

        $user_role = Sentinel::findRoleBySlug('user');
        $unreg_user_role = Sentinel::findRoleBySlug('unregister_user');
        $unreg_users = $unreg_user_role->users()->with('roles')->get();
        $users = $user_role->users()->with('roles')->get();
        $users = $users->merge($unreg_users);

        return redirect('/admin/users')
            ->with('users', $users)
            ->with('message-success', 'Пользователь ' . $user->first_name . ' успешно обновлен.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeData()
    {
        $user = Sentinel::check();
        if ($user) {
            $user = User::find($user->id);
        }

        return view('users.change_data')
            ->with('user', $user);
    }

    public function saveChangedData(Request $request)
    {
        $user = Sentinel::check();
        if ($user) {
            $user = User::find($user->id);
        }

        $rules = [
            'first_name' => 'required',
            'phone'     => 'required|regex:/^[0-9\-! ,\'\"\/+@\.:\(\)]+$/',
            'email'     => 'required|email|unique:users,email,'.$user->id,
            'password'  => 'min:6|confirmed',
            'password_confirmation' => 'min:6',
        ];

        $messages = [
            'first_name.required' => 'Не заполнены обязательные поля!',
            'phone.required'    => 'Не заполнены обязательные поля!',
            'phone.regex'       => 'Некорректный телефон!',
            'email.required'    => 'Не заполнены обязательные поля!',
            'email.email'       => 'Некорректный email-адрес!',
            'email.unique'      => 'Пользователь с таким email-ом уже зарегистрирован!',
            'password.min'      => 'Пароль должен быть не менее 6 символов!',
            'password.confirmed' => 'Введенные пароли не совпадают!',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }

        $user->first_name = htmlspecialchars($request->first_name);
        $user->last_name = htmlspecialchars($request->last_name);
        $user->email = htmlspecialchars($request->email);
        $user->phone = htmlspecialchars($request->phone);
        $user->user_data->adress = htmlspecialchars($request->adress);
        $user->user_data->company = htmlspecialchars($request->company);
        $user->user_data->subscribe = $request->subscribe ? 1 : 0;

        if($request->password) {
            $user->password = password_hash($request->password, PASSWORD_DEFAULT);
        }

        $user->push();

        return redirect('/user')
            ->with('status', 'Ваши личные данные успешно изменены!');
    }

    public function subscribe(Request $request, User $user, UserData $user_data)
    {
        $rules = [
            'email'     => 'required|email'
        ];

        $messages = [
            'email.required'    => 'Вы не указали email-адрес!',
            'email.email'       => 'Некорректный email-адрес!',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }

        $user_exists = User::where('email', $request->email)->first();

        if ($user_exists){
            $subscribe = $user->where('id', $user_exists->id)->first();
            $subscribe->user_data->subscribe = 1;
            $subscribe->save();
        } else {
            $user = Sentinel::registerAndActivate(array(
                'email'    => $request->email,
                'password' => 'null',
                'permissions' => [
                    'unregistered' => true
                ]
            ));

            $role = Sentinel::findRoleBySlug('unregister_user');
            $role->users()->attach($user);

            $user_data->create([
                'user_id'   => $user->id,
                'image_id'  => 1,
                'subscribe' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        return response()->json(['success' => 'Вы успешно подписались на новости!']);
    }

    public function fromCartToOrder($cart_data)
    {

    }

    public function statistic($id)
    {
        $orders = Order::where('user_id', $id)->get();

        return view('admin.users.orders')->with('orders', $orders)->with('user', User::find($id));
    }
    public function reviews($id)
    {
        $reviews = Reviews::where('user_id', $id)->paginate(10);

        return view('admin.users.reviews')->with('reviews', $reviews)->with('user', User::find($id));
    }
    public function adminWishlist($id)
    {
        $wishlist = Wishlist::where('user_id', $id)->paginate(10);

        return view('admin.users.wishlist')->with('wishlist', $wishlist)->with('user', User::find($id));
    }

    public function fuck()
    {
        $order = Order::find(14);
        foreach($order->products as $product) {
//            $p[] = $product->product->name;
            foreach ($product->product->attributes as $atributes) {
                if ($atributes->attribute_id == 1) {
                    $value = $atributes->value;
                }
            }
        }

        return dd($value->name);
    }
}
