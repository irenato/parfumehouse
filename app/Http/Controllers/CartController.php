<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cart;
use App\Products;
use App\ProductsCart;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\User;
use App\Order;
use Crypt;
use Illuminate\Support\Facades\Session;
use App\Settings;

class CartController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
    public function addToCart(Request $request)
    {
        if(!Session::has('user_cart_id')){
            $user_cart_id = md5(rand(0,100500));
            Session::put('user_cart_id', $user_cart_id);
        }else{
            $user_cart_id = Session::get('user_cart_id');
        }
        $user_id = $request->user_id;
        $product_id = $request->product_id;
        $product_quantity = $request->quantity;

        $product = Products::find($product_id);

        if ($product->sale) {
            $sale = ($product->price / 100) * $product->sale;
            $product_price = $product->price - $sale;
        } else {
            $product_price = $product->price;
        }

        $insert_cart_data = [
            'user_id' => $user_id,
            'user_cart_id' => $user_cart_id,
            'user_ip' => $_SERVER['REMOTE_ADDR'],
            'products_quantity' => $product_quantity,
            'products_sum' => $product_price * $product_quantity,
        ];
        $insert_prod_cart_data = [
            'product_id' => $product_id,
            'product_quantity' => $product_quantity,
        ];

        $current_cart = Cart::where('user_cart_id', $user_cart_id)->first();
        /*
         * Менять юзера в корзине во время логина надо
         * */
//        if($user_id != $current_cart->user_id and $current_cart->user_id == 0){
//            $current_cart->update(['user_id' => $user_id]);
//        }
        if(is_null($current_cart)){
            $current_cart = Cart::create($insert_cart_data);
            if($product_quantity != 0){
                $new_products_cart = new ProductsCart($insert_prod_cart_data);
                $current_cart->products_cart()->save($new_products_cart);
            }
        }else{
            $update_product = $current_cart->products_cart()->where('product_id',$product_id)->first();

            if(!$update_product){
                if($product_quantity != 0) {
                    $new_products_cart = new ProductsCart($insert_prod_cart_data);
                    $update_product = $current_cart->products_cart()->save($new_products_cart);
                }
            }else{
				$insert_prod_cart_data['product_quantity'] += $update_product->product_quantity;
                if($product_quantity != 0) {
                    $update_product->update($insert_prod_cart_data);
                }else{
                    $update_product->delete();
                }
            }
        }
        $products_in_cart = $current_cart->products_cart()->get();
        $products_quantity = $products_in_cart->sum('product_quantity');
        $products_array = $products_in_cart->lists('product_quantity' , 'product_id')->toArray();

        $products_sum = 0;
        foreach($products_in_cart as $products){

            if ($products->product->sale) {
                $sale = ($products->product->price / 100) * $products->product->sale;
                $product_price = $products->product->price - $sale;
            } else {
                $product_price = $products->product->price;
            }
            $products_sum += $product_price * $products_array[$products->product->id];
        }

        $current_cart->update([
            'products_quantity' => $products_quantity,
            'products_sum' => $products_sum,
            'user_ip' => $_SERVER['REMOTE_ADDR']
        ]);

        $settings = Settings::find(1);
        return ['product_quantity' => $products_quantity, 'products_sum' => $products_sum * $settings->rate];
    }

    /**
     * @return array
     */
    public function getCartData()
    {
        if(!Session::has('user_cart_id')){
            $user_cart_id = md5(rand(0,100500));
            Session::put('user_cart_id', $user_cart_id);
        }else{
            $user_cart_id = Session::get('user_cart_id');
        }
        $current_cart = Cart::where('user_cart_id', $user_cart_id)->first();

        $i = 0;
        $products_in_cart = [];
        if(!is_null($current_cart)) {

            $settings = Settings::find(1);

            foreach ($current_cart->products_cart as $products) {
                if ($products->product->sale) {
                    $sale = ($products->product->price / 100) * $products->product->sale;
                    $product_price = $products->product->price - $sale;
                } else {
                    $product_price = $products->product->price;
                }

                $products_in_cart[$i]['id'] = $products->product->id;
                $products_in_cart[$i]['name'] = $products->product->name;
                $products_in_cart[$i]['alias'] = $products->product->url_alias;
                $products_in_cart[$i]['quantity'] = $products->product_quantity;

                $products_in_cart[$i]['price'] = round($product_price * $settings->rate, 2);
                $products_in_cart[$i]['href'] = $products->product->get_product_image->get_current_file_url('product_list');
                $products_in_cart[$i]['product_quantity'] = $products->product->capacity ? $products->product->capacity : $products->product->quantity;
                $products_in_cart[$i]['measure'] = $products->product->measures->name;
                $i++;
            }
        }
        return $products_in_cart;
    }

    /**
     * @param Request $request
     * @return array|bool
     */
    public function update(Request $request)
    {
        $user_cart_id = Session::get('user_cart_id');

        $current_cart = Cart::where('user_cart_id', $user_cart_id)->first();

        if(is_null($current_cart)){
            return false;
        }
        $current_cart->products_cart()->delete();
        if(!empty($request['cart'])){
            foreach($request['cart'] as $product){
                if($product['product_quantity'] <= 0){
                    continue;
                }
                $new_products_cart = new ProductsCart($product);
                $current_cart->products_cart()->save($new_products_cart);
            }
        }

        $products_in_cart = $current_cart->products_cart()->get();
        $products_quantity = $products_in_cart->sum('product_quantity');
        $products_array = $products_in_cart->lists('product_quantity' , 'product_id')->toArray();

        $products_sum = 0;
        foreach($products_in_cart as $products){
            if ($products->product->sale) {
                $sale = ($products->product->price / 100) * $products->product->sale;
                $product_price = $products->product->price - $sale;
            } else {
                $product_price = $products->product->price;
            }
            $products_sum += $product_price * $products_array[$products->product->id];
        }

        $current_cart->update([
            'products_quantity' => $products_quantity,
            'products_sum' => $products_sum,
            'user_ip' => $_SERVER['REMOTE_ADDR']
        ]);
//        $current_cart->update(['products_quantity' => $request['products_quantity']]);
        return ['products_quantity' => $request['products_quantity'], 'sum' => $request['sum']];
    }

    /**
     * @param $user_cart_id
     * @return mixed
     */
    public function getCartProducts($user_cart_id)
    {
        $cart = Cart::where('user_cart_id',$user_cart_id)->first();

        return $cart;
    }

    /**
     * Метод передает корзину юзеру
     * @param $user_id
     * @return array
     */
    public static function cartToUser($user_id)
    {
        $user_cart_id = Session::get('user_cart_id');
        $current_cart_from_sess = Cart::where('user_cart_id', $user_cart_id)->first();
        $current_cart = [];
        if(!is_null($current_cart_from_sess)) {
            $current_cart_from_sess->update(['user_id' => $user_id]);
            $current_cart = $current_cart_from_sess;
        }
        $current_cart_from_user = Cart::where('user_id', $user_id)->first();
        if(!is_null($current_cart_from_user)) {
//            $current_cart_from_user->update(['user_id' => $user_id]);
            $current_cart = $current_cart_from_user;
            $user_cart_id = $current_cart->user_cart_id;
            Session::put('user_cart_id', $user_cart_id);
        }

        return $current_cart;
    }
}
