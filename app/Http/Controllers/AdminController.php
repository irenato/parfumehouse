<?php

namespace App\Http\Controllers;

use App\UserSession;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function dash(UserSession $session)
    {
        $users = $session->getUserActivity();

        $online_users = [];

        foreach ($users as $user) {
            $url = 'http://ipinfo.io/' . $user->ip_address . '/json';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $ipinfo = curl_exec($ch);

            if (!is_null($user->user_id)) {
                $user_info = Sentinel::findById($user->user_id);
            } else {
                $user_info = null;
            }

            if ($user->user_agent) {
                $browser = $session->getBrowser($user->user_agent);
            } else {
                $browser = null;
            }

            $online_users[] = [
                'ipinfo' => json_decode($ipinfo),
                'userinfo' => $user_info,
                'browserinfo' => $browser
            ];

        }

        return view('admin.dashboard')
            ->with('online_users', $online_users);
    }
}

/*
"ip": "203.205.28.14",
  "hostname": "static.cmcti.vn",
  "city": "Ho Chi Minh City",
  "region": "Ho Chi Minh City",
  "country": "VN",
  "loc": "10.8142,106.6438",
  "org": "AS45903 CMC Telecom Infrastructure Company"
*/