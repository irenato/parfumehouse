<?php

namespace App\Http\Controllers;

use App\Attributes;
use App\ModuleBestsellers;
use App\Modules;
use App\ProductAttributes;
use Illuminate\Http\Request;
use App\Categories;
use App\Settings;
use App\Image;
use App\Products;
use App\Http\Requests;
use Validator;


class CategoriesController extends Controller
{

    private $rules = [
        'name' => 'required',
        'meta_title' => 'required|max:75',
        'meta_description' => 'max:180',
        'meta_keywords' => 'max:180',
        'url_alias' => 'required|unique:categories',
    ];

    private $messages = [
        'name.required' => 'Поле должно быть заполнено!',
        'meta_title.required' => 'Поле должно быть заполнено!',
        'meta_title.max' => 'Максимальная длина заголовка не может превышать 75 символов!',
        'meta_description.max' => 'Максимальная длина описания не может превышать 180 символов!',
        'meta_keywords.max' => 'Максимальная длина ключевых слов не может превышать 180 символов!',
        'url_alias.required' => 'Поле должно быть заполнено!',
        'url_alias.unique' => 'Значение должно быть уникальным для каждой категории!'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index')->with('categories', Categories::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create')
            ->with('categories', Categories::all())
            ->with('attributes', Attributes::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Categories $categories)
    {

        $validator = Validator::make($request->all(), $this->rules, $this->messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $categories->fill($request->except('_token'));
        $categories->description = !empty($request->description) ? $request->description : null;
        $categories->sort_order = !empty($request->sort_order) ? $request->sort_order : 0;
        $categories->save();

        return redirect('/admin/categories')
            ->with('message-success', 'Категория ' . $categories->name . ' успешно добавлена.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.categories.edit')
            ->with('attributes', Attributes::all())
            ->with('category', Categories::find($id))
            ->with('categories', Categories::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Categories $categories)
    {

        $rules = $this->rules;
        $rules['url_alias'] = 'required|unique:categories,url_alias,'.$id;

        $validator = Validator::make($request->all(), $rules, $this->messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $category = $categories->find($id);
        $category->fill($request->except('_token'));
        $category->description = !empty($request->description) ? $request->description : null;
        $category->sort_order = !empty($request->sort_order) ? $request->sort_order : 0;
        $category->save();

        return redirect('/admin/categories')
            ->with('message-success', 'Категория ' . $category->name . ' успешно обновлена.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Categories::find($id);
        $category->delete();

        return redirect('/admin/categories')
            ->with('message-success', 'Категория ' . $category->name . ' успешно удалена.');
    }

    public function show($alias, Request $request, Categories $categories, Attributes $attributes)
    {
        $sort_array = [
            [
                'value' => 'popularity',
                'name'  => 'популярности'
            ],
            [
                'value' => 'date',
                'name'  => 'дате'
            ],
            [
                'value' => 'name',
                'name'  => 'названию'
            ]
        ];

        $filter = [];

        if ($request->filter_attributes !== null) {
            foreach ($request->filter_attributes as $attribute_id => $value) {
                foreach ($value['value'] as $attribute_value_id => $on) {
                    $filter[$attribute_id][] = $attribute_value_id;
                }
            }
        }

        $current_sort = $request->sort ? $request->sort : 'date';

        if ($alias !== 'popular' && $alias !== 'new' && $alias !== 'sale') {
            $category = $categories->where('url_alias', $alias)->first();
            if (is_null($category)){
                abort(404);
            }
            $category_id = $category->id;
            $take = false;

        } elseif ($alias == 'popular') {

            $category_id = 'bestsellers';
            $category = [
                'meta_title'    => 'Популярные товары',
                'name'          => 'Популярные',
                'url_alias'     => 'popular'
            ];

            $new_settings = json_decode(Modules::where('alias_name', 'bestsellers')->value('settings'));
            $take = $new_settings->quantity;

        } elseif ($alias == 'new') {

            $category_id = 'new';
            $category = [
                'meta_title'    => 'Новые товары',
                'name'          => 'Новинки',
                'url_alias'     => 'new'
            ];

            $new_settings = json_decode(Modules::where('alias_name', 'latest')->value('settings'));
            $take = $new_settings->quantity;
        } elseif ($alias == 'sale') {
            $category_id = 'sale';
            $category = [
                'meta_title'    => 'Акция',
                'name'          => 'Акционные товары',
                'url_alias'     => 'sale'
            ];

            $take = 30;
        }

        $capacity = empty($request->capacity) ? [] : $request->capacity;

        $products = $categories->get_products($category_id, $filter, $capacity, $current_sort, $take);

        $product_attributes = [];
        $product_attributes_values = [];

        foreach ($products as $product) {
            $product->reviews = $product->reviews->where('published', 1);
            foreach ($product->attributes as $attribute) {
                $product_attributes[] = $attribute->attribute_id;
                $product_attributes_values[] = $attribute->attribute_value_id;
            }
            if ($product->sale) {
                $sale = ($product->price / 100) * $product->sale;
                $product->price = $product->price - $sale;
            }
        }

        $product_attributes_values = array_unique($product_attributes_values);

        $product_attributes = $attributes->get_current_product_attributes($category_id, $take);

        if(method_exists($products, 'total')) {
            $paginator = $products->appends(['sort' => $current_sort, 'view' => empty($request->view) || !in_array($request->view, ['tile', 'list']) ? 'tile' : $request->view]);
        } else {
            $paginator = false;
        }

        $capacities = $categories->get_all_capacities($category_id);

		// Вывод фильтра по брендам в начале
        $product_attributes = $product_attributes->filter(function ($value, $key) {
            return $value->id == 6;
        })->merge($product_attributes->filter(function ($value, $key) {
            return $value->id != 6;
        }));
		
        return view('public.category')
            ->with('category', $category)
            ->with('products', $products)
            ->with('attributes', $product_attributes)
            ->with('sort_array', $sort_array)
            ->with('current_sort', $current_sort)
            ->with('product_attributes_values', $product_attributes_values)
            ->with('filter', $filter)
            ->with('paginator', $paginator)
            ->with('capacities', $capacities)
			->with('view', empty($request->view) || !in_array($request->view, ['tile', 'list']) ? 'tile' : $request->view)
            ->with('capacity', $capacity);
    }

    public function filtered($alias, Request $request, Categories $categories, Attributes $attributes){
        $filter = [];

        if ($request->filter_attributes !== null) {
            foreach ($request->filter_attributes as $attribute_id => $value) {
                foreach ($value['value'] as $attribute_value_id => $on) {
                    $filter[$attribute_id][] = $attribute_value_id;
                }
            }
        }

        if ($alias !== 'popular' && $alias !== 'new') {
            $category = $categories->where('url_alias', $alias)->first();
            $category_id = $category->id;
        } elseif ($alias == 'popular') {
            $category_id = 'bestsellers';
        } elseif ($alias == 'new') {
            $category_id = 'new';
        }

        $capacity = empty($request->capacity) ? [] : $request->capacity;

        return $categories->get_products_count($category_id, $filter, $capacity);
    }
}


