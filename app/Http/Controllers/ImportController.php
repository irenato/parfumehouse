<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class ImportController extends Controller
{
    public function show(){
        $str1 = file_get_contents('reni.txt');
        $arr1 = explode(PHP_EOL, $str1);
        $trm_arr1 = array_map('trim', $arr1);
        $unq_arr1 = array_unique($trm_arr1);

        $str2 = file_get_contents('lof.txt');
        $arr2 = explode(PHP_EOL, $str2);
        $trm_arr2 = array_map('trim', $arr2);
        $unq_arr2 = array_unique($trm_arr2);

        $str3 = file_get_contents('refan.txt');
        $arr3 = explode(PHP_EOL, $str3);
        $trm_arr3 = array_map('trim', $arr3);
        $unq_arr3 = array_unique($trm_arr3);

        $res = array_merge($unq_arr1, $unq_arr2, $unq_arr3);
        $unq_res = array_unique($res);

        foreach ($unq_res as $attr) {
            DB::table('attribute_values')->insert([
                array('attribute_id' => 6,'name' => $attr,'image_href' => null),
            ]);
        }

        dd($unq_res);
    }
}
