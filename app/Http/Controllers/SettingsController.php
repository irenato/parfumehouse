<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Http\Requests;
use App\Settings;
use Validator;

class SettingsController extends Controller
{
    private $user;
    private $settings;

    function __construct(Settings $settings)
    {
        $this->user = Sentinel::check();
        $this->settings = $settings;
    }

    public function index()
    {
        $settings = $this->settings->find(1);
        $socials = ($settings->socials !== null) ? json_decode($settings->socials) : null;

        return view('admin.settings')
            ->with('user', $this->user)
            ->with('settings', $settings)
            ->with('socials', $socials);
    }

    public function update(Request $request)
    {
        $rules = [
            'meta_title' => 'required|max:75',
            'meta_description' => 'max:180',
            'meta_keywords' => 'max:180',
            'notify_emails.*' => 'email|distinct|filled',
            'other_phones.*' => 'distinct|filled|regex:/^[0-9\-! ,\'\"\/+@\.:\(\)]+$/',
            'main_phone_1' => 'regex:/^[0-9\-! ,\'\"\/+@\.:\(\)]+$/',
            'main_phone_2' => 'regex:/^[0-9\-! ,\'\"\/+@\.:\(\)]+$/',
            'product_list_image_width' => 'required|numeric',
            'product_list_image_height' => 'required|numeric',
            'product_image_width' => 'required|numeric',
            'product_image_height' => 'required|numeric',
            'blog_image_width' => 'required|numeric',
            'blog_image_height' => 'required|numeric',
            'slider_image_width' => 'required|numeric',
            'slider_image_height' => 'required|numeric',
            'rate' => 'required|numeric'
        ];

        $messages = [
            'meta_title.required' => 'Поле должно быть заполнено!',
            'meta_title.max' => 'Максимальная длина заголовка не может превышать 75 символов!',
            'meta_description.max' => 'Максимальная длина описания не может превышать 180 символов!',
            'meta_keywords.max' => 'Максимальная длина ключевых слов не может превышать 180 символов!',
            'notify_emails.*.email' => 'Введите корректный e-mail адрес!',
            'notify_emails.*.distinct' => 'Значения одинаковы!',
            'notify_emails.*.filled' => 'Поле должно быть заполнено!',
            'other_phones.*.distinct' => 'Значения одинаковы!',
            'other_phones.*.filled' => 'Поле должно быть заполнено!',
            'other_phones.*.regex' => 'Неверный формат телефона!',
            'main_phone_1.regex' => 'Неверный формат телефона!',
            'main_phone_2.regex' => 'Неверный формат телефона!',
            'product_list_image_width.required' => 'Поле должно быть заполнено!',
            'product_list_image_height.required' => 'Поле должно быть заполнено!',
            'product_image_width.required' => 'Поле должно быть заполнено!',
            'product_image_height.required' => 'Поле должно быть заполнено!',
            'product_list_image_width.numeric' => 'Введите числовое значение!',
            'product_list_image_height.numeric' => 'Введите числовое значение!',
            'product_image_width.numeric' => 'Введите числовое значение!',
            'product_image_height.numeric' => 'Введите числовое значение!',
            'rate' => 'Введите числовое значение!'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->with('message-error', 'Сохранение не удалось! Проверьте форму на ошибки!')
                ->withErrors($validator);
        }

        $settings = $this->settings->find(1);
        $settings->fill($request->except('_token'));
        $settings->about = htmlentities($request->about);
        $settings->terms = htmlentities($request->terms);
        $settings->other_phones = !empty($request->other_phones) ? json_encode($request->other_phones) : null;
        $settings->notify_emails = !empty($request->notify_emails) ? json_encode($request->notify_emails) : null;
        $settings->socials = !empty($request->socials) ? json_encode($request->socials) : null;
        $settings->save();

        return back()->with('message-success', 'Настройки успешно сохранены!');
    }

}
