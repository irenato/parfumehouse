<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules;
use App\ModuleSlideshow;
use App\Http\Requests;
use App\Settings;

class ModuleSlideshowController extends Controller
{

    protected $request;
    protected $slideshow;

    public function __construct(Request $request, ModuleSlideshow $slideshow) {
        $this->request = $request;
        $this->slideshow = $slideshow;
    }

    public function index()
    {
        $module = Modules::where('alias_name', 'slideshow')->first();
        $settings = json_decode($module->settings);
        
        $global_settings = new Settings;
        $registered_sizes = $global_settings->get_images_sizes();

        $image_size = [
            'width' => $registered_sizes['slide']['w'],
            'height' => $registered_sizes['slide']['h']
        ];

        return view('admin.modules.slideshow')
            ->with('module', $module)
            ->with('settings', $settings)
            ->with('image_size', $image_size)
            ->with('slideshow', $this->slideshow->all());
    }

    public function save()
    {
        $modules = Modules::all();
        $module = Modules::where('alias_name', 'slideshow')->first();
        $settings = json_encode([
            'image_width'   => $this->request->image_width,
            'image_height'  => $this->request->image_height,
            'quantity'      => $this->request->quantity
            ]
        );

        $module->status = $this->request->status;
        $module->settings = $settings;
        $module->save();

        $this->slideshow->truncate();

        if (!empty($this->request->slide)) {
            foreach ($this->request->slide as $slide) {
                if (!$slide['image_id']) {
                    $slide['image_id'] = 1;
                };
                $this->slideshow->create($slide);
            }
        }

        return redirect('admin/modules')
            ->with('modules', $modules)
            ->with('message-success', 'Настройки модуля ' . $module->name . ' успешно обновлены!');
    }
}
