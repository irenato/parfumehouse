<?php

namespace App\Http\Controllers;

use App\ProductDescription;
use App\Products;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Reviews;
use App\Http\Requests;
use Validator;
use App\User;
use App\UserData;
use Carbon\Carbon;
use App\Settings;
use Mail;

class ReviewsController extends Controller
{
    public function index()
    {
        $all_reviews = Reviews::orderBy('updated_at', 'desc')->paginate(20);
        $new_reviews = [];
        $reviews = [];

        foreach ($all_reviews as $review) {
            if($review->new) {
                $new_reviews[] = $review;
            } else {
                $reviews[] = $review;
            }
        }

        return view('admin.reviews.index')
            ->with('new', $new_reviews)
            ->with('reviews', $reviews)
            ->with('all_reviews', $all_reviews);
    }

    public function show($id)
    {
        $review = Reviews::find($id);
        return view('admin.reviews.show')
            ->with('review', $review);
    }


    public function update(Request $request, $id)
    {

        $review = Reviews::find($id);

        if($request->published) {
            $grades = Reviews::where('product_id', $request->product_id)
                ->where('published', 1)
                ->pluck('grade');

            if(!$grades->isEmpty()) {
                $sum = 0;
                foreach ($grades as $grade) {
                    $sum += $grade;
                }

                $average = $sum / $grades->count();
                $rating = round($average, 2, PHP_ROUND_HALF_UP);

                $product = Products::find($request->product_id);
                $product->rating = $rating;
                $product->update();
            }
        }

        $review->published = $request->published;
        $review->new = 0;
        $review->update();

        return redirect('/admin/reviews')
            ->with('message-success', 'Отзыв успешно обновлен!');
    }

    public function destroy($id)
    {
        $review = Reviews::find($id);
        $review->delete();
        return redirect('/admin/reviews')
            ->with('message-success', 'Отзыв успешно удален!');
    }

    public function add(Request $request, Reviews $review, UserData $user_data)
    {
        $rules = [
            'grade' => 'required',
            'review' => 'required',
            'name' => 'required',
            'email' => 'required|email',
        ];

        $messages = [
            'grade.required'    => 'Вы не поставили оценку!',
            'review.required'   => 'Оставьте текст отзыва!',
            'name.required'     => 'Введите имя!',
            'email.required'    => 'Введите email!',
            'email.email'       => 'Введите корректный email-адрес!'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }

        $user = User::where('email', $request->email)->first();

        if($user == null) {
            $user = Sentinel::registerAndActivate(array(
                'email'    => $request->email,
                'password' => 'null',
                'first_name' => $request->name,
                'permissions' => [
                    'unregistered' => true
                ]
            ));

            $role = Sentinel::findRoleBySlug('unregister_user');
            $role->users()->attach($user);

            $user_data->create([
                'user_id'   => $user->id,
                'image_id'  => 1,
                'subscribe' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $review->fill($request->except('_token'));
        $review->user_id = $user->id;
        $review->published = 0;
        $review->new = 1;
        $review->save();

        $emails = json_decode(Settings::find(1)->value('notify_emails'));

        Mail::send('emails.review', ['emails', $emails], function($msg) use ($emails){
            $msg->from('parfumhouse@parfumhouse.com.ua', 'Интернет-магазин Parfum House');
            $msg->to($emails);
            $msg->subject('Новый отзыв на сайте!');
        });

        return response()->json(['success' => 'Ваш отзыв успешно добавлен! Он появится на сайте после проверки администратором!']);
    }
}
