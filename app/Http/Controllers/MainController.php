<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use App\Modules;
use App\ModuleBestsellers;
use App\Products;
use App\ModuleSlideshow;
use App\Blog;
use Carbon\Carbon;
use App\Http\Requests;

class MainController extends Controller
{
    public function index(Carbon $carbon)
    {

        $modules_settings = Modules::all();

        foreach ($modules_settings as $module_setting) {
            if ($module_setting->alias_name == 'latest') {
                $latest_settings = json_decode($module_setting->settings);
                $latest_status = $module_setting->status;
            } elseif ($module_setting->alias_name == 'bestsellers') {
                $bestseller_settings = json_decode($module_setting->settings);
                $bestseller_status = $module_setting->status;
            } elseif ($module_setting->alias_name == 'slideshow') {
                $slideshow_settings = json_decode($module_setting->settings);
                $slideshow_status = $module_setting->status;
            }
        }

        $latest_products = Products::orderBy('created_at', 'desc')->take($latest_settings->quantity)->get();

        if (!$latest_products->isEmpty()) {
            foreach ($latest_products as $product) {
                $product->reviews = $product->reviews->where('published', 1);
                if ($product->sale) {
                    $sale = ($product->price / 100) * $product->sale;
                    $product->price = $product->price - $sale;
                }
            }
        }


        $bestsellers = ModuleBestsellers::inRandomOrder()->take($bestseller_settings->quantity)->inRandomOrder()->get();
        $bestseller_products = [];

        if (!$bestsellers->isEmpty()) {
            foreach ($bestsellers as $bestseller) {
                $bestseller_products[] = Products::where('id', $bestseller->product_id)->first();
            }
            foreach ($bestseller_products as $product) {
                if (!empty($product) && $product->sale) {
                    $sale = ($product->price / 100) * $product->sale;
                    $product->price = $product->price - $sale;
                }
            }

        }


        $slideshow = ModuleSlideshow::where('status', 1)->orderBy('sort_order', 'asc')->take($slideshow_settings->quantity)->get();

        $articles = Blog::where('published', 1)->orderBy('updated_at', 'desc')->take(2)->get();

        setlocale(LC_TIME, 'RU');

        foreach ($articles as $key => $article) {
            $articles[$key]->date = iconv("cp1251", "UTF-8", $articles[$key]->updated_at->formatLocalized('%d %b %Y'));
        }

        return view('index')
            ->with('settings', Settings::find(1))
            ->with('latest_products', $latest_products)
            ->with('latest_status', $latest_status)
            ->with('bestseller_products', $bestseller_products)
            ->with('bestseller_status', $bestseller_status)
            ->with('slideshow', $slideshow)
            ->with('slideshow_status', $slideshow_status)
            ->with('articles', $articles);
    }
}
