<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index');
Route::get('/page/{alias}', 'HTMLContentController@show');
Route::get('/categories/{alias}', 'CategoriesController@show');
Route::get('/articles', 'BlogController@showAll');
Route::get('/articles/{alias}', 'BlogController@show');
Route::get('/order', 'OrdersController@show');
//Route::post('/order/start_data', 'OrdersController@startData');
Route::post('/order/start_data', 'OrdersController@createOrder');
Route::get('/thank_you', 'OrdersController@thank_you');
Route::match(['get', 'post'], '/search', ['as' => 'search', 'uses' => 'ProductsController@search']);

Route::post('/checkout', 'OrdersController@createOrder');
Route::post('/checkout/cities', 'OrdersController@getCities');
Route::post('/checkout/warehouses', 'OrdersController@getWarehouses');
Route::post('/checkout/confirm', 'OrdersController@orderConfirmation');
Route::get('/checkout/complete', 'OrdersController@orderComplete');
Route::post('/api/liqpay/callback', 'OrdersController@liqpayCallback');

Route::post('/callback', 'CallbackController@index');
Route::post('/quest', 'CallbackController@quest');

Route::get('/product/{alias}', 'ProductsController@show');
Route::post('/review/add', 'ReviewsController@add');

Route::get('/import', 'ImportController@show');


/**
 * Authorization routing
 */
Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@authenticate');
Route::get('/logout', 'LoginController@logout');
Route::get('/register', 'LoginController@register');
Route::post('/register', 'LoginController@store');
Route::get('/forgotten', 'LoginController@forgotten');
Route::post('/forgotten', 'LoginController@reminder');
Route::get('/lostpassword', 'LoginController@lostpassword');
Route::post('/lostpassword', 'LoginController@changePassword');

/**
 * Admin routing
 */
Route::group(['middleware' => ['admin'], 'prefix' => 'admin'], function(){
    Route::get('/', 'AdminController@dash');
    Route::get('/products', 'ProductsController@index');

    Route::get('/settings', 'SettingsController@index');
    Route::post('/settings', 'SettingsController@update');

    Route::post('/upload_attribute_image', 'AttributesController@upload_image');

    Route::group(['prefix' => 'categories'], function(){
        Route::get('/', 'CategoriesController@index');
        Route::get('/create', 'CategoriesController@create');
        Route::post('/create', 'CategoriesController@store');
        Route::get('/delete/{id}', 'CategoriesController@destroy');
        Route::get('/edit/{id}', 'CategoriesController@edit');
        Route::post('/edit/{id}', 'CategoriesController@update');
    });

    Route::group(['prefix' => 'attributes'], function(){
        Route::get('/', 'AttributesController@index');
        Route::get('/create', 'AttributesController@create');
        Route::post('/create', 'AttributesController@store');
        Route::get('/delete/{id}', 'AttributesController@destroy');
        Route::get('/edit/{id}', 'AttributesController@edit');
        Route::post('/edit/{id}', 'AttributesController@update');
    });

    Route::group(['prefix' => 'products'], function(){
        Route::get('/', 'ProductsController@index');
        Route::get('/create', 'ProductsController@create');
        Route::post('/create', 'ProductsController@store');
        Route::get('/delete/{id}', 'ProductsController@destroy');
        Route::get('/edit/{id}', 'ProductsController@edit');
        Route::post('/edit/{id}', 'ProductsController@update');
        Route::get('/table', 'ProductsController@table');
        Route::get('/getattributevalues', 'ProductsController@getAttributes');
        Route::post('/getattributevalues', 'ProductsController@getAttributeValues');

    });
    Route::match(['get', 'post'], '/upload-products', 'ProductsController@upload');

    Route::group(['prefix' => 'articles'], function(){
        Route::get('/', 'BlogController@index');
        Route::get('/create', 'BlogController@create');
        Route::post('/create', 'BlogController@store');
        Route::get('/edit/{id}', 'BlogController@edit');
        Route::post('/edit/{id}', 'BlogController@update');
        Route::get('/delete/{id}', 'BlogController@destroy'); //softDelete
    });

    Route::group(['prefix' => 'users'], function(){
        Route::get('/', 'UserController@index');
        Route::get('/create', 'UserController@create');
        Route::post('/create', 'UserController@store');
        Route::get('/edit/{id}', 'UserController@edit');
        Route::post('/edit/{id}', 'UserController@update');
        Route::get('/stat/{id}', 'UserController@statistic');
        Route::get('/reviews/{id}', 'UserController@reviews');
        Route::get('/wishlist/{id}', 'UserController@adminWishlist');
        Route::get('/delete/{id}', 'UserController@destroy'); //softDelete
    });

    Route::get('/managers', 'UserController@managers');

    Route::group(['prefix' => 'orders'], function(){
        Route::get('/', 'OrdersController@index');
        Route::get('/create', 'OrdersController@create');
        Route::post('/create', 'OrdersController@store');
        Route::get('/edit/{id}', 'OrdersController@edit');
        Route::post('/edit/{id}', 'OrdersController@update');
        Route::post('/stat/{id}', 'OrdersController@statistic');
        Route::get('/delete/{id}', 'OrdersController@destroy'); //softDelete
    });
    Route::group(['prefix' => 'html'], function(){
        Route::get('/', 'HTMLContentController@index');
        Route::get('/create', 'HTMLContentController@create');
        Route::post('/create', 'HTMLContentController@store');
        Route::get('/edit/{id}', 'HTMLContentController@edit');
        Route::post('/edit/{id}', 'HTMLContentController@update');
        Route::get('/delete/{id}', 'HTMLContentController@destroy'); //softDelete
    });
    Route::group(['prefix' => 'measures'], function(){
        Route::get('/', 'MeasuresController@index');
        Route::get('/create', 'MeasuresController@create');
        Route::post('/create', 'MeasuresController@store');
        Route::get('/edit/{id}', 'MeasuresController@edit');
        Route::post('/edit/{id}', 'MeasuresController@update');
        Route::get('/delete/{id}', 'MeasuresController@destroy'); //softDelete
    });
    Route::group(['prefix' => 'parfum-groups'], function(){
        Route::get('/', 'ParfumGroupsController@index');
        Route::get('/create', 'ParfumGroupsController@create');
        Route::post('/create', 'ParfumGroupsController@store');
        Route::get('/edit/{id}', 'ParfumGroupsController@edit');
        Route::post('/edit/{id}', 'ParfumGroupsController@update');
        Route::get('/delete/{id}', 'ParfumGroupsController@destroy'); //softDelete
    });
    Route::group(['prefix' => 'parfum-notes'], function(){
        Route::get('/', 'ParfumNotesController@index');
        Route::get('/create', 'ParfumNotesController@create');
        Route::post('/create', 'ParfumNotesController@store');
        Route::get('/edit/{id}', 'ParfumNotesController@edit');
        Route::post('/edit/{id}', 'ParfumNotesController@update');
        Route::get('/delete/{id}', 'ParfumNotesController@destroy'); //softDelete
    });


    Route::group(['prefix' => 'modules'], function(){
        Route::get('/', 'ModulesController@index');
        Route::get('/settings/{name}', function($name) {
            $controller = App::make('\App\Http\Controllers\Module' . ucfirst($name) . 'Controller');
            return $controller->callAction('index', []);
        });
        Route::post('/settings/{name}', function($name) {
            $controller = App::make('\App\Http\Controllers\Module' . ucfirst($name) . 'Controller');
            return $controller->callAction('save', []);
        });

    });
    Route::group(['prefix' => 'reviews'], function(){
        Route::get('/', 'ReviewsController@index');
        Route::get('/show/{id}', 'ReviewsController@show');
        Route::post('/show/{id}', 'ReviewsController@update');
        Route::get('/delete/{id}', 'ReviewsController@destroy'); //softDelete
    });

    Route::get('/loadimages', 'ImagesController@loadImages');
    Route::post('/upload', 'ImagesController@uploadImages');

    Route::group(['prefix' => 'images'], function(){
        Route::post('/start_updating', 'ImagesController@startUpdatingImages');
        Route::post('/update_sizes', 'ImagesController@updateImageSize');
        Route::post('/remove_images', 'ImagesController@removeImages');
    });
});
Route::post('wishlist/update','WishListController@update');
Route::post('wishlist/del','WishListController@delWishlist');

Route::post('cart/update','CartController@addToCart');
Route::post('cart/updateAll','CartController@update');
Route::post('cart/get','CartController@getCartData');



/**
 * Users routing
 */
Route::group(['prefix' => 'user', 'middleware' => ['user']], function(){
    Route::get('/', 'UserController@show');
    Route::get('/history', 'UserController@history');
    Route::get('/wish_list', 'UserController@wishList');
    Route::get('/change-data', 'UserController@changeData');
    Route::post('/change-data', 'UserController@saveChangedData');
});
Route::post('/subscribe', 'UserController@subscribe');
Route::get('/livesearch', 'ProductsController@livesearch');

Route::get('/fuck', 'UserController@fuck');

Route::post('/filtered/{alias}', 'CategoriesController@filtered');