<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDescription extends Model
{
    protected $table = 'product_description';

    protected $fillable = [
        'product_id',
        'product_description',
        'product_description_image_id',
        'upper_note',
        'heart_note',
        'base_note',
        'character',
        'gallery',
    ];

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Products');
    }

    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'product_description_image_id');
    }
}
