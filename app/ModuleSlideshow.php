<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleSlideshow extends Model
{
    protected $table = 'module_slideshow';
    protected $fillable = [
        'image_id',
        'sort_order',
        'link',
        'enable_link',
        'status'
    ];

    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'image_id');
    }

}
