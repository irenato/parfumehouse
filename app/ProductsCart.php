<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsCart extends Model
{
    protected $table = 'products_cart';
    protected $fillable = [
        'cart_id',
        'product_id',
        'product_quantity'
    ];

    public $timestamps = false;

    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }

    public function product()
    {
//        return $this->hasMany('App\Products', 'id','product_id');
        return $this->belongsTo('App\Products');
    }
}
