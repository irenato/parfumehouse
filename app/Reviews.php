<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reviews extends Model
{
    use SoftDeletes;

    protected $table = 'products_review';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id',
        'product_id',
        'grade',
        'review',
        'advantages',
        'flaws',
        'new',
        'published'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function product()
    {
        return $this->hasOne('App\Products', 'id', 'product_id');
    }
}
