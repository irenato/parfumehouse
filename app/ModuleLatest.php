<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleLatest extends Model
{
    protected $table = 'module_new_products';
    protected $fillable = ['product_id'];

    public function product()
    {
        return $this->hasOne('App\Products', 'id', 'product_id');
    }
}
