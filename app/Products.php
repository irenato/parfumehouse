<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Pagination\LengthAwarePaginator;
use Cartalyst\Support\Collection;
use App\Image;

class Products extends Model
{

    use SoftDeletes;

    protected $fillable =[
        'name',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'url_alias',
        'articul',
        'price',
        'sale',
        'quantity',
        'capacity',
        'measure_id',
        'product_category_id',
        'product_related_category_id',
        'original_image_id',
        'image_id',
        'stock',
        'rating',
        'label'
    ];

    protected $table = 'products';
    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->hasOne('App\Categories', 'id', 'product_category_id');
    }

    public function original_image()
    {
        return $this->hasOne('App\Image', 'id', 'original_image_id');
    }

    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'image_id');
    }

    public function get_product_image()
    {
        $image = $this->image_id ? 'image_id' : 'original_image_id';
        return $this->hasOne('App\Image', 'id', $image);
    }

    public function description()
    {
        return $this->hasOne('App\ProductDescription', 'product_id');
    }

    public function attributes()
    {
        return $this->hasMany('App\ProductAttributes', 'product_id');
    }

    public function measures()
    {
        return $this->hasOne('App\Measures', 'id', 'measure_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\ProductsReview', 'product_id');
    }
    public function wishlist()
    {
        return $this->hasMany('App\Wishlist', 'product_id');
    }
    public function products_cart()
    {
        return $this->hasOne('App\ProductsCart', 'product_id');
    }

    public function gallery_images(){
        $images_ids = json_decode($this->description->gallery);
        $images = new Collection(0);

        if(!empty($images_ids)) {
            $image = new Image;
            $images = $image->whereIn('id', $images_ids)->get();
        }

        return $images;
    }

    /**
     * Поиск товаров
     * @param string $text Поисковый текст
     * @param int $page Номер страницы
     * @param int $count Колличество на странице
     * @return LengthAwarePaginator
     */
    public function search($text = '', $page = 1, $count = 8)
    {
        $title = $this->where('stock', 1)->where('name', 'like', '%'.$text.'%')
            ->orWhere('articul', 'like', '%'.$text.'%');
        $upper_note = $this->select('products.*')->join('product_description', 'products.id', '=', 'product_description.product_id')
            ->where('product_description.upper_note', 'like', '%'.$text.'%')
            ->orderBy('products.updated_at', 'desc');
        $heart_note = $this->select('products.*')->join('product_description', 'products.id', '=', 'product_description.product_id')
            ->where('product_description.heart_note', 'like', '%'.$text.'%')
            ->orderBy('products.updated_at', 'desc');
        $base_note = $this->select('products.*')->join('product_description', 'products.id', '=', 'product_description.product_id')
            ->where('product_description.base_note', 'like', '%'.$text.'%')
            ->orderBy('products.updated_at', 'desc');

        $paginate = 1;
        $all_products = $title->union($upper_note)->union($heart_note)->union($base_note);


        $all_count = $upper_note->count() + $heart_note->count() + $base_note->count() + $this->where('name', 'like', '%'.$text.'%')->orWhere('articul', 'like', '%'.$text.'%')->count();

        $data = new LengthAwarePaginator($all_products->skip($count*($page-1))->take($count)->get(), ceil($all_count/$count), $paginate, $page, array('path' => 'search/'));
        foreach ($data as $product){
            if($product->sale){
                $sale = ($product->price / 100) * $product->sale;
                $product->price = $product->price - $sale;
            }
        }
        return $data;

//        return $this->join('product_description', 'products.id', '=', 'product_description.product_id')
//            ->where('products.name', 'like', '%'.$text.'%')
//            ->orWhere('product_description.upper_note', 'like', '%'.$text.'%')
//            ->orWhere('product_description.middle_note', 'like', '%'.$text.'%')
//            ->orWhere('product_description.bottom_note', 'like', '%'.$text.'%')
//            ->orderBy('products.updated_at', 'desc')
//            ->paginate(8);
    }

    public function create_product_thumnail($id)
    {
        $product = $this->find($id);

        $overlay = [];
        foreach ($product->get_attributes as $attribute) {
            $attribute_info = Attributes::find($attribute->attribute_id);

            if($attribute_info->enable_image_overlay) {
                $image = AttributeValues::find($attribute->attribute_value_id);

                $overlay[$attribute->attribute_id]['images'][] = $image->image_href;
                $overlay[$attribute->attribute_id]['settings'] = unserialize($attribute_info->image_overlay_settings);

            }
        }

        if (!empty($overlay)) {
            $image = new Image;
            $id = $image->create_thumbnail(public_path() . '/assets/images/' . $product->original_image->href, $overlay);

            $product->image_id = $id;
            $product->save();
        }
    }

    public function get_attributes()
    {
        return $this->hasMany('App\ProductAttributes', 'product_id');
    }
    
    /**
     * Создание продукта
     * @param $data
     * @return string
     */
    public function insert_product($data)
    {
        if($this->isset_product_with_url_alias($data['url_alias']) == false) {
            $result_data = $this->get_prepared_data($data);

            $id = $this->insertGetId($result_data['product']);
            $product = $this->find($id);
            $product->description()->save(new ProductDescription($result_data['product_description']));
            if(!empty($result_data['product_attributes']))
                $product->attributes()->createMany($result_data['product_attributes']);
            $product->create_product_thumnail($id);
            return $id;
        } else {
            return 'already_exist';
        }
    }

    /**
     * Обновление продукта
     * @param $data
     * @return string
     */
    public function update_product($data)
    {
        if($this->isset_product_with_url_alias($data['url_alias']) == false) {
            $id = $this->insert_product($data);
        }else{
            $result_data = $this->get_prepared_data($data);

            $product = $this->select('id')
                ->where('url_alias', $data['url_alias'])
                ->take(1)
                ->get()
                ->first();

            $id = $product->id;

            $this->where('id', $id)->update($result_data['product']);
            $product->description()->where('product_id', $id)->update($result_data['product_description']);
            $product->attributes()->where('product_id', $id)->delete();
            if (!empty($result_data['product_attributes']))
                $product->attributes()->createMany($result_data['product_attributes']);
            $product->create_product_thumnail($id);
        }
        return $id;
    }

    public function get_prepared_data($data)
    {
        $fills = [
            'product' => $this->fillable,
            'product_description' => [
                'product_description',
                'product_description_image_id',
                'upper_note',
                'heart_note',
                'base_note',
                'character'
            ],
            'product_attributes' => [
                'attribute_id',
                'attribute_value_id'
            ]
        ];

        foreach (['image_id', 'rating'] as $key) {
            while (($i = array_search($key, $fills['product'])) !== false) {
                unset($fills['product'][$i]);
            }
        }

        $result_data = [];
        foreach ($fills as $table => $table_fills) {
            $result_data[$table] = [];
            if ($table != 'product_attributes') {
                foreach ($table_fills as $fill) {
                    $result_data[$table][$fill] = isset($data[$fill]) ? $data[$fill] : ($fill == 'stock' ? 0 : null);
                }
            } else {
                if(isset($data['product_attributes'] )) {
                    foreach ($data['product_attributes'] as $attribute) {
                        $attributes = [];
                        foreach ($table_fills as $fill) {
                            $attributes[$fill] = $attribute[$fill] ? $attribute[$fill] : null;
                        }
                        $result_data[$table][] = $attributes;
                    }
                }
            }
        }

        return $result_data;
    }

    /**
     * Проверка существования продукта с таким url_alias
     * @param $url_alias
     * @return bool
     */
    public function isset_product_with_url_alias($url_alias)
    {
        return (bool)$this->where('url_alias', $url_alias)
            ->take(1)
            ->count();
    }
}