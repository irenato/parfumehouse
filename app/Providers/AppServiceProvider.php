<?php

namespace App\Providers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

use App\HTMLContent;
use App\Settings;
use App\Categories;
use App\Wishlist;
use App\Cart;
use App\User;
use App\Reviews;
use App\Order;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    private $user;
    private $products = array();
    public function boot()
    {
        $this->user = Sentinel::getUser();
        if(!is_null($this->user)) {
            view()->composer('admin.layouts.main', function ($view) {
                $view->with('user', $this->user)->with('orders', Order::where('status_id', 1)->count());
            });
            view()->composer('public.order', function ($view) {
                $view->with('user', User::find($this->user->id));
            });

            view()->composer('public.layouts.header-middle', function ($view) {
                $view->with('user_logged', $this->user);
            });

            $wish_list = Wishlist::where('user_id',$this->user->id)->get();
            foreach($wish_list as $val){
                $this->products[] = $val->product_id;
            }

            view()->composer([
                'public.layouts.header-main',
                'public.layouts.header-middle',
                'public.layouts.product',
				'public.layouts.product-list',
                'public.layouts.product_small',
                'public.product'
            ], function ($view) {
                $settings = Settings::find(1);
                $view->with('user_id', $this->user->id)
                    ->with('user_wishlist',$this->products)
                    ->with('user_logged', true)
                    ->with('rate', $settings->rate);
            });

        } else {
            view()->composer([
                'public.layouts.header-main',
                'public.layouts.header-middle',
                'public.layouts.product',
				'public.layouts.product-list',
                'public.product'
                ], function ($view) {
                $view->with('user_logged', false);
            });

            view()->composer([
                    'public.layouts.product',
                    'public.layouts.product_small',
					'public.layouts.product-list',
                    'public.product'
                ], function ($view) {
                $settings = Settings::find(1);
                $view->with('user_id', 0)->with('user_wishlist',[])
                    ->with('rate', $settings->rate);
            });
        }

        view()->composer('public.layouts.header-middle', function ($view) {
            $settings = Settings::find(1);
            if(Session::has('user_cart_id')){
                $user_cart_id = Session::get('user_cart_id');
                $products_quantity = Cart::where('user_cart_id', $user_cart_id)->first() ? Cart::where('user_cart_id', $user_cart_id)->first()->products_quantity : 0;
                $products_sum = Cart::where('user_cart_id', $user_cart_id)->first() ? Cart::where('user_cart_id', $user_cart_id)->first()->products_sum * $settings->rate: 0;

            }else{
                $products_quantity = 0;
                $products_sum = 0;
            }
            $view->with('html_pages', HTMLContent::where('status', 1)->orderBy('sort_order', 'asc')->get())
            ->with('products_quantity', $products_quantity)
                ->with('products_sum', $products_sum);
        });
        view()->composer('public.layouts.header-main', function ($view) {
            $view->with('html_pages', HTMLContent::where('status', 1)->orderBy('sort_order', 'asc')->get());
        });

        view()->composer('public.layouts.footer', function ($view) {
            $view->with('html_pages', HTMLContent::where('status', 1)->orderBy('sort_order', 'asc')->get());
        });

        view()->composer([
            'public/*',
            'users/*',
            'errors/*',
            'index',
            'login',
            'register',
            'forgotten'
        ], function ($view) {
            $settings = Settings::find(1);
            $socials = $settings->socials ? json_decode($settings->socials) : null;

            $view->with('settings', $settings)
                ->with('socials', $socials)
                ->with('categories', Categories::where('status', 1)->orderBy('sort_order', 'asc')->get());
        });

        view()->composer('admin.layouts.sidebar', function($view) {
            $view->with('new_reviews', Reviews::where('new', 1)->get());
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
