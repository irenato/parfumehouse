<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keywords',
        'about',
        'text',
        'main_phone_1',
        'main_phone_2',
        'other_phones',
        'notify_emails',
        'socials',
        'product_list_image_width',
        'product_list_image_height',
        'product_image_width',
        'product_image_height',
        'blog_image_width',
        'blog_image_height',
        'slide_image_width',
        'slide_image_height',
        'rate'
    ];

    protected $table = 'settings';
    public $timestamps = false;


    /**
     * Получение настроек изображений
     * @return array
     */
    public function get_images_sizes(){
        // TODO: Обединить все настройки изображений в одну колонку в виде json строки
        $settings = $this->select('product_list_image_width',
            'product_list_image_height',
            'product_image_width',
            'product_image_height',
            'blog_image_width',
            'blog_image_height',
            'slide_image_width',
            'slide_image_height')->first()->toArray();
        $registered_sizes = ['product_list' => ['w' => $settings['product_list_image_width'], 'h' => $settings['product_list_image_height']],
            'product' => ['w' => $settings['product_image_width'], 'h' => $settings['product_image_height']],
            'blog' => ['w' => $settings['blog_image_width'], 'h' => $settings['blog_image_height']],
            'slide' => ['w' => $settings['slide_image_width'], 'h' => $settings['slide_image_height']]];

        return $registered_sizes;
    }
}
