<?php

namespace App;

//use Illuminate\Foundation\Auth\User as Authenticatable;
//use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

//class User extends Authenticatable
class User extends \Cartalyst\Sentinel\Users\EloquentUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
		'quest_time',
    ];
    /**
     * Array of login column names.
     *
     * @var array
     */
    protected $loginNames = ['phone'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function blog()
    {
        return $this->hasOne('App\Blog', 'user_id', 'id');
    }
    public function user_data()
    {
//        return $this->belongsTo('App\UserData', 'user_id', 'id');
        return $this->hasOne('App\UserData', 'user_id', 'id');
    }
    public function orders()
    {
        return $this->hasMany('App\Order', 'user_id', 'id');
    }
    public function wishlist()
    {
        return $this->hasMany('App\Wishlist', 'user_id', 'id');
    }
    public function reviews()
    {
        return $this->hasMany('App\Reviews', 'user_id', 'id');
    }
    public function checkIfUnregistered($phone){
        return $this->where('phone', $phone)->first();
    }
}
