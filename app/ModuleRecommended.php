<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleRecommended extends Model
{
    protected $table = 'module_recommended';

    protected $fillable = [
        'product_manufacturer',
        'product_id',
    ];

    public function product()
    {
        return $this->hasOne('App\Products', 'id', 'product_id');
    }
}
