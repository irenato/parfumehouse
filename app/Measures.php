<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Measures extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'measures';

    public $fillable = [
        'name',
        'status'
    ];

}
