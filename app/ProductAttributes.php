<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttributes extends Model
{
    protected $table = 'product_attributes';

    protected $fillable = [
        'product_id',
        'attribute_id',
        'attribute_value_id'
    ];

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Products');
    }

    public function info()
    {
        return $this->hasOne('App\Attributes', 'id', 'attribute_id');
    }

    public function value()
    {
        return $this->hasOne('App\AttributeValues', 'id', 'attribute_value_id');
    }

}
