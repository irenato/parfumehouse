<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $table = 'users_data';

    protected $fillable = [
        'user_id',
        'image_id',
        'adress',
        'company',
        'other_data',
        'subscribe'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function image()
    {
        return $this->belongsTo('App\Image');
    }
}
