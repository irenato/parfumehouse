<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //$table->integer('user_id')->nullable();
    //$table->string('user_session_id');
    //$table->text('user_data');
    //$table->text('cart_data');
    protected $table = 'cart';
    protected $fillable = [
        'user_id',
        'user_cart_id',
        'user_ip',
        'products_quantity',
        'products_sum',
        'user_data',
        'cart_data'
    ];


    public function products_cart()
    {
        return $this->hasMany('App\ProductsCart', 'cart_id', 'id');
    }
}
