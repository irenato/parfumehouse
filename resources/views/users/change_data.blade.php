@extends('public.layouts.main')

@section('meta')
    <title>{!! $settings->meta_title !!}</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('change_user') !!}
@endsection

@section('content')

    <main>
        <div class="container">
            <section class="account-content">
                <h2 class="section-title">Личные данные</h2>
                <div class="row">
                    <div class="col-md-8 col-md-push-2">
                        <div class="registration-content">
                            @if(!empty($errors->all()))
                                <span class="error-message">
                                {!! $errors->first() !!}
                            </span>
                            @endif
                            <form class="registration-form" method="post">
                                {!! csrf_field() !!}
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="name">Имя*</label>
                                    <input type="text"
                                           name="first_name"
                                           id="name"
                                           class="registration-form__input @if($errors->has('first_name')) input_error @endif"
                                           value="{!! old('first_name') ? old('first_name') : $user->first_name !!}">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="surname">Фамилия</label>
                                    <input type="text"
                                           name="last_name"
                                           id="surname"
                                           class="registration-form__input"
                                           value="{!! old('last_name') ? old('last_name') : $user->last_name !!}">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="phone">Телефон*</label>
                                    <input type="text"
                                           name="phone"
                                           id="phone"
                                           class="registration-form__input @if($errors->has('phone')) input_error @endif"
                                           value="{!! old('phone') ? old('phone') : $user->phone !!}">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="email">Email*</label>
                                    <input type="text"
                                           name="email"
                                           id="email"
                                           class="registration-form__input @if($errors->has('email')) input_error @endif"
                                           value="{!! old('email') ? old('email') : $user->email !!}">
                                </div>
                                
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="address">Адрес</label>
                                    <input class="registration-form__input"
                                              name="adress"
                                              id="address"
                                              value="{!! old('address') ? old('address') : $user->user_data->adress !!}" />
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="company">Компания</label>
                                    <input class="registration-form__input"
                                              name="company"
                                              id="company"
                                              value="{!! old('company') ? old('company') : $user->user_data->company !!}" />
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="password">Новый пароль</label>
                                    <input type="password"
                                           name="password"
                                           id="password"
                                           class="registration-form__input @if($errors->has('password')) input_error @endif">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="passwordr">Повторите новый пароль</label>
                                    <input type="password"
                                           name="password_confirmation"
                                           id="passwordr" class="registration-form__input @if($errors->has('password_confirmation')) input_error @endif">
                                </div>
                                <span class="registration-form__note">*Поля обязательные для заполнения</span>

                                <div class="registration-form__input-wrap">
                                    <input type="checkbox"
                                           name="subscribe"
                                           id="subscribe"
                                           class="product-filter__checkbox"
                                           @if($user->user_data->subscribe) checked @endif
                                           hidden />
                                    <label for="subscribe" class="product-filter__check-label">Подписаться на новости</label>
                                </div>

                                <button type="submit" class="product__btn registration-form__btn">Применить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

@endsection