@extends('users.layouts.default')

@section('breadcrumbs')
    {!! Breadcrumbs::render('user') !!}
@endsection

@section('content-user')
    <section class="personal-data">
        <div class="personal-data-top clearfix">
            <h4 class="sidebar-title sidebar-title_personal">Личные данные</h4>
            <a href="/user/change-data" class="personal-data__change-link">Изменить личные данные</a>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="personal-data__wrap">
                    <span class="personal-data__item">Ваше имя</span>
                    <span class="personal-data__item"><strong>{{ $user->first_name ? $user->first_name : '' }}</strong></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="personal-data__wrap">
                    <span class="personal-data__item">Ваша фамилия</span>
                    <span class="personal-data__item"><strong>{{ $user->last_name ? $user->last_name : '' }}</strong></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="personal-data__wrap">
                    <span class="personal-data__item">Телефон</span>
                    <span class="personal-data__item"><strong>{!! $user->phone ? $user->phone : '' !!}</strong></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="personal-data__wrap">
                    <span class="personal-data__item">E-mail</span>
                    <span class="personal-data__item"><strong>{{ $user->email ? $user->email : '' }}</strong></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="personal-data__wrap">
                    <span class="personal-data__item">Адрес</span>
                    <span class="personal-data__item"><strong>
                        {{ $address }}
                </strong></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="personal-data__wrap">
                    <span class="personal-data__item">Компания</span>
                    <span class="personal-data__item"><strong>{!! !is_null($user->user_data->company) ? $user->user_data->company : '' !!}</strong></span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="personal-data__wrap">
                    <span class="personal-data__item">Подписка на новости</span>
                    <span class="personal-data__item"><strong>{!! $user->user_data->subscribe ? 'Да' : 'Нет' !!}</strong></span>
                </div>
            </div>
        </div>
    </section>

@endsection