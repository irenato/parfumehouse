@extends('public.layouts.main')

@section('meta')
    <title>Восстановление пароля</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('forgotten') !!}
@endsection

@section('content')
    <main >

        <section class="account-content">
            <h2 class="section-title">Восстановление пароля</h2>
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <div class="registration-content">
                        <p class="autorization-form__label">Восстановление пароля завершено, теперь Вы можете авторизоваться на сайте, используя новый пароль.</p>
                        <a href="/login" class="main-btn">Авторизоваться</a>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection