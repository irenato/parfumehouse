@extends('public.layouts.main')

@section('meta')
    <title>Восстановление пароля</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('forgotten') !!}
@endsection

@section('content')

    <main>
        <div class="container">
            <section class="account-content">
                <h2 class="section-title">Восстановление пароля</h2>
                <div class="row">
                    <div class="col-md-8 col-md-push-2">
                        <div class="registration-content">
                            <span class="autorization-form__label">На Вашу почту было отправлено письмо. Для завершения процедуры восстановления пароля, пожалуйста, перейдите по ссылке, указанной в письме.</span>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

@endsection