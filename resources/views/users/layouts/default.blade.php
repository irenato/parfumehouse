@extends('public.layouts.main')

@section('meta')
    <title>{!! $settings->meta_title !!}</title>
    <meta name="description" content="description">
    <meta name="keywords" content="keywords">
@endsection

@section('content')
    <main class="account-wrap">

        @if (session('status'))
            <div class="container">
                <span class="autorization-message">{!! session('status') !!}
                    <i class="autorization-message__close" data-dismiss="alert" aria-label="close">&#xe806;</i>
                </span>
            </div>
        @endif

        <div class="container">
            <section class="account-content">
                <h2 class="section-title">Личный кабинет</h2>
                <div class="row">
                    <div class="col-md-3">
                        <ul class="account-sidebar">
                            <li class="accout-sidebar__list-item"><i class="header-middle__search-icon account-sidebar__list-icon">&#xe807;</i><a href="/user" class="accout-sidebar__list-link">Личные данные</a></li>
                            <li class="accout-sidebar__list-item"><i class="header-middle__search-icon account-sidebar__list-icon">&#xe80e;</i><a href="/user/history" class="accout-sidebar__list-link">История заказов</a></li>
                            <li class="accout-sidebar__list-item"><i class="header-middle__search-icon account-sidebar__list-icon">&#xe80a;</i><a href="/user/wish_list" class="accout-sidebar__list-link">Список желаний</a></li>
                            {{--<li class="accout-sidebar__list-item"><i class="header-middle__search-icon account-sidebar__list-icon">&#xe80f;</i><a href="#" class="accout-sidebar__list-link">Просмотренные товары</a></li>--}}
                        </ul>
                    </div>
                    <div class="col-md-9">
                        @yield('content-user')
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection