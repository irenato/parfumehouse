@extends('users.layouts.default')

@section('breadcrumbs')
    {!! Breadcrumbs::render('wishlist') !!}
@endsection

@section('content-user')
    <section class="favorites">
        <div class="favorites__top clearfix">
            <h4 class="sidebar-title sidebar-title_favorites">Список желаний</h4>
            <form class="sorting-form">
                <label for="sorting-form__select" class="sorting-form__label">Период</label>
                <select name="sorting-form__select">
                    <option selected>за сегодня</option>
                    <option>за неделю</option>
                    <option>за месяц</option>
                </select>
            </form>
        </div>
            <div class="row">
                @forelse($wish_list as $product)
                    @include('public.layouts.product', ['product' => $product->product, 'wishlist' => true])
                @empty
                    <article class="order">
                        <h5 class="order__title">Тут будут отображаться избранные вами товары</h5>
                    </article>
                @endforelse
            </div>
    </section>
@endsection