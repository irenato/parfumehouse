@extends('users.layouts.default')

@section('breadcrumbs')
    {!! Breadcrumbs::render('history') !!}
@endsection

@section('content-user')

    <section class="favorites">
        <div class="favorites__top clearfix">
            <h4 class="sidebar-title sidebar-title_favorites">История заказов</h4>
            {{--<form class="sorting-form">--}}
                {{--<label for="sorting-form__select" class="sorting-form__label">Период</label>--}}
                {{--<select name="sorting-form__select">--}}
                    {{--<option selected>за сегодня</option>--}}
                    {{--<option>за неделю</option>--}}
                    {{--<option>за месяц</option>--}}
                {{--</select>--}}
            {{--</form>--}}
        </div>
        @forelse($orders as $order)
        <article class="order">
            <h5 class="order__title">Заказ №{{ $order->id }}, {{ $order->created_at }}</h5>
            <table class="cart-table">

                @foreach($order->products as $product)
                    @if(!is_null($product->product))
                        <tr class="cart-table__row">
                            <td class="cart-table__item cart-table__thumb">
                                <a href="/product/{{ $product->product->url_alias }}" class="cart-table__thumb-link" style="background-image: url('/assets/images/{{ $product->product->get_product_image->href }}') ">
                                </a>
                                <a href="/product/{{ $product->product->url_alias }}" class="cart-table__delete-link cart-table__delete-link_mobile"><i class="header-middle__search-icon cart-table__delete-icon">&#xe806;</i></a>
                            </td>
                            <td class="cart-table__item cart-table__name"><span class="cart-table__name_accent">{{ $product->product->name }}</span></td>
                            <td class="cart-table__item cart-table__val">{{ $product->product->capacity }} {{ $product->product->measures->name }}</td>
                            <td class="cart-table__item cart-table__quantity">{{ $product->product_quantity }} шт.</td>
                            <td class="cart-table__item cart-table__price">{{ $product->product_sum }} $</td>
                            <td class="cart-table__item cart-table__delete"> </td>
                        </tr>
                    @endif
                @endforeach

                <tr class="cart-table__row">
                    <td class="cart-table__item cart-table__thumb">
                    </td>
                    <td class="cart-table__item cart-table__name"><span class="cart-table__name_accent"><b>Итого: </b></span></td>
                    <td class="cart-table__item cart-table__val"></td>
                    <td class="cart-table__item cart-table__quantity">{{ $order->products_quantity }} шт.</td>
                    <td class="cart-table__item cart-table__price">{{ $order->products_sum }} $</td>
                    <td class="cart-table__item cart-table__delete"></td>
                </tr>
            </table>
        </article>
        @empty
            <article class="order">
                <h5 class="order__title">Тут будет отображаться ваши заказы</h5>
            </article>
        @endforelse
    </section>
@endsection