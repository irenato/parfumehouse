@extends('public.layouts.main')

@section('meta')
    <title>Восстановление пароля</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('forgotten') !!}
@endsection

@section('content')

    <main>
        <div class="container">
            <section class="account-content">
                <h2 class="section-title">Восстановление пароля</h2>
                <div class="row">
                    <div class="col-md-8 col-md-push-2">
                        <div class="registration-content">
                            @if(!empty($errors->all()))
                                <span class="error-message">
                                {!! $errors->first() !!}
                            </span>
                            @endif
                            <form class="registration-form" method="post">
                                {!! csrf_field() !!}
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="email">Email</label>
                                    <input type="text"
                                           name="email"
                                           id="email"
                                           class="registration-form__input @if($errors->has('email')) input_error @endif"
                                           value="{!! old('email') !!}">
                                </div>
                                <br>
                                <button type="submit" class="product__btn registration-form__btn">Продолжить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

@endsection