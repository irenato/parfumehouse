@extends('public.layouts.main')
@section('meta')
    <title>{!! $settings->meta_title !!}</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection
@section('content')
    <main class="main-container homepage">
        @if($slideshow_status)
            <section class=section-1>
                <div class=main-slider>
                    @forelse($slideshow as $slide)
                        <div class="main-slide first-slide" style="background-image: url({!! $slide->image->get_current_file_url() !!});">
                            <div class=container>
                                <div class="col-md-8 zero-padding main-slide-content">
                                    <span class=main-slide__title>НЕ ЗНАЕТЕ КАКОЙ ПАРФЮМ ПОДОБРАТЬ?</span>
                                    <span class=main-slide__subtitle>Parfum House поможет Вам</span>
                                    <a href="{!! $slide->link !!}">
                                        <button class="main-slide__btn">Узнать больше</button>
                                    </a>
                                </div>
                                <div class=col-md-4></div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </section>
        @endif

        @if($latest_status)
            <section class=section-2>
                <span class=section-title>Новинки</span>
                <div class=wrapper>
                    <div class=new-items>
                        @forelse($latest_products as $product)
                            @if(!empty($product))
                                @include('public.layouts.product', $product)
                            @endif
                        @empty
                        @endforelse
                    </div>
                </div>
            </section>
        @endif

        @if($bestseller_status)
            <section class=section-3>
                <span class=section-title>Новинки</span>
                <div class=wrapper>
                    <div class=new-items>
                        @forelse($bestseller_products as $product)
                            @if(!empty($product))
                                @include('public.layouts.product', $product)
                            @endif
                        @empty

                        @endforelse
                    </div>
                </div>
            </section>
        @endif

        <div class="container">
            @if($articles !== 'null')
                <section class=section-4><span class=section-title>Статьи</span>
                    <div class=articles-slider>
                        @foreach($articles as $article)
                            <div class="articles-slide">
                                <img class="article-pic" src="{!! $article->image->get_current_file_url('blog') !!}" alt="{!! $article->title !!}">
                                <a class="article-btn" href="/articles/{!! $article->url_alias !!}">ЧТО НОВОГО</a>
                                <div class=article-date>{!! $article->date !!}</div>
                                <div class=clearfix></div>
                                <div class=article-info>
                                    <span class=article-tite>{!! $article->title !!}</span>
                                    <div class=article-txt>
                                        {!! strip_tags(html_entity_decode($article->text)) !!}
                                    </div>
                                    <a class="article-link icon-arrow" href="/articles/{!! $article->url_alias !!}">ПЕРЕЙТИ</a></div>
                            </div>
                        @endforeach
                    </div>
                </section>
            @endif
        </div>

        <section class=section-4><span class=section-title>О компании</span>
            <div class=container>
                <article class=about-text>
                    {!! html_entity_decode($settings->about) !!}
                </article>
            </div>
        </section>

    </main>

@endsection