<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table class="table" width="100%" cellspacing="0" cellpadding="0" >
        <tbody>
        @foreach($products as $product)
            <tr>
                <td ><img style="width: 55px;" src="/assets/images/{!! $product->image ? $product->image->href : $product->original_image->href !!}" alt=""></td>
                <td >{!! $product->name !!}</td>
                <td >{!! $product->capacity !!} {!! $product->measures->name !!}</td>
                {{--<td >{!! $product->price !!} грн</td>--}}
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>

<style>
    td{
        border: 1px solid #000;
        font-family: 'PT Sans Narrow', Helvetica, sans-serif;
        text-align: center;
        vertical-align: middle;
    }
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    td img {
        display: block;
        margin: 0 auto;
        padding: 10px 0;
    }
    table{
        border-collapse: collapse;
    }
    @media print {
        td{
            padding: 10px 0;
        }
    }
</style>