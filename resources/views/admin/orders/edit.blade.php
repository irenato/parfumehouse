@include('admin.layouts.header')
@extends('admin.layouts.main')
@section('title')
    Заказы
@endsection
@section('content')

    <h1>Просмотр заказа № {{ $order->id }}</h1>

    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Товары в заказе</h4>
            </div>
            <div class="table table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr class="success">
                        <td align="center">Артикул</td>
                        <td>Производитель</td>
                        <td>Наименование</td>
                        <td>Объем</td>
                        <td>Количество</td>
                        <td align="center">Цена</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->products as $item)
                        <tr>
                            <td align="center">{!! $item->product->articul or ''!!}</td>
                            @foreach($item->product->attributes as $atributes)
                                @if($atributes->attribute_id == 1)
                                    <td>{!! $atributes->value->name !!}</td>
                                @elseif($atributes->attribute_id == 5)
                                    <td>{!! $atributes->value->name !!}</td>
                                @endif
                            @endforeach
                            <td>{!! $item->product->name !!}</td>
                            <td>{!! $item->product->capacity !!} {!! $item->product->measures->name !!}</td>
                            <td>{!! $item->product_quantity !!} шт</td>
                            <td align="center">{!! round($item->product_sum * $rate, 2) !!} грн</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4" align="right">Итого:</td>
                        <td>{!! $order->products_quantity !!} шт</td>
                        <td align="center">{!! round($order->products_sum * $rate, 2) !!} грн</td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Информация о заказе</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="table table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <td colspan="2" align="center">
                                        Покупатель
                                    </td>
                                </tr>
                                </thead>
                                <tr>
                                    <td>Покупатель</td>
                                    <td>{!! $order_info['user_name'] or $order->user->first_name !!}</td>
                                </tr>
                                <tr>
                                    <td>Телефон</td>
                                    <td>{!! $order_info['phone'] or $order->user->phone !!}</td>
                                </tr>
                                <tr>
                                    <td>Почта</td>
                                    <td>{!! $order_info['email'] or ''!!}</td>
                                </tr>
                                <tr>
                                    <td>Заказы покупателя</td>
                                    <td><a href="/admin/users/stat/{!! $order->user_id !!}" >Все заказы</a></td>
                                </tr>
                                <tr>
                                    <td>Сумма заказа</td>
                                    <td>{!! round($order->products_sum * $rate, 2) !!} грн</td>
                                </tr>
                                <tr>
                                    <td>Количество товаров</td>
                                    <td>{!! $order->products_quantity !!} шт</td>
                                </tr>
                                <tr>
                                    <td>Дата заказа</td>
                                    <td>{!! $order->date !!} {!! $order->time !!}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <td colspan="2" align="center">
                                        Доставка и оплата
                                    </td>
                                </tr>
                                </thead>
                                @if(is_array($order_info['delivery']))
                                    @foreach($order_info['delivery'] as $key => $value)
                                        @if($key == 'method')
                                            <tr>
                                                <td>Способ доставки</td>
                                                <td>{!! $value !!}</td>
                                            </tr>
                                        @endif
                                        @if($key == 'region')
                                            <tr>
                                                <td>Область</td>
                                                <td>{!! $value !!}</td>
                                            </tr>
                                        @endif
                                        @if($key == 'city')
                                            <tr>
                                                <td>Город</td>
                                                <td>{!! $value !!}</td>
                                            </tr>
                                        @endif
                                        @if($key == 'department')
                                            <tr>
                                                <td>Отделение Новой Почты</td>
                                                <td>{!! $value !!}</td>
                                            </tr>
                                        @endif
                                        @if($key == 'street')
                                            <tr>
                                                <td>Улица</td>
                                                <td>{!! $value !!}</td>
                                            </tr>
                                        @endif
                                        @if($key == 'house')
                                            <tr>
                                                <td>Дом</td>
                                                <td>{!! $value !!}</td>
                                            </tr>
                                        @endif
                                        @if($key == 'flat')
                                            <tr>
                                                <td>Квартира</td>
                                                <td>{!! $value !!}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td>Доставка</td>
                                        <td>{!! $order_info['delivery'] !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Отделение НП</td>
                                        <td>{!! $order_info['department'] !!}</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <form method="post">
                {!! csrf_field() !!}
                <div class="panel-heading">
                    <h4>Настройки</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-2 text-right">Определите статус</label>
                            <div class="form-element col-sm-10">
                                <select name="status" class="form-control">
                                    @foreach($orders_statuses as $status)
                                        @if($status->id == $order->status_id)
                                            <option value="{{ $status->id }}" selected>{{ $status->status }}</option>
                                        @else
                                            <option value="{{ $status->id }}">{{ $status->status }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <button type="submit" class="btn">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection
