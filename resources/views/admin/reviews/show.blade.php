@include('admin.layouts.header')
@extends('admin.layouts.main')
@section('title')
    Отзывы
@endsection
@section('content')

    <h1>Просмотр отзыва</h1>

    <div class="form">
        <form method="post">
            {!! csrf_field() !!}
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Информация</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Пользователь</label>
                                <div class="form-element col-sm-10">
                                    {!! $review->user->first_name !!} {!! $review->user->last_name !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Товар</label>
                                <div class="form-element col-sm-10">
                                    {!! $review->product->name !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Оценка</label>
                                <div class="form-element col-sm-10">
                                    {!! $review->grade !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Достоинства</label>
                                <div class="form-element col-sm-10">
                                    {!! $review->advantages !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Недостатки</label>
                                <div class="form-element col-sm-10">
                                    {!! $review->flaws !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Текст отзыва</label>
                                <div class="form-element col-sm-10">
                                    {!! $review->review !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Настройки</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right">Опубликовать</label>
                                <div class="form-element col-sm-10">
                                    <select name="published" class="form-control">
                                        <option value="1">Да</option>
                                        <option value="0">Нет</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <input type="hidden" name="product_id" value="{!! $review->product_id !!}">
                                <button type="submit" class="btn">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>

@endsection
