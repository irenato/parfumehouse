@include('admin.layouts.header')
@extends('admin.layouts.main')
@section('title')
    Атрибуты
@endsection
@section('content')

    <h1>Добавление атрибута</h1>

    @if(session('message-error'))
        <div class="alert alert-danger">
            {{ session('message-error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="form">
        <form method="post">
            {!! csrf_field() !!}
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Общая информация</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-right control-label">Название</label>
                                <div class="form-element col-sm-10">
                                    <input type="text" data-translit="input" class="form-control" name="name" value="{!! old('name') !!}" />
                                    @if($errors->has('name'))
                                        <p class="warning" role="alert">{!! $errors->first('name',':message') !!}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="form-element col-sm-12">
                                    <table class="table table-hover attribute-images-settings">
                                        <thead>
                                        <tr class="success">
                                            <td colspan="2" align="center">Настройки наложения изображений</td>
                                        </tr>
                                        <tr class="success">
                                            <td>Включить наложение изображений</td>
                                            <td>
                                                <select name="enable_image_overlay" onchange="overlaySettings($(this).val())" class="form-control">
                                                    @if(old('enable_image_overlay'))
                                                        <option value="1" selected>Да</option>
                                                        <option value="0">Нет</option>
                                                    @else
                                                        <option value="1">Да</option>
                                                        <option value="0" selected>Нет</option>
                                                    @endif
                                                </select>
                                            </td>
                                        </tr>
                                        </thead>

                                        <tbody id="attribute-image-overlay-setting" @if(!old('enable_image_overlay')) style="display: none;" @endif>
                                        <tr class="success">
                                            <td>Выберите расположение картинки на изображении товара:</td>
                                            <td><select name="coordinates" class="form-control">
                                                    @foreach($overlay_position as $position)
                                                        <option value="{!! $position['placement'] !!}"
                                                                @if(old('coordinates') == $position['placement'])
                                                                selected
                                                                @endif>
                                                            {!! $position['name'] !!}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="success">
                                            <td>Укажите размер накладываемых картинок (в % от размера исходного изображения товара)</td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <input type="range"
                                                               min="0"
                                                               max="1"
                                                               name="image_percent"
                                                               step="0.05"
                                                               value="{!! old('image_percent') ? old('image_percent') : 0 !!}"
                                                               onchange="$('#image-percent').html(($(this).val() * 100) + '%');"
                                                        />

                                                    </div>
                                                    <div class="col-xs-6">
                                                        <span id="image-percent">{!! old('image_percent') ? old('image_percent') * 100 : 0 !!}%</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="success">
                                            <td>Отступ накладываемой картинки от края исходного изображения</td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>По горизонтали</p>
                                                        <input type="text"
                                                               name="offset_x"
                                                               class="form-control"
                                                               value="{!! old('offset_x') ? old('offset_x') : 0 !!}"
                                                        />
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <p>По вертикали</p>
                                                        <input type="text"
                                                               name="offset_y"
                                                               class="form-control"
                                                               value="{!! old('offset_y') ? old('offset_y') : 0 !!}"
                                                        />
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="success">
                                            <td>Максимальное количество накладываемых картинок</td>
                                            <td>
                                                <input type="text"
                                                       name="max_quantity"
                                                       class="form-control"
                                                       value="{!! old('max_quantity') ? old('max_quantity') : 0  !!}"
                                                />
                                                @if($errors->has('max_quantity'))
                                                    <p class="warning" role="alert">{!! $errors->first('max_quantity',':message') !!}</p>
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if($errors->has('values'))
                            <p class="warning" role="alert">{!! $errors->first('values',':message') !!}</p>
                        @endif
                        <div class="form-group attribute-value">
                            <div class="row">
                                <label class="col-sm-2 text-right">Значения</label>
                                <div class="form-element col-sm-10">
                                    @if(old('values'))
                                        @foreach(old('values') as $id => $value)
                                            @foreach($value as $new_id => $new_attribute)
                                                <div class="row form-group">
                                                    <div class="col-xs-9 attribute-name">
                                                        <input type="text"
                                                               name="values[new][{!! $new_id !!}][name]"
                                                               class="form-control"
                                                               value="{!! $new_attribute['name'] !!}"
                                                        />
                                                        @if($errors->has('values.new.' . $new_id . '.name'))
                                                            <p class="warning" role="alert">{!! $errors->first('values.new.' . $new_id . '.name',':message') !!}</p>
                                                        @endif
                                                    </div>
                                                    <div class="col-xs-2 attribute-image text-center">
                                                        <input type="hidden"
                                                               class="input-uploaded-image-href"
                                                               name="values[new][{!! $new_id !!}][image_href]"
                                                               value="{{ $new_attribute['image_href'] ? $new_attribute['image_href'] : null }}"
                                                        />
                                                        <button type="button" class="btn button-upload-attribute-image">
                                                            <img src="/assets/attributes_images/{!! $new_attribute['image_href'] ? $new_attribute['image_href'] : 'no_image.jpg' !!}" class="img-thumbnail" />
                                                        </button>
                                                        <button type="button" class="btn btn-del" data-toggle="tooltip" data-placement="bottom" title="Удалить изображение">X</button>
                                                    </div>
                                                    <div class="col-xs-1 text-center">
                                                        <button type="button" class="btn btn-danger" onclick="$(this).parent().parent().remove();">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endforeach
                                        <input type="hidden" value="{!! $new_id ? $new_id : 0 !!}" id="attribute-values-iterator" />
                                    @else
                                        <input type="hidden" value="0" id="attribute-values-iterator" />
                                    @endif
                                </div>
                                <div class="col-sm-12 text-right">
                                    <button type="button" class="btn" id="button-add-attribute">Добавить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <button type="submit" class="btn">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
