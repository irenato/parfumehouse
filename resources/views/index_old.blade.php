@extends('public.layouts.main')
@section('meta')
    <title>{!! $settings->meta_title !!}</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection
@section('content')
	<main class="main-content">
        @if($slideshow_status)
		<section class="banner">
            <div class="container">
                <div class="banner-slider">
                    @forelse($slideshow as $slide)
                        <div class="banner__item banner__item_1" style="background-image: url({!! $slide->image->get_current_file_url() !!});">
							@if(strpos($slide->link, 'http') === 0)
								<a href="{!! $slide->link !!}" class="banner__btn">Узнать больше</a>
							@else
								<a {!! $slide->link !!} class="banner__btn">Узнать больше</a>
							@endif
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
		</section>
        @endif

        @if($latest_status)
		<section class="new-items">
			<div class="container">
				<h2 class="section-title">Новинки</h2>
				<div class="new-slider">
                    @forelse($latest_products as $product)
						@if(!empty($product))
							@include('public.layouts.product', $product)
						@endif
                    @empty

                    @endforelse
				</div>
			</div>
		</section>
        @endif

        @if($bestseller_status)
		<section class="new-items leaders">
			<div class="container">
				<h2 class="section-title">Лидеры продаж</h2>
				<div class="new-slider">
					@forelse($bestseller_products as $product)
						@if(!empty($product))
							@include('public.layouts.product', $product)
						@endif
					@empty

					@endforelse
				</div>
			</div>
		</section>
        @endif

		<div class="container">
            @if($articles !== 'null')
			<section class="blog">
				<h2 class="section-title section-title_blog">Статьи</h2>
				<div class="row">
                    @foreach($articles as $article)
                        <div class="col-sm-6">
                            <article class="blog-item clearfix">
                                <div class="blog-item__thumbnail" style="background-image: url({!! $article->image->get_current_file_url('blog') !!});"></div>
                                <div class="blog-item__title-wrap clearfix">
                                    <a href="/articles/{!! $article->url_alias !!}" class="blog-item__name">{!! $article->title !!}</a>
                                    <span class="blog-item__date">{!! $article->date !!}</span>
                                </div>
                                <div class="blog-item__text blog-item__text_cut">
                                    {!! strip_tags(html_entity_decode($article->text)) !!}
                                </div>
                                <a href="/articles/{!! $article->url_alias !!}" class="blog-item__link">Подробнее ></a>
                            </article>
                        </div>
                    @endforeach
				</div>
            </section>
            @endif
        </div>

		<section class="about">
			<div class="container">
			    {!! html_entity_decode($settings->about) !!}
            </div>
		</section>

	</main>

@endsection