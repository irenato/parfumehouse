<div class="header" style="text-align: center; background:#000;">
    <img src="{!! url('/img/logo.png') !!}" alt="logo"  title="Parfum" style="max-width: 50%; display: block; margin: 15px auto;" />
</div>

<h1 style="font-size: 20px;">Новый опрос на сайте Parfum House.</h1>

@if(!empty($user))
    <p style="font-size:16px; color: #333;"><strong>Имя: </strong>{!! $user->first_name !!}</p>
    <p style="font-size:16px; color: #333;"><strong>E-mail: </strong>{!! $user->email !!}</p>

    <h3>Оценка уровня качества и наличия товара</h3>
    @if(!empty($data['q1r']))
        <p style="font-size:16px; color: #333;"><strong>Оценка: </strong>{!! $data['q1r'] !!}</p>
    @endif
    @if(!empty($data['q1c']))
        <p style="font-size:16px; color: #333;"><strong>Комментарий: </strong>{!! $data['q1c'] !!}</p>
    @endif
    <br>

    <h3>Оценка качкства обслуживания (удобста заказа, компетентности менеджера)</h3>
    @if(!empty($data['q2r']))
        <p style="font-size:16px; color: #333;"><strong>Оценка: </strong>{!! $data['q2r'] !!}</p>
    @endif
    @if(!empty($data['q2c']))
        <p style="font-size:16px; color: #333;"><strong>Комментарий: </strong>{!! $data['q2c'] !!}</p>
    @endif
    <br>

    <h3>Оценка работы службы доставки</h3>
    @if(!empty($data['q3r']))
        <p style="font-size:16px; color: #333;"><strong>Оценка: </strong>{!! $data['q3r'] !!}</p>
    @endif
    @if(!empty($data['q3c']))
        <p style="font-size:16px; color: #333;"><strong>Комментарий: </strong>{!! $data['q3c'] !!}</p>
    @endif
    <br>

    <h3>Оценка качества сервиса (решения возникающих проблем)</h3>
    @if(!empty($data['q4r']))
        <p style="font-size:16px; color: #333;"><strong>Оценка: </strong>{!! $data['q4r'] !!}</p>
    @endif
    @if(!empty($data['q4c']))
        <p style="font-size:16px; color: #333;"><strong>Комментарий: </strong>{!! $data['q4c'] !!}</p>
    @endif
    <br>

    <h3>Комментарий по улучшению качества работы Компании</h3>
    @if(!empty($data['q5c']))
        <p style="font-size:16px; color: #333;"><strong>Комментарий: </strong>{!! $data['q5c'] !!}</p>
    @endif
    <br>

@endif