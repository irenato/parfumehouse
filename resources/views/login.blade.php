@extends('public.layouts.main')

@section('meta')
    <title>Вход в личный кабинет</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('login') !!}
@endsection

@section('content')
    <main>
        <div class="container">
            <section class="account-content autorization-content">
                <h2 class="section-title">Авторизация</h2>
                @if(!empty($errors->all()))
                    <span class="error-message">
                        {!! $errors->first() !!}
                    </span>
                @endif
                <div class="row">
                    <div class="col-lg-3 col-lg-push-2 col-md-4 col-md-push-1 col-sm-6">
                        <div class="autorization-col autorization">
                            <form id="autorization-form" class="autorization-form" method="post">
                                {!! csrf_field() !!}
                                <div class="autorization-form__input-wrap">
                                    <label for="email" class="autorization-form__label">E-mail</label>
                                    <input type="text" name="email" id="email" class="autorization-form__input @if($errors->has('email')) input_error @endif" value="{!! old('email') !!}">
                                </div>
                                <div class="autorization-form__input-wrap">
                                    <label for="password" class="autorization-form__label">Пароль</label>
                                    <input type="password" id="password" name="password" class="autorization-form__input">
                                    <a href="/forgotten" class="autorization-form__forgot">Забыли пароль?</a>
                                </div>
                                <button type="submit" class="product__btn autorization-form__btn">Войти</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-5 col-lg-push-3 col-md-6 col-md-push-2 col-sm-6">
                        <div class="autorization-col registration-col">
                            <ul class="registration-iconset">
                                <span class="registration-iconset__title">Создав учетную запись Вы сможете: </span>
                                <li class="registration-iconset__item">
                                    <i class="registration-iconset__icon">&#xe816;</i>
                                    <span class="registration-iconset__text">Отслеживать статус заказа</span>
                                </li>
                                <li class="registration-iconset__item">
                                    <i class="registration-iconset__icon">&#xe80e;</i>
                                    <span class="registration-iconset__text">Просматривать историю покупок</span>
                                </li>
                                <li class="registration-iconset__item">
                                    <i class="registration-iconset__icon">&#xe817;</i>
                                    <span class="registration-iconset__text">Узнавать о новинках </span>
                                </li>
                            </ul>
                            <a class="product__btn registration-col__btn" href="/register">Зарегистрироваться</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection