@extends('public.layouts.main')

@section('meta')
    <title>Регистрация</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    <meta name="keywords" content="{!! $settings->meta_keywords !!}">
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('register') !!}
@endsection

@section('content')

    <main>
        <div class="container">
            <section class="account-content">
                <h2 class="section-title">Регистрация</h2>
                <div class="row">
                    <div class="col-md-8 col-md-push-2">
                        <div class="registration-content">
                            <span class="registration-content__text">Если Вы уже зарегистрированы, перейдите на страницу <a href="/login" class="registration-content__link">Входа в систему</a></span>
                            <ul class="registration-iconset">
                                <span class="registration-iconset__title">Создав учетную запись Вы сможете: </span>
                                <li class="registration-iconset__item">
                                    <i class="registration-iconset__icon">&#xe816;</i>
                                    <span class="registration-iconset__text">Отслеживать статус заказа</span>
                                </li>
                                <li class="registration-iconset__item">
                                    <i class="registration-iconset__icon">&#xe80e;</i>
                                    <span class="registration-iconset__text">Просматривать историю покупок</span>
                                </li>
                                <li class="registration-iconset__item">
                                    <i class="registration-iconset__icon">&#xe817;</i>
                                    <span class="registration-iconset__text">Узнавать о новинках </span>
                                </li>
                            </ul>
                            @if(!empty($errors->all()))
                            <span class="error-message">
                                {!! $errors->first() !!}
                            </span>
                            @endif
                            <form class="registration-form" method="post">
                                {!! csrf_field() !!}
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="name">Имя*</label>
                                    <input type="text"
                                           name="first_name"
                                           id="name"
                                           class="registration-form__input @if($errors->has('first_name')) input_error @endif"
                                           value="{!! old('first_name') !!}">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="surname">Фамилия</label>
                                    <input type="text"
                                           name="last_name"
                                           id="surname"
                                           class="registration-form__input"
                                           value="{!! old('last_name') !!}">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="phone">Телефон*</label>
                                    <input type="text"
                                           name="phone"
                                           id="phone"
                                           class="registration-form__input @if($errors->has('phone')) input_error @endif"
                                           value="{!! old('phone') !!}">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="email">Email*</label>
                                    <input type="text"
                                           name="email"
                                           id="email"
                                           class="registration-form__input @if($errors->has('email')) input_error @endif"
                                           value="{!! old('email') !!}">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="password">Пароль*</label>
                                    <input type="password"
                                           name="password"
                                           id="password"
                                           class="registration-form__input @if($errors->has('password')) input_error @endif">
                                </div>
                                <div class="registration-form__input-wrap">
                                    <label class="registration-form__label" for="passwordr">Повторите пароль*</label>
                                    <input type="password"
                                           name="password_confirmation"
                                           id="passwordr" class="registration-form__input @if($errors->has('password_confirmation')) input_error @endif">
                                </div>
                                <span class="registration-form__note">*Поля обязательные для заполнения</span>
                                <div class="checkbox-wrap">
                                    <input type="checkbox" name="terms" id="registration-check" class="registration-form__checkbox" hidden>
                                    <label for="registration-check" class="registration-form__check-label">Я прочитал <a href="#" class="registration-form__check-link">Пользовательское соглашение</a> и согласен с ними</label>
                                </div>
                                <button type="submit" class="product__btn registration-form__btn">Зарегистрироваться</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

@endsection