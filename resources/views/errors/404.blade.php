@extends('public.layouts.main')
@section('meta')
    <title>Ошибка 404. Страница не найдена</title>
@endsection
@section('content')
    <main class="main-container not-found-container">
        <section class=not-found>
            <div class=container>
                <div class=not-found-wrapper>
                    <div class="col-sm-6 not-found-pic__wrapper"><img class=not-found-pic src=/assets/static/png/404_595e01a7514be03ed238b38813b51407.png alt=""></div>
                    <div class="col-sm-6 not-found-text">
                        <div>404 not page</div>
                        <span>УПС!</span>
                        <small>Запрашиваемая Вами страница не найдена</small>
                        <a class=to-main__btn href="/">Перейти на главную</a></div>
                </div>
            </div>
        </section>
    </main>
@endsection