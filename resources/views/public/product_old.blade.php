@extends('public.layouts.main')
@section('meta')
	<title>{!! $product->name !!} {!! empty($product->capacity) ? '' : $product->capacity.'мл' !!} {!! $product->articul !!} | Parfumhouse</title>
    <meta name="description" content="Купить {!! $product->name !!} {!! empty($product->capacity) ? '' : $product->capacity.'мл' !!} {!! $product->articul !!} | Parfumhouse">
    {{--<title>{!! $product->meta_title !!}</title>--}}
    {{--<meta name="description" content="{!! $product->meta_description !!}">--}}
    {{--<meta name="keywords" content="{!! $product->meta_keywords !!}">--}}
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('product', $product, $product->category) !!}
@endsection

@section('content')

    <div class="container">
        <h1 class="product__title">{!! !empty($product->articul) ? $product->articul . ' / ' : '' !!}{!! $product->name !!}</h1>
        <div class="tabs">
            <ul class="tabs-caption clearfix">
                <li class="tabs-caption__item active">Все о товаре</li>
                <li class="tabs-caption__item product-review">Отзывы ({{ $reviews->total() }})</li>
                @if($related_category) <li class="tabs-caption__item">{!! $related_category->name !!}</li> @endif
                {{--<li class="tabs-caption__item">Доставка</li>--}}
            </ul>

            <article class="tabs_content active product-main">
                <section class="product">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-5">
                            {{--<div class="product__thumb product-col">--}}
                                {{--<a href="{!! $product->get_product_image->get_current_file_url('product') !!}" class="product__thumb-link">--}}
                                    {{--<img src="{!! $product->get_product_image->get_current_file_url('product') !!}"--}}
                                         {{--alt="{!! $product->get_product_image->title !!}"--}}
                                         {{--class="product__thumb-img">--}}
                                {{--</a>--}}
                                {{--@if($product->sale)--}}
                                    {{--<span class="product-card__stock product-card__stock-prod_layout">-{!! $product->sale !!}%</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}

                            <div class="product__thumb product-col" style="overflow: hidden; height: auto">
                                <div class="main-product_gallery-slider slick-gallery">
                                    @foreach($gallery as $image)
                                        <a href="{!! $image->get_current_file_url() !!}" class="product__thumb-link">
                                        <img alt="{!! $product->name !!}" src="{!! $image->get_current_file_url() !!}" class="product__thumb-img" style="display: inline-block !important;">
                                        </a>
                                    @endforeach
                                </div>
                                @if(count($gallery) > 1)
                                <div class="main-product_gallery-nav slick-navigation">
                                    @foreach($gallery as $image)
                                        <img alt="{!! $product->name !!}" src="{!! $image->get_current_file_url('product') !!}">
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-lg-4">
                            <div class="product-col" id="product-card">
                                <table class="product__attr">
                                    @if(!empty($product->articul))
                                    <tr>
                                        <td class="product__attr-item product__attr-item_left">Артикул</td>
                                        <td class="product__attr-item">{!! $product->articul !!}</td>
                                    </tr>
                                    @endif
                                    @if(!empty($product_attributes))
                                        @foreach($product_attributes as $attribute_name => $attribute_value)
                                            <tr>
                                                <td class="product__attr-item product__attr-item_left">{!! $attribute_name !!}</td>
                                                <td class="product__attr-item">{!! $attribute_value !!}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    @if($product->capacity)
                                    <tr>
                                        <td class="product__attr-item product__attr-item_left">Объем</td>
                                        <td class="product__attr-item">
                                            <strong>{!! $product->capacity !!} {!! $product->measures->name !!}</strong>
                                            <input type="number" step="1" min="1" id="product-card__amount-input" max name="quantity" class="product__item-quantity" value="1">
                                        </td>
                                    </tr>
                                    @else
                                        <input type="hidden" id="product-card__amount-input" max name="quantity" class="product__item-quantity" value="1">
                                    @endif
                                    @if($product->quantity)
                                        <tr>
                                            <td class="product__attr-item product__attr-item_left">Количество в упаковке</td>
                                            <td class="product__attr-item">
                                                <strong>{!! $product->quantity !!} шт</strong>
                                            </td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td class="product__attr-item product__attr-item_left">Цена</td>
                                        <td class="product__attr-item product__attr-item_price">
                                            {{--@if($product->sale)--}}
                                                {{--<i class="product-card__price-icon">&#xe81f;</i>--}}
                                            {{--@endif--}}
                                            {{ round($product->price, 2) }} $
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="product__attr-item product__attr-item_left"></td>
                                        <td class="product__attr-item product__attr-item_price">
                                            {{--@if($product->sale)--}}
                                                {{--<i class="product-card__price-icon">&#xe81f;</i>--}}
                                            {{--@endif--}}
                                            {{ round($product->price * $rate, 2) }} грн
                                        </td>
                                    </tr>
                                </table>
                                @if($user_logged)
                                    <a data-prod-id='{{ $product->id}}'
                                       data-user-id='{{ $user_id }}'
                                       id="add-to-wishlist"
                                       href="javascript:void(0)"
                                       style="position: static"
                                       class="product__btn product-card__add-fav {!! in_array($product->id, $user_wishlist) ? 'product-card__add-fav_active' : '' !!}">
                                    {!! in_array($product->id, $user_wishlist) ? '<i class="product-card__add-fav-icon" style="font-size: 1em">&#xe80a;</i>' : '' !!}
                                     В избранное
                                    </a>
                                @else
                                    <a href="javascript:void(0)"
                                       class="product__btn noactive"
                                       onclick="$(this).next('.add-fav-popup').addClass('add-fav-popup_active')">
                                        <i class="header-middle__search-icon product__btn-icon"></i> В избранное
                                    </a>
                                    <div class="add-fav-popup add-fav-popup_card" onmouseleave="$(this).removeClass('add-fav-popup_active')">
                                        <span class="add-fav-popup__descr">Войдите, чтобы сохранять понравившиеся Вам товары</span>
                                        <a href="/login" class="add-fav-popup__login-btn">Войти</a>
                                        или<br>
                                        <a href="/register" class="add-fav-popup__reg-btn">Зарегистрироваться</a>
                                    </div>
                                @endif

                                <a href="javascript:void(0)"
                                   class="product__btn product__btn_cart"
                                   id="product-card__btn"
                                   data-product-id="{!! $product->id !!}"
                                   data-user-id="{!! $user_id !!}">Купить
                                </a>

                                <div class="product-card__stars-wrap product-card__stars-wrap_full">
                                    <ul class="product-card__stars-list">
                                        @for($i=1; $i<=5; $i++)
                                            @break($reviews->total() == 0)

                                            @if($i <= $product->rating)
                                                <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe810;</i></li>
                                            @else
                                                <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe811;</i></li>
                                            @endif
                                        @endfor
                                    </ul>
                                    {{--<span class="product-card__review">--}}
										{{--<a href="javascript:void(0)" class="product-card__review-link">{{ $reviews->total() }} отзывов</a>--}}
									{{--</span>--}}
                                </div>
                                <ul class="product__soc-list">
                                    <li class="product__soc-item"><a href="{!! $socials->facebook or '' !!}" target="_blank" class="product__soc-link"><i class="product__soc-icon">&#xe813;</i></a></li>
                                    <li class="product__soc-item"><a href="{!! $socials->instagram or '' !!}" target="_blank"  class="product__soc-link"><i class="product__soc-icon">&#xf16d;</i></a></li>
                                    <li class="product__soc-item"><a href="{!! $socials->vkontakte or '' !!}" target="_blank"  class="product__soc-link"><i class="product__soc-icon">&#xf189;</i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-lg-3">
                            <div class="product__delivery product__delivery_main product-col clearfix">
                                <div class="product__delivery-list-wrap product__delivery-list-wrap_main">
                                    <h4 class="sidebar-title"><i class="header-middle__search-icon product__delivery-icon">&#xe80b;</i>Доставка</h4>
                                    <ul class="product__delivery-list">
                                        <li>В отделение Новой почты</li>
                                        <li>Курьерская доставка</li>
                                    </ul>
                                </div>
                                <div class="product__delivery-list-wrap product__delivery-list-wrap_main">
                                    <h4 class="sidebar-title"><i class="header-middle__search-icon product__delivery-icon">&#xe80c;</i>Бесплатная доставка при заказе от 100$ (1$={{ number_format($rate, 2, '.', '') }}грн)</h4>
                                </div>
                                <div class="product__delivery-list-wrap product__delivery-list-wrap_main">
                                    <h4 class="sidebar-title"><i class="header-middle__search-icon product__delivery-icon">&#xe80d;</i>Оплата</h4>
                                    <ul class="product__delivery-list">
                                        <li>Наличными</li>
                                        <li>Оплата картой</li>
                                    </ul>
                                </div>
                                <div class="product__delivery-list-wrap product__delivery-list-wrap_main">
                                    <h4 class="sidebar-title"><i class="header-middle__search-icon product__delivery-icon">&#xe814;</i>Минимальная сумма заказа 30$ (1$={{ number_format($rate, 2, '.', '') }} грн)</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="row">
                    @if($product->description->product_description)
                    <div class="col-md-8">
                        <h3 class="product__title"><strong>Описание</strong> {!! $product->name !!}</h3>
                        <section class="product-descr">
                            <ul class="product-triangle">
                                <li class="product-triangle__line clearfix">
                                    <img src="/assets/images/_notes-top.png" alt="" class="product-triangle__img">
                                        <span class="product-triangle__title">Верхние ноты</span>
                                        @if($product->description->upper_note)
                                            <span class="product-triangle__notes">{!! $product->description->upper_note !!}</span>
                                        @endif
                                </li>
                                <li class="product-triangle__line clearfix">
                                    <img src="/assets/images/_notes-middle.png" alt="" class="product-triangle__img">
                                    <span class="product-triangle__title">Сердечные ноты</span>
                                    @if($product->description->heart_note)
                                        <span class="product-triangle__notes">{!! $product->description->heart_note !!}</span>
                                    @endif
                                </li>
                                <li class="product-triangle__line clearfix">
                                    <img src="/assets/images/_notes-bottom.png" alt="" class="product-triangle__img">
                                    <span class="product-triangle__title">Шлейфовые ноты</span>
                                    @if($product->description->base_note)
                                        <span class="product-triangle__notes">{!! $product->description->base_note !!}</span>
                                    @endif
                                </li>
                            </ul>
                            <div class="product-descr__text">
                                @if(isset($product_attributes['Группа ароматов'])) <p><strong>Группа:</strong> {!! $product_attributes['Группа ароматов'] !!} </p> @endif
                                    <p>{!! $product->description->product_description or '' !!}</p>
                                @if($product->description->character)<p><strong>Характер:</strong> {!! $product->description->character !!}</p> @endif
                                    @if($product->description->base_note)<p><strong>Ноты:</strong>
                                        {!! $product->description->upper_note or '' !!}
                                        {!! $product->description->heart_note ? ', ' . $product->description->heart_note : '' !!}
                                        {!! $product->description->base_note ? ', ' . $product->description->base_note : '' !!}
                                    </p> @endif
                            </div>
                        </div>
                    @endif
                        <div class="{!! $product->description->product_description ? 'col-md-4' : 'col-md-12' !!}">
                            <div class="reviews_short">
                                <div class="title-reviews_short clearfix">
                                    <h3 class="product__title product__title_left"><strong>Отзывы</strong></h3>
                                </div>

                                @forelse($reviews as $key => $review)
                                    <article class="reviews-item {!! ($key + 1 == count($reviews)) ? 'reviews-item_last' : '' !!}">
                                        <div class="reviews-item__title clearfix">
                                            <span class="reviews-item__autor"><strong>{!! $review->user->first_name !!}</strong></span>
                                            <ul class="product-card__stars-list reviews-item_stars-list">
                                                @for($i=1; $i<=5; $i++)
                                                    @if($i <= $review->grade)
                                                        <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe810;</i></li>
                                                    @else
                                                        <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe811;</i></li>
                                                    @endif
                                                @endfor
                                            </ul>
                                            <span class="reviews-item__date">{!! $review->date !!}</span>
                                        </div>
                                        <span class="reviews-item__text">
                                                {!! $review->review !!}
                                            </span>
                                        <span class="reviews-item__text"><strong>Достоинства: </strong>{!! $review->advantages !!}</span>
                                        <span class="reviews-item__text"><strong>Недостатки: </strong>{!! $review->flaws !!}</span>
                                    </article>

                                    @if($key + 1 == count($reviews))
                                        <a href="javascript:void(0)" class="reviews_short__all-link">Смотреть все отзывы></a>
                                    @endif
                                @empty
                                    <span class="reviews-item__text">Пока нет отзывов. Будьте первым, кто оставит его.</span>
                                    <a href="javascript:void(0)" style="float: left; margin-top: 15px !important;" class="add-review-btn reviews_short__all-link">Оставить отзыв</a>
                                @endforelse
                            </div>
                        </div>
                    </section>
                </div>
            </article>


            <article class="tabs_content product-main product-review">
                <div class="row">
                    <div class="col-md-9">
                        <div class="reviews_short">
                            <div class="title-reviews_short clearfix">
                                <h3 class="product__title product__title_left"><strong>Отзывы</strong></h3>
                            </div>
                            @forelse($reviews as $key => $review)
                                <article class="reviews-item {!! ($key + 1 == count($reviews)) ? 'reviews-item_last' : '' !!}">
                                    <div class="reviews-item__title clearfix">
                                        <span class="reviews-item__autor"><strong>{!! $review->user->first_name !!}</strong></span>
                                        <ul class="product-card__stars-list reviews-item_stars-list">
                                            @for($i=1; $i<=5; $i++)
                                                @if($i <= $review->grade)
                                                    <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe810;</i></li>
                                                @else
                                                    <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe811;</i></li>
                                                @endif
                                            @endfor
                                        </ul>
                                        <span class="reviews-item__date">{!! $review->date !!}</span>
                                    </div>
                                    <span class="reviews-item__text">
                                                {!! $review->review !!}
                                            </span>
                                    <span class="reviews-item__text"><strong>Достоинства: </strong>{!! $review->advantages !!}</span>
                                    <span class="reviews-item__text"><strong>Недостатки: </strong>{!! $review->flaws !!}</span>
                                </article>
                            @empty

                            @endforelse
                        </div>
                        {!! $reviews->fragment('reviews')->links(); !!}

                        <div class="give-review">
                            <div class="title-reviews_short clearfix">
                                <h3 class="product__title product__title_left"><strong>Оставить отзыв</strong></h3>
                            </div>
                            <form class="give-review__form" id="give-review__form">
                                {!! csrf_field() !!}
                                <div class="give-review__input-wrap  give-review__input-wrap_left">
                                    <label for="plus" class="give-review__label">Достоинства</label>
                                    <input type="text" name="advantages" id="plus" class="give-review__input">
                                </div>
                                <div class="give-review__input-wrap  give-review__input-wrap_right">
                                    <label for="minus" class="give-review__label">Недостатки</label>
                                    <input type="text" name="flaws" id="minus" class="give-review__input">
                                </div>
                                <label for="minus" class="give-review__label">Коментарий</label>
                                <textarea rows="4" name="review" id="minus" class="give-review__textarea"></textarea>
                                <div class="give-review__input-wrap  give-review__input-wrap_left">
                                    <label for="name" class="give-review__label">Имя</label>
                                    <input type="text" name="name" id="name" class="give-review__input">
                                </div>
                                <div class="give-review__input-wrap  give-review__input-wrap_right">
                                    <label for="email" class="give-review__label">E-mail</label>
                                    <input type="text" name="email" id="email" class="give-review__input">
                                </div>
                                <div class="clearfix">
                                    <div class="give-review__input-wrap  give-review__input-wrap_left">
                                        <div class="give-review__rating">
                                            <input id="star-5" type="radio" name="grade" class="give-review__rating-input" value="5">
                                            <label for="star-5" class="give-review__rating-label"></label>

                                            <input id="star-4" type="radio" name="grade" class="give-review__rating-input" value="4">
                                            <label for="star-4" class="give-review__rating-label"></label>

                                            <input id="star-3" type="radio" name="grade" class="give-review__rating-input" value="3">
                                            <label for="star-3" class="give-review__rating-label"></label>

                                            <input id="star-2" type="radio" name="grade" class="give-review__rating-input" value="2">
                                            <label for="star-2" class="give-review__rating-label"></label>

                                            <input id="star-1" type="radio" name="grade" class="give-review__rating-input" value="1">
                                            <label for="star-1" class="give-review__rating-label"></label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="product_id" value="{!! $product->id !!}">
                                    <button type="submit" class="give-review__btn">Оставить отзыв</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <ul class="tabs-caption_right clearfix">
                            <li class="tabs-caption__item-right active">Ваш товар</li>
                            <li class="tabs-caption__item-right">Доставка и оплата</li>
                        </ul>
                        <article class="tabs_content_right active">
                            <div id="product-card" class="product-card product-card_right" onmouseleave="$(this).find('.add-fav-popup').removeClass('add-fav-popup_active')">
                                @if($user_logged)
                                    <a data-prod-id='{{ $product->id}}'
                                       data-user-id='{{ $user_id }}'
                                       id="add-to-wishlist"
                                       href="javascript:void(0)"
                                       class="product-card__add-fav {!! in_array($product->id, $user_wishlist) ? 'product-card__add-fav_active' : '' !!}">
                                        <i class="product-card__add-fav-icon">&#xe80a;</i>
                                    </a>
                                @else
                                    <a href="javascript:void(0)"
                                       class="product-card__add-fav noactive"
                                       onclick="$(this).next('.add-fav-popup').addClass('add-fav-popup_active')">
                                        <i class="product-card__add-fav-icon">&#xe80a;</i>
                                    </a>
                                    <div class="add-fav-popup">
                                        <span class="add-fav-popup__descr">Войдите, чтобы сохранять понравившиеся Вам товары</span>
                                        <a href="/login" class="add-fav-popup__login-btn">Войти</a>
                                        или<br>
                                        <a href="/register" class="add-fav-popup__reg-btn">Зарегистрироваться</a>
                                    </div>
                                @endif
                                <a href="javascript:void(0)" class="product-card__thumbnail" style="background-image: url('{{ $product->get_product_image->get_current_file_url('product_list') }}');"> </a>
                                <a href="javascript:void(0)" class="product-card__title"><span class="product-card__title-main">{!! $product->name !!}</a>
                                <div class="product-card__stars-wrap">
                                    <ul class="product-card__stars-list">
                                        @for($i=1; $i<=5; $i++)
                                            @break(count($product->reviews) == 0)

                                            @if($i <= $product->description->rating)
                                                <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe810;</i></li>
                                            @else
                                                <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe811;</i></li>
                                            @endif
                                        @endfor
                                    </ul>
                                    <span class="product-card__review">
                                        <a href="javascript:void(0)" class="product-card__review-link">{{count($product->reviews)}} отзывов</a>
                                    </span>
                                </div>
                                <form class="product-card__form">
                                    <div class="product-card__descr">
                                        <span class="product-card__vol">{!! $product->quantity !!} {!! $product->measures->name !!}</span>
                                        <span class="product-card__amount">
											<input type="number" value="1" min="1" class="product-card__amount-input" id="product-card__amount-input">
										</span>
                                        <span class="product-card__price">{!! round($product->price) !!} грн</span>
                                    </div>
                                    <button type="button"
                                            id="product-card__btn"
                                            class="product-card__btn"
                                            data-product-id="{!! $product->id !!}"
                                            data-user-id="{!! $user_id !!}">Купить</button>
                                </form>
                            </div>
                            <ul class="product__soc-list product__soc-list_right">
                                <li class="product__soc-item"><a href="#" class="product__soc-link"><i class="product__soc-icon">&#xe813;</i></a></li>
                                <li class="product__soc-item"><a href="#" class="product__soc-link"><i class="product__soc-icon">&#xf16d;</i></a></li>
                                <li class="product__soc-item"><a href="#" class="product__soc-link"><i class="product__soc-icon">&#xf189;</i></a></li>
                            </ul>
                        </article>
                        <article class="tabs_content_right">
                            <div class="product__delivery product-col clearfix">
                                <div class="product__delivery-list-wrap">
                                    <h4 class="sidebar-title"><i class="header-middle__search-icon product__delivery-icon">&#xe80b;</i>Доставка</h4>
                                    <ul class="product__delivery-list">
                                        <li>В отделение Новой почты</li>
                                        <li>Курьерская доставка</li>
                                    </ul>
                                </div>
                                <div class="product__delivery-list-wrap">
                                    <h4 class="sidebar-title"><i class="header-middle__search-icon product__delivery-icon">&#xe80c;</i>Бесплатная доставка при заказе от 100$ (1$=25грн)</h4>
                                </div>
                                <div class="product__delivery-list-wrap">
                                    <h4 class="sidebar-title"><i class="header-middle__search-icon product__delivery-icon">&#xe80d;</i>Оплата</h4>
                                    <ul class="product__delivery-list">
                                        <li>Наличными</li>
                                        <li>Оплата картой</li>
                                    </ul>
                                </div>
                                <div class="product__delivery-list-wrap">
                                    <h4 class="sidebar-title"><i class="header-middle__search-icon product__delivery-icon">&#xe814;</i>Минимальная сумма заказа 30$ (1$=25 грн)</h4>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

            </article>
            @if(!$related_products->isEmpty())
                <section class="bottles">
                    <h3 class="product__title"><strong>{!! $related_category->name !!}</strong> для {!! $product->name !!} /</h3>
                    <div class="bottle-slider">
                       @foreach($related_products as $product)
                            @include('public.layouts.product_small', $product)
                       @endforeach
                    </div>
                </section>
            @endif
        </div>

        @if(!empty($recommended))
            <section class="new-items">
                <div class="container">
                    <h2 class="section-title">Рекомендуемые товары</h2>
                    <div class="new-slider">
                        @foreach($recommended as $product)
                            @include('public.layouts.product', $product)
                        @endforeach
                    </div>
                </div>
            </section>
        @endif
    </div>


@endsection