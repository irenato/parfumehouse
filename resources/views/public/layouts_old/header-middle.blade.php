
<section>
    <section class="header-middle stuck-wrap">
        <div class="container">
            <div class="row">
                {{--<div class="col-lg-1 col-md-1 col-sm-2">--}}
                {{--</div>--}}
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <a href="#"  data-toggle="collapse" data-target=".navbar-collapse" class="menu_button">
                        <button type="button" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        Меню
                    </a>
                    <div class="navbar-collapse menu">
                        <ul class="nav navbar-nav">
                            @forelse($html_pages as $page)
                                <li class="top-line__mnu-item"><a href="/page/{!! $page->url_alias !!}" class="top-line__mnu-link">{!! $page->name !!}</a></li>
                            @empty
                            @endforelse
                            <li class="top-line__mnu-item"><a href="/articles" class="top-line__mnu-link">Статьи</a></li>
                            <li class="top-line__mnu-item"><a href="/categories/sale" class="top-line__mnu-link">Акции</a></li>
                        </ul>
                    </div>

                    <ul class="header-middle__phone">
                        <i class="header-middle__phone-icon header-middle__phone-icon_top">&#xe800;</i>
                        <li class="header-middle__phone-item">{!! $settings->main_phone_1 !!}</li>
                        <li class="header-middle__phone-item">{!! $settings->main_phone_2 !!}</li>
                    </ul>
                    @if($user_logged)
                        <a href="/user" class="header-middle__login-link mobile-icon"><i class="header-middle__search-icon">&#xe807;</i></a>
                        <a href="/logout" class="header-middle__logout-link mobile-icon"><i class="header-middle__search-icon">&#xe818;</i></a>
                    @else
                        <a href="javascript:void(0)" class="header-middle__login-link header-middle__login-link_unlogged mobile-icon"><i class="header-middle__search-icon">&#xe807;</i></a>
                    @endif
                    {{--<a href="#" data-mfp-src=".callback-form" class="header-middle__callback-btn popup-btn">Обратный звонок</a>--}}
                    <a href="/files/PRICELIST.pdf" class="header-middle__callback-btn" target="_blank"><i class="header-middle__price-icon"></i> Скачать прайс</a>
                </div>
                <div class="col-lg-5 col-lg-push-2 col-md-6 col-sm-6">
                    <div class="header-middle__right">
                        {{--@if($user_logged)--}}
                            {{--<a href="/user/wish_list" class="header-middle__fav-link">--}}
                                {{--<span class="header-middle__fav-counter">{!! count($user_wishlist) !!}</span>--}}
                                {{--<i class="header-middle__fav-icon">&#xe80a;</i>--}}
                                {{--<span class="header-middle__fav-name">Избранное</span>--}}
                            {{--</a>--}}
                        {{--@else--}}
                            {{--<a href="javascript:void(0)" class="header-middle__fav-link">--}}
                                {{--<span class="header-middle__fav-counter">0</span>--}}
                                {{--<i class="header-middle__fav-icon">&#xe80a;</i>--}}
                                {{--<span class="header-middle__fav-name">Избранное</span>--}}
                            {{--</a>--}}
                            {{--<div class="add-fav-popup add-fav-popup_header">--}}
                                {{--<span class="add-fav-popup__descr">Войдите, чтобы сохранять понравившиеся Вам товары</span>--}}
                                {{--<a href="/login" class="add-fav-popup__login-btn">Войти</a>--}}
                                {{--или<br>--}}
                                {{--<a href="/register" class="add-fav-popup__reg-btn">Зарегистрироваться</a>--}}
                            {{--</div>--}}
                        {{--@endif--}}

                        @if(Request::path() !== 'order')
                            <a id="cart-link" href="#" data-mfp-src=".cart-wrap" class="header-middle__cart-link animated{{ $products_quantity ? ' shake' : '' }}">
                        @else
                            <a href="" class="header-middle__cart-link">
                        @endif
                            <span id="header-middle__cart-counter" class="header-middle__cart-counter">{{ $products_quantity }}</span>
                            <i class="header-middle__cart-icon">&#xe804;</i>
                            <span class="header-middle__cart-name">Корзина</span>
                        </a>
                        <div class="cart-popup">
                            <span class="cart-popup__descr">В вашей корзине</span>
                            <span class="cart-popup__descr"><strong id="cart_popup_prod_quantity">{{ $products_quantity }} товара</strong></span>
                            <span class="cart-popup__descr"><strong id="cart_popup_prod_sum">на сумму {{ $products_sum }} грн.</strong></span>
{{--                            @if($products_sum >= 30 * $settings->rate)--}}
                                <a id="popup_accept_button" href="/order" class="cart-popup__btn">Оформить заказ</a>
                            {{--@else--}}
                                {{--<a id="popup_accept_button" href="javascript:void(0)" class="cart-popup__btn">Заказ от {{ 30 * $settings->rate }} грн</a>--}}
                            {{--@endif--}}
                        </div>
                        <a href="#" class="header-middle__search-popup"><i class="header-middle__search-icon">&#xe805;</i></a>
                        @if($user_logged)
                            <a href="/user" class="top-line__login-link">Личный кабинет</a>
                            <span class="top-login__separator">|</span>
                            <i class="top-line__login-icon">&#xe801;</i>
                            <a href="/logout" class="top-line__login-link">Выход</a>
                        @else
                            <i class="top-line__login-icon">&#xe801;</i>
                            <a href="/login" class="top-line__login-link" id="login">Войти</a>
                            <span class="top-login__separator">|</span>
                            <a href="/register" class="top-line__login-link">Регистрация</a>
                        @endif
                    </div>
                    {{--<a href="/files/PRICELIST.pdf" target="_blank" class="header-middle__price-link">--}}
                        {{--<i class="header-middle__price-icon">&#xe81e;</i>--}}
                        {{--<span class="header-middle__price-name">Скачать прайс</span>--}}
                    {{--</a>--}}
                    <a href="http://bz.parfumhouse.com.ua" target="_blank" class="header-middle__price-link">
                        <i class="header-middle__price-icon">&#xe820;</i>
                        <span class="header-middle__price-name">Бизнес под ключ</span>
                    </a>
                </div>
                <div class="col-lg-2 col-lg-pull-5 col-md-12 col-sm-12">
                    {!! Form::open(['route' => 'search', 'class' => 'header-middle__search-form']) !!}
                    {!! Form::input('search', 'text', null, ['class'=>'header-middle__search-input', 'placeholder' => 'Найдите товары'] ) !!}
                    <button type="submit" class="header-middle__search-btn"><i class="header-middle__search-icon">&#xe805;</i></button>
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </section>
    <section class="main-mnu">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-lg-push-5 col-md-2 col-md-push-5 col-sm-2 col-sm-push-5 col-xs-4 col-xs-push-4">
                    <div class="main-mnu__logo-wrap">
                        <a
                            @if(url()->current() == url('/'))
                                href="javascript:void(0)"
                            @else
                                href="/"
                            @endif
                            class="main-mnu__logo"><img src="/img/logo.svg" alt="Parfum House" title="Parfum House"></a>
                    </div>
                </div>
                <div class="col-lg-5 col-lg-pull-2 col-md-5 col-md-pull-2 col-sm-5 col-sm-pull-2 col-xs-4 col-xs-pull-4">
                    <div class="mobile-left">
                        <div class="btn-wrap mobile-icon">
                            <div class="btn-mnu" title="Меню">
                                <div class="btn-mnu__row"></div>
                                <div class="btn-mnu__row"></div>
                                <div class="btn-mnu__row"></div>
                            </div>
                        </div>
                        <a href="#" class="header-middle__search-popup_mobile mobile-icon"><i class="header-middle__search-icon">&#xe805;</i></a>
                    </div>
                    <ul class="main-mnu__part main-mnu__part_left">
                        @forelse($categories as $iterator => $category)
                            <li class="main-mnu__item @if($category->related_attribute_id) main-mnu__item_wsubmenu @endif">
                                <a href="/categories/{!! $category->url_alias !!}" class="main-mnu__link">{!! $category->name !!}</a>
                                @if($category->related_attribute_id)
                                <ul class="main-mnu__submenu">
                                    @foreach($category->related_attributes as $attribute)
                                        <li class="main-mnu__submenu-item">
                                            <a href="/categories/{!! $category->url_alias !!}?filter_attributes[{!! $attribute->attribute_id !!}][value][{!! $attribute->id !!}]"
                                               class="main-mnu__submenu-link"
                                               @if($attribute->image_href)
                                               style="background-image: url('/assets/attributes_images/{!! $attribute->image_href !!}');"
                                               @endif
                                            >{!! $attribute->name !!}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @break($iterator == 0)
                        @empty

                        @endforelse
                            <li class="main-mnu__item"><a href="/categories/popular" class="main-mnu__link">популярные</a></li>
                            <li class="main-mnu__item"><a href="/categories/new" class="main-mnu__link">новинки</a></li>
                    </ul>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <div class="mobile-right">
                        <a id="cart-link-mobile" href=".cart-wrap" class="mobile-mnu__cart-link mobile-icon">
                            <i class="header-middle__cart-icon">&#xe804;</i>
                            {{--<span id="header-middle__cart-counter" class="header-middle__cart-counter">{{ $products_quantity }}</span>--}}
                        </a>
                        @if($user_logged)
                            <a href="/user/wish_list" class="header-middle__fav-link mobile-icon">
                                <span class="header-middle__fav-counter">{!! count($user_wishlist) !!}</span>
                                <i class="header-middle__fav-icon">&#xe80a;</i>
                            </a>
                        @else
                            <a href="javascript:void(0)" class="header-middle__fav-link">
                                <span class="header-middle__fav-counter">0</span>
                                <i class="header-middle__fav-icon">&#xe80a;</i>
                            </a>
                            <div class="add-fav-popup add-fav-popup_header">
                                <span class="add-fav-popup__descr">Войдите, чтобы сохранять понравившиеся Вам товары</span>
                                <a href="/login" class="add-fav-popup__login-btn">Войти</a>
                                или<br>
                                <a href="/register" class="add-fav-popup__reg-btn">Зарегистрироваться</a>
                            </div>
                        @endif
                    </div>
                    <ul class="main-mnu__part main-mnu__part_right">
                        @forelse($categories as $iterator => $category)
                            @continue($iterator <= 0)
                            <li class="main-mnu__item @if($category->related_attribute_id) main-mnu__item_wsubmenu @endif">
                                <a href="/categories/{!! $category->url_alias !!}" class="main-mnu__link">{!! $category->name !!}</a>
                                @if($category->related_attribute_id)
                                    <ul class="main-mnu__submenu">
                                        @foreach($category->related_attributes as $attribute)
                                            <li class="main-mnu__submenu-item">
                                                <a href="/categories/{!! $category->url_alias !!}?filter_attributes[{!! $attribute->attribute_id !!}][value][{!! $attribute->id !!}]"
                                                   class="main-mnu__submenu-link"
                                                   @if($attribute->image_href)
                                                   style="background-image: url('/assets/attributes_images/{!! $attribute->image_href !!}');"
                                                        @endif
                                                >{!! $attribute->name !!}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @empty

                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </section>
</section>
