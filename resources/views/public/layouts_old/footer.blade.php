<footer>

    <section class="footer-main">
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        <form class="footer-top__form">
                            {!! csrf_field() !!}
                            <i class="footer-top__mail-icon">&#xe812;</i>
                            <input type="email" name="email" placeholder="Ваш e-mail" class="footer-top__input">
                            <button type="submit" class="footer-top__btn">Подписаться</button>
                        </form>
                    </div>
                    <div class="col-md-5 col-sm-6">
                        <div class="footer-top__soc-wrap clearfix">
                            <span class="footer-top__soc-text">Присоединяйтесь</span>
                            <ul class="footer-main__soc-list">
                                <li class="footer-main__soc-item">
                                    <a href="{!! $socials->facebook or '' !!}" class="footer-main__soc-link" target="_blank" rel="nofollow">
                                        <i class="footer-top__soc-icon">&#xe813;</i>
                                    </a>
                                </li>
                                <li class="footer-main__soc-item">
                                    <a href="{!! $socials->instagram or '' !!}" class="footer-main__soc-link" target="_blank" rel="nofollow">
                                        <i class="footer-top__soc-icon">&#xf16d;</i>
                                    </a>
                                </li>
                                {{--<li class="footer-main__soc-item">--}}
                                    {{--<a href="{!! $socials->vkontakte or '' !!}" class="footer-main__soc-link" target="_blank" rel="nofollow">--}}
                                        {{--<i class="footer-top__soc-icon">&#xf189;</i>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-main__logo">
                        <img src="/img/logo.svg" alt="" class="footer-main__logo-img" alt="footer-logo" title="Parfumhouse">
                    </div>
                </div>
                <div class="col-md-2 col-sm-3">
                    <ul class="footer-main__mnu">
                        @forelse($html_pages as $page)
                            <li class="footer-main__mnu-item"><a href="/page/{!! $page->url_alias !!}" class="footer-main__mnu-link">{!! $page->name !!}</a></li>
                        @empty
                        @endforelse
                        <li class="footer-main__mnu-item"><a href="/articles" class="footer-main__mnu-link">Статьи</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-3">
                    <ul class="footer-main__mnu">
                        @forelse($categories as $category)
                            <li class="footer-main__mnu-item"><a href="/categories/{!! $category->url_alias !!}" class="footer-main__mnu-link">{!! $category->name !!}</a></li>
                        @empty
                        @endforelse
                    </ul>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="footer-main__right-wrap clearfix">
                        <div class="footer-main__phone-wrap">
                            <i class="header-middle__phone-icon footer-main__phone-icon">&#xe800;</i>
                            <span class="footer-main__phone-title">Связаться с нами</span>
                            <ul class="footer-main__phone">
                                <li class="footer-main__phone-item">{!! $settings->main_phone_1 !!}</li>
                                <li class="footer-main__phone-item">{!! $settings->main_phone_2 !!}</li>
                            </ul>
                        </div>
                        <div class="footer-main__bz-logo">
                            <span class="footer-main__bz-title">Разработка сайта:</span>
                            <a href="//zavodbiz.com/" target="_blank" rel="nofollow" class="footer-main__bz-logo-link">Первый бизнес завод</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bootom-line">
        <div class="container">
            <span class="bootom-line__item">© <strong class="bootom-line__item_upper">Parfum house</strong></span>
            <span class="bootom-line__item">Все права защищены</span>
        </div>
    </section>

</footer>

<!-- <a href="http://intercharm.kiev.ua/ru-RU/registration.aspx#personal" class="button_promocode">
    <img src="/img/button_promocode.png" alt="">
</a> -->

<atricle class="mobile-mnu">
    <div class="btn-wrap">
        <div class="btn-mnu btn-mnu_mobile" title="Меню">
            <div class="btn-mnu__row"></div>
            <div class="btn-mnu__row"></div>
            <div class="btn-mnu__row"></div>
        </div>
    </div>
    <div class="mobile-mnu__logo">
        <img src="/img/logo.svg" alt="footer-logo" title="Parfumhouse">
    </div>
    <ul class="mobile-mnu__nav">
        @forelse($html_pages as $page)
            <li class="mobile-mnu__item"><a href="/page/{!! $page->url_alias !!}" class="mobile-mnu__link">{!! $page->name !!}</a></li>
        @empty
        @endforelse
            <li class="mobile-mnu__item"><a href="/articles" class="mobile-mnu__link">Статьи</a></li>
		    <li class="mobile-mnu__item"><a href="/categories/sale" class="mobile-mnu__link">Акции</a></li>
            <li class="mobile-mnu__item"><a href="//bz.parfumhouse.com.ua" target="_blank" class="mobile-mnu__link">Бизнес под ключ</a></li>
        @forelse($categories as $category)
            <li class="mobile-mnu__item"><a href="/categories/{!! $category->url_alias !!}" class="mobile-mnu__link">{!! $category->name !!}</a></li>
        @empty
        @endforelse
    </ul>
</atricle>

<article class="mobile-search">
    <form class="header-middle__search-form header-middle__search-form_popup">
        <input type="search" class="header-middle__search-input" placeholder="Поиск..">
        <button type="submit" class="header-middle__search-btn"><i class="header-middle__search-icon">&#xe805;</i></button>
        <a href="#" class="header-middle__search-popup_mobile"><i class="header-middle__search-icon">&#xe806;</i></a>
    </form>
</article>

<article class="mobile-login">
    <h4 class="mobile-login__title">Вход в личный кабинет</h4>
    <a href="#" class="header-middle__login-link header-middle__login-link_unlogged mobile-icon"><i class="header-middle__search-icon">&#xe806;</i></a>
    <form class="login-form" action="/login" method="post">
        {!! csrf_field() !!}
        <input type="text" name="email" class="login-form__input" placeholder="Email">
        <input type="password" name="password" class="login-form__input" placeholder="Пароль">
        <button type="submit" class="login-form__btn">Войти</button>
    </form>
    <div class="mobile-login__links">
        <a href="/forgotten" class="mobile-login__forgot">Забыли пароль?</a>
        <a href="/register" class="mobile-login__registration">Регистрация</a>
    </div>
</article>


<div class="hidden">
    <div class="del-info-popup">
        <i class="del-info-popup__icon">&#xe812;</i>
        <div class="del-info-popup__text">Вы подписаны на рассылку</div>
    </div>

    <form class="callback-form" id="callback-form">
        {!! csrf_field() !!}
        <span class="callback-form__title">Обратный звонок</span>
        <span class="error-message hidden">
        </span>
        <label for="callback-form__name" class="callback-form__label">Имя</label>
        <input type="text" name="name" id="callback-form__name" class="callback-form__input">
        <label for="callback-form__phone" class="callback-form__label">Телефон</label>
        <input type="text" name="phone" id="callback-form__phone" class="callback-form__input">
        <button type="submit" class="callback-form__btn">Заказать</button>
    </form>
</div>

