<h2 id="cart-title" class="section-title">Корзина</h2>
<form class="cart-form">
    <div class="cart-table-wrap">
        <table id="cart-table" class="cart-table">
{{--            {{ var_dump($products) }}--}}
            @foreach($products as $product)
                <tr class="cart-table__row">
                    <td class="cart-table__item cart-table__thumb">
                        <a href="/product/{{ $product['alias'] }}" class="cart-table__thumb-link" style="background-image: url( {{ $product['href'] }} ) "></a>
                        <a href="javascript:void(0)" class="cart-table__delete_mob cart-table__delete-link cart-table__delete-link_mobile">
                            <i class="header-middle__search-icon cart-table__delete-icon">&#xe806;</i>
                        </a>
                    </td>
                    <td class="cart-table__item cart-table__name" style="font-size: 12px;">
                        {{ $product['name'] }}<span class="cart-table__name_accent"></span>
                    </td>
                    <td class="cart-table__item cart-table__val">
                        {{ $product['product_quantity'] }} {{ $product['measure'] }}
                    </td>
                    <td data-product-id="{{ $product['id'] }}" class="cart-table__item cart-table__quantity">
                    <input id="cart-table__input" data-product-price="{{ $product['price'] }}" type="number" step="1" min="1" max name="quantity" class="cart-table__input" value="{{ $product['quantity'] }}">
                    </td>
                    <td class="cart-table__item cart-table__price">
                        {{ $product['sum_price'] }} грн
                    </td>
                    <td class="cart-table__item cart-table__delete">
                        <a href="javascript:void(0)" class="cart-table__delete-link">
                            <i class="header-middle__search-icon cart-table__delete-icon">&#xe806;</i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="cart-result clearfix">
        <a id="cart_reload-btn" href="javascript:void(0)" class="cart-result__reload-btn">Обновить корзину</a>
        <table class="cart-result__table">
            <tr class="cart-result__table-row">
                <td class="cart-result__item">Сумма к оплате:</td>
                <td id="cart-result__item" class="cart-result__item cart-result__item_right">{{ $sum_cart }} грн</td>
            </tr>
            <tr class="cart-result__table-row">
                <td class="cart-result__item">Доставка:</td>
                <td class="cart-result__item cart-result__item_right">0 грн</td>
            </tr>
            <tr class="cart-result__table-row">
                <td class="cart-result__item cart-result__item_accent">Итого:</td>
                <td id="cart-result__item_accent" class="cart-result__item cart-result__item_accent cart-result__item_right">{{ $sum_cart }} грн</td>
            </tr>
        </table>
    </div>
    <div class="btn-wrap clearfix">
        <a id="from_cart_to_order_button" href="javascript:void(0)" class="cart-btn cart-btn_right cart-btn_disabled">Минимальная сумма заказа {{ 30 * $settings->rate }} грн</a>
        <a href="/" class="cart-btn cart-btn_left cart-btn_close">Продолжить покупки</a>
    </div>
</form>