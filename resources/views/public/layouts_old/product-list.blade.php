@if(!is_null($product))
<div class="col-xs-12 list">
    <div class="product-card" id="product-card" onmouseleave="$(this).find('.add-fav-popup').removeClass('add-fav-popup_active')" style="position: relative;">

        <div style="float: left; width: 20%; position: relative; min-width: 150px;">
            @if($product->sale)
                <span class="product-card__stock">-{!! $product->sale !!}%</span>
            @endif
            @if($user_logged)
                <a data-prod-id='{{ $product->id}}'
                   data-user-id='{{ $user_id }}'
                    id="add-to-wishlist"
                   href="javascript:void(0)"
                   class="product-card__add-fav {!! in_array($product->id, $user_wishlist) ? 'product-card__add-fav_active' : '' !!}">
                    <i class="product-card__add-fav-icon">&#xe80a;</i>
                </a>

            @else
                <a href="javascript:void(0)"
                   class="product-card__add-fav noactive"
                   onclick="$(this).next('.add-fav-popup').addClass('add-fav-popup_active')">
                    <i class="product-card__add-fav-icon">&#xe80a;</i>
                </a>
                <div class="add-fav-popup">
                    <span class="add-fav-popup__descr">Войдите, чтобы сохранять понравившиеся Вам товары</span>
                    <a href="/login" class="add-fav-popup__login-btn">Войти</a>
                    или<br>
                    <a href="/register" class="add-fav-popup__reg-btn">Зарегистрироваться</a>
                </div>
            @endif

                <a href="/product/{{ $product->url_alias }}" class="product-card__thumbnail"
                   @if(!is_null($product->get_product_image))) style="height: 180px; background-image: url({{ $product->get_product_image->get_current_file_url('product_list') }});" @endif>{!! !empty($product->label) ? '<img class="label" src="/assets/attributes_images/'.$product->label.'.png">' : '' !!} </a>
        </div>

        <div style="float: left; width: 80%; position: relative; text-align: left; padding: 15px 0 15px 15px; max-width: calc(100% - 150px);">
            <a href="/product/{{ $product->url_alias }}" class="product-card__title">{!! !empty($product->articul) ? $product->articul . ' / ' : '' !!}{{ $product->name }}</a>

            <form class="product-card__form" >
                <div class="product-card__descr">
                    <span class="product-card__vol">{!! $product->capacity !!} {!! $product->capacity ? $product->measures->name : '' !!}</span>
                    <span class="product-card__amount">
                    <input type="number" value="1" min="1" id="product-card__amount-input" class="product-card__amount-input">
                </span>
                    <span class="product-card__price">
                    <span class="product-card__price_inner">
                        {{--@if($product->sale)<i class="product-card__price-icon">&#xe81f;</i>@endif--}}
                            {{ round($product->price, 2) }} $</span>
                    <span class="product-card__price_inner">
                        {{--@if($product->sale)<i class="product-card__price-icon">&#xe81f;</i>@endif--}}
                            {{ round($product->price * $rate, 2) }} грн</span>
                </span>
                </div>
                <button type="button"
                        id="product-card__btn"
                        class="product-card__btn"
                        data-product-id="{!! $product->id !!}"
                        data-user-id="{!! $user_id !!}">В корзину
                </button>
            </form>

            <div class="product-card__stars-wrap">

                <ul class="product-card__stars-list">
                    @for($i=1; $i<=5; $i++)
                        @break(count($product->reviews) == 0)

                        @if($i <= $product->rating)
                            <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe810;</i></li>
                        @else
                            <li class="product-card__stars-item"><i class="product-card__star-icon">&#xe811;</i></li>
                        @endif
                    @endfor
                </ul>

                <span class="product-card__review">
                <a href="/product/{!! $product->url_alias !!}#reviews" class="product-card__review-link">{{count($product->reviews)}} отзывов</a>
            </span>
            </div>
        </div>

    </div>
</div>
@endif