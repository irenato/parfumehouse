<!DOCTYPE html>
<html>
@include('public.layouts.header')

<body class="{{ strpos(Request::path(), 'product') === 0 ? '' : 'account-body' }}{{ Request::path()=='/' ? ' home' : '' }}">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZM2JG"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="main-header">
@include('public.layouts.header-main')
@include('public.layouts.header-middle')
</header>

<div class="container">
    @yield('breadcrumbs')
</div>

@yield('content')


@include('public.layouts.footer')

@if(Request::path() !== 'order')
    @include('public.layouts.popup-cart')
@endif

@if(Request::path() !== 'login')
    @include('public.layouts.popup-login')
@endif

@include('public.layouts.footer-scripts')

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = '4V70Ljal0i';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>