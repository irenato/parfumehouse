@extends('public.layouts.main')
@section('meta')
    <title>{!! $product->name !!} {!! empty($product->capacity) ? '' : $product->capacity.'мл' !!} {!! $product->articul !!} | Parfumhouse</title>
    <meta name="description" content="Купить {!! $product->name !!} {!! empty($product->capacity) ? '' : $product->capacity.'мл' !!} {!! $product->articul !!} | Parfumhouse">
    {{--<title>{!! $product->meta_title !!}</title>--}}
    {{--<meta name="description" content="{!! $product->meta_description !!}">--}}
    {{--<meta name="keywords" content="{!! $product->meta_keywords !!}">--}}
@endsection

@section('content')

    <main class=main-container>
        <section class=product-header>
            <div class=product-wrapper>
                <nav class=breadcrumbs>
                    @section('breadcrumbs')
                        {!! Breadcrumbs::render('product', $product, $product->category) !!}
                    @endsection
                </nav>
                <div class=product-name>{{ $product->name }}</div>
                <nav class=product-menu__tabs>
                    <ul class=product-menu>
                        <li class="product-menu__item active"> Все о товаре</li>
                        <li class=product-menu__item> Отзывы (<span class=rev-count>{{ $reviews->total() }}</span>)</li>
                        @if($related_category)
                            <li class="product-menu__item bottles-tab"> {{ $related_category->name }}</li>
                        @endif
                    </ul>
                </nav>
            </div>
        </section>
        <div class="product-content active">
            <section class=product-content__main>
                <div class=product-wrapper>
                    <div class="col-lg-4 product-pic__wrapper">
                        @foreach($gallery as $image)
                            <img src="{{ $image->get_current_file_url() }}" alt="{{ $product->name }}"></div>
                    @endforeach
                    <div class="col-lg-8 product-specs__wrapper">
                        <div class="col-md-8 product-buy__wrapper">
                            <div class=product-buy__inner>
                                <div class=product-buy__top>
                                    <table class=product-table__specs>
                                        @if(!empty($product->articul))
                                            <tr>
                                                <td>Артикул:</td>
                                                <td>{{ $product->articul }}</td>
                                            </tr>
                                        @endif
                                        @if(!empty($product_attributes))
                                            @foreach($product_attributes as $attribute_name => $attribute_value)
                                                <tr>
                                                    <td>{{ $attribute_name }}</td>
                                                    <td>{{ $attribute_value }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        @if($product->quantity)
                                            <tr>
                                                <td>Количество в упаковке:</td>
                                                <td>{{ $product->quantity }}</td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                                <div class=product-buy__bot>
                                    <div class=product-price>
                                        <span class=product-price__title>Цена:</span>
                                        <span class=product-price__usd> {{ round($product->price, 2) }} $</span>
                                        <span class=product-price__uah>{{ round($product->price * $rate, 2) }} грн</span>
                                        <div class=product-counter>
                                            <div class=volume-slider>
                                                <div>{{ $product->capacity }} {{ $product->measures->name }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=clearfix></div>
                                    <div class=product-buy__main>
                                        <div class=product-socials>
                                            <a class="product-socials__item icon-facebook" href="{{ $socials->facebook }}">Мы в facebook</a>
                                            <a class="product-socials__item icon-insta" href="{{ $socials->instagram }}">Мы в instagram</a>
                                        </div>
                                        <button class=product-buy__btn>ДОБАВИТЬ В КОРЗИНУ</button>
                                    </div>
                                </div>
                                <div class="product-label new"><span>новинки</span></div>
                            </div>
                        </div>
                        <div class="col-md-4 product-delivery__wrapper">
                            <div class=product-delivery__inner>
                                <div class=product-delivery__banner><span class=product-delivery__banner-title>Бесплатная доставка</span> <span class=product-delivery__banner-text>при заказе от 100$ (1$=27.00грн)</span>
                                </div>
                                <ul class=product-delivery__types>
                                    <li class=product-delivery__type><i class="product-delivery__type-icon icon-delivery"></i>
                                        <div class=product-delivery__type-descr><span class=product-delivery__type-title>Доставка</span>
                                            <ul class=product-delivery__type-list>
                                                <li class=product-delivery__type-item>В отделение Новой почты</li>
                                                <li class=product-delivery__type-item>Курьерская доставка</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class=product-delivery__type><i class="product-delivery__type-icon icon-money"></i>
                                        <div class=product-delivery__type-descr><span class=product-delivery__type-title>Оплата</span>
                                            <ul class=product-delivery__type-list>
                                                <li class=product-delivery__type-item>Наличными</li>
                                                <li class=product-delivery__type-item>Оплата картой</li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                                <div class=product-delivery__footer>
                                    <div class=product-delivery__footer-title>Минимальная сумма заказа</div>
                                    <span class=product-delivery__footer-text>30$ (1$=27.00 грн)</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class=product-content>
            <section class=product-content__reviews>
                <div class=product-wrapper>
                    <div class="col-md-9 product-rev__wrapper"><span class=product-reviews__title>Оставьте отзыв</span>
                        <form action="" class="review-form pbz_form clear-styles" id="give-review__form" data-error-title="Ошибка отправки!" data-error-message="Попробуйте отправить заявку через некоторое время."
                              data-success-title="Спасибо за заявку!" data-success-message="Наш менеджер свяжется с вами в ближайшее время.">
                            {!! csrf_field() !!}
                            <div class=row>
                                <div class="col-md-6 review-form-input__wrapper">
                                    <span class=review-form-input__title>Достоинства</span>
                                    <input type=text name="advantages" class="review-form-input" data-validate-required="Обязательное поле" data-title=Достоинства></div>
                                <div class="col-md-6 review-form-input__wrapper">
                                    <span class=review-form-input__title>Недостатки</span>
                                    <input type=text name="flaws" class="review-form-input" data-validate-required="Обязательное поле" data-title=Недостатки></div>
                            </div>
                            <div class=row>
                                <div class="col-md-12 review-form-input__wrapper">
                                    <span class=review-form-text__title>Комментарий</span>
                                    <textarea name="review" class=review-form-text></textarea></div>
                            </div>
                            <div class=row>
                                <div class="col-md-6 review-form-input__wrapper"><span class=review-form-input__title>Имя</span>
                                    <input type=text name=name class=review-form-input data-validate-required="Обязательное поле" data-title=Имя></div>
                                <div class="col-md-6 review-form-input__wrapper"><span class=review-form-input__title>E-mail</span>
                                    <input type=text name=email class=review-form-input data-validate-required="Обязательное поле" data-title=E-mail></div>
                            </div>
                            <div class=row>
                                <div class="col-md-12 review-form__btn-wrapper">
                                    <input type="hidden" name="product_id" value="{!! $product->id !!}">
                                    <button type="submit" class=review-form__btn>ОСТАВИТЬ ОТЗЫВ</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3 product-deliv__wrapper">
                        <div class=product-delivery__inner>
                            <div class=product-delivery__banner><span class=product-delivery__banner-title>Бесплатная доставка</span> <span class=product-delivery__banner-text>при заказе от 100$ (1$=27.00грн)</span>
                            </div>
                            <ul class=product-delivery__types>
                                <li class=product-delivery__type><i class="product-delivery__type-icon icon-delivery"></i>
                                    <div class=product-delivery__type-descr><span class=product-delivery__type-title>Доставка</span>
                                        <ul class=product-delivery__type-list>
                                            <li class=product-delivery__type-item>В отделение Новой почты</li>
                                            <li class=product-delivery__type-item>Курьерская доставка</li>
                                        </ul>
                                    </div>
                                </li>
                                <li class=product-delivery__type><i class="product-delivery__type-icon icon-money"></i>
                                    <div class=product-delivery__type-descr><span class=product-delivery__type-title>Оплата</span>
                                        <ul class=product-delivery__type-list>
                                            <li class=product-delivery__type-item>Наличными</li>
                                            <li class=product-delivery__type-item>Оплата картой</li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                            <div class=product-delivery__footer>
                                <div class=product-delivery__footer-title>Минимальная сумма заказа</div>
                                <span class=product-delivery__footer-text>30$ (1$=27.00 грн)</span></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class=product-content>
            <section class=product-content__bottles>
                <div class=product-wrapper><span class=bottles-title>Флаконы для Chance (Chanel) новая коллекция</span></div>
                <div class=wrapper>
                    <div class=slider-wrapper>
                        <div class=tab-items>
                            <div class=item>
                                <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                               src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                               alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                            class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                    <ul class=item-price__wrapper>
                                        <li class="item-price__section item-vol">
                                            <div class=volume-slider>
                                                <div>100 мл</div>
                                                <div>200 мл</div>
                                                <div>300 мл</div>
                                            </div>
                                        </li>
                                        <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                        <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                    </ul>
                                    <button class=item-btn-tocart>Добавить в корзину</button>
                                </div>
                            </div>
                            <div class=item>
                                <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                               src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                               alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                            class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                    <ul class=item-price__wrapper>
                                        <li class="item-price__section item-vol">
                                            <div class=volume-slider>
                                                <div>100 мл</div>
                                                <div>200 мл</div>
                                                <div>300 мл</div>
                                            </div>
                                        </li>
                                        <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                        <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                    </ul>
                                    <button class=item-btn-tocart>Добавить в корзину</button>
                                </div>
                            </div>
                            <div class=item>
                                <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                               src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                               alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                            class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                    <ul class=item-price__wrapper>
                                        <li class="item-price__section item-vol">
                                            <div class=volume-slider>
                                                <div>100 мл</div>
                                                <div>200 мл</div>
                                                <div>300 мл</div>
                                            </div>
                                        </li>
                                        <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                        <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                    </ul>
                                    <button class=item-btn-tocart>Добавить в корзину</button>
                                </div>
                            </div>
                            <div class=item>
                                <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                               src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                               alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                            class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                    <ul class=item-price__wrapper>
                                        <li class="item-price__section item-vol">
                                            <div class=volume-slider>
                                                <div>100 мл</div>
                                                <div>200 мл</div>
                                                <div>300 мл</div>
                                            </div>
                                        </li>
                                        <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                        <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                    </ul>
                                    <button class=item-btn-tocart>Добавить в корзину</button>
                                </div>
                            </div>
                            <div class=item>
                                <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                               src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                               alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                            class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                    <ul class=item-price__wrapper>
                                        <li class="item-price__section item-vol">
                                            <div class=volume-slider>
                                                <div>100 мл</div>
                                                <div>200 мл</div>
                                                <div>300 мл</div>
                                            </div>
                                        </li>
                                        <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                        <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                    </ul>
                                    <button class=item-btn-tocart>Добавить в корзину</button>
                                </div>
                            </div>
                            <div class=item>
                                <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                               src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                               alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                            class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                    <ul class=item-price__wrapper>
                                        <li class="item-price__section item-vol">
                                            <div class=volume-slider>
                                                <div>100 мл</div>
                                                <div>200 мл</div>
                                                <div>300 мл</div>
                                            </div>
                                        </li>
                                        <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                        <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                    </ul>
                                    <button class=item-btn-tocart>Добавить в корзину</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <section class=product-content__descr>
            <div class=product-wrapper><span class=product-descr__title>Описание Chance (Chanel) новая коллекция</span>
                <div class=product-descr__info>
                    <div class=product-descr__notes>
                        <div class=product-descr__notes-top><img src=/assets/static/png/notes-top_44513b877dfcb1c0915416b9f2c451f3.png alt="">
                            <div class=product-descr__notes-text><span class=product-descr__notes-title>Верхние ноты</span> <span class=product-descr__notes-info>Ананас, Ирис, Пачули, Розовый перец, Гиацинт</span></div>
                        </div>
                        <div class=product-descr__notes-mid><img src=/assets/static/png/notes-mid_dc2a5fb405b697c642f25b4c62aa50cd.png alt="">
                            <div class=product-descr__notes-text><span class=product-descr__notes-title>Сердечные ноты</span> <span class=product-descr__notes-info>Жасмин, Лимон</span></div>
                        </div>
                        <div class=product-descr__notes-bot><img src=/assets/static/png/notes-bot_e2d3210106ecdb50773d44dc9250db5f.png alt="">
                            <div class=product-descr__notes-text><span class=product-descr__notes-title>Шлейфовые ноты</span> <span class=product-descr__notes-info>Мускус, Пачули, Ваниль, Ветивер</span></div>
                        </div>
                    </div>
                    <div class=product-descr><strong>Группа:</strong> шипровые, цветочные<br> Chanel Chance — это показатель отменного вкуса, изысканности и женственности. Необычная композиция аромата ярких цветов и
                        волны
                        свежести чудесным образом смешиваются с чувственными, сладкими и пряными нотками. Это шанс дамы стать неотразимой, которым трудно пренебречь.<br> Chance Chanel — это аромат для женщин, принадлежит
                        к
                        группе шипровые цветочные. Аромат характерен осязаемой легкостью и свежестью, где интересно переплетаются свежие цитрусовые ноты, запах жасмина и грейпфрута, что находятся на основном его плане.
                        Его
                        базовые нотки основываются на сладостном жасмине, ирисе и гиацинте. Глубинный шлейф аромата различает более спокойные веяния амбры, мускуса и ванили. <br><br> <strong>Ноты:</strong> Ананас, Ирис,
                        Пачули, Розовый перец, Гиацинт , Жасмин, Лимон , Мускус, Пачули, Ваниль, Ветивер
                    </div>
                </div>
            </div>
        </section>
        <section class=product-content__reviews>
            <div class=product-wrapper>
                <div class=product-reviews>
                    <div class=product-reviews__wrapper><span class=product-reviews__title>Отзывы</span>
                        <div class=product-reviews__container>Нет отзывов</div>
                    </div>
                    <button class=product-reviews__btn>ОСТАВИТЬ ОТЗЫВ</button>
                </div>
            </div>
        </section>
        <section class=section-bottles>
            <div class=product-wrapper><span class=bottles-title>Флаконы для Chance (Chanel) новая коллекция</span></div>
            <div class=wrapper>
                <div class=slider-wrapper>
                    <div class=new-items>
                        <div class=item>
                            <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                           src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                           alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                        class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                <ul class=item-price__wrapper>
                                    <li class="item-price__section item-vol">
                                        <div class=volume-slider>
                                            <div>100 мл</div>
                                            <div>200 мл</div>
                                            <div>300 мл</div>
                                        </div>
                                    </li>
                                    <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                    <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                </ul>
                                <button class=item-btn-tocart>Добавить в корзину</button>
                            </div>
                        </div>
                        <div class=item>
                            <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                           src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                           alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                        class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                <ul class=item-price__wrapper>
                                    <li class="item-price__section item-vol">
                                        <div class=volume-slider>
                                            <div>100 мл</div>
                                            <div>200 мл</div>
                                            <div>300 мл</div>
                                        </div>
                                    </li>
                                    <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                    <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                </ul>
                                <button class=item-btn-tocart>Добавить в корзину</button>
                            </div>
                        </div>
                        <div class=item>
                            <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                           src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                           alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                        class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                <ul class=item-price__wrapper>
                                    <li class="item-price__section item-vol">
                                        <div class=volume-slider>
                                            <div>100 мл</div>
                                            <div>200 мл</div>
                                            <div>300 мл</div>
                                        </div>
                                    </li>
                                    <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                    <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                </ul>
                                <button class=item-btn-tocart>Добавить в корзину</button>
                            </div>
                        </div>
                        <div class=item>
                            <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                           src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                           alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                        class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                <ul class=item-price__wrapper>
                                    <li class="item-price__section item-vol">
                                        <div class=volume-slider>
                                            <div>100 мл</div>
                                            <div>200 мл</div>
                                            <div>300 мл</div>
                                        </div>
                                    </li>
                                    <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                    <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                </ul>
                                <button class=item-btn-tocart>Добавить в корзину</button>
                            </div>
                        </div>
                        <div class=item>
                            <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                           src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                           alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                        class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                <ul class=item-price__wrapper>
                                    <li class="item-price__section item-vol">
                                        <div class=volume-slider>
                                            <div>100 мл</div>
                                            <div>200 мл</div>
                                            <div>300 мл</div>
                                        </div>
                                    </li>
                                    <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                    <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                </ul>
                                <button class=item-btn-tocart>Добавить в корзину</button>
                            </div>
                        </div>
                        <div class=item>
                            <div class=item-inner><a href="" class=item-logo-wrapper> <img class=item-logo
                                                                                           src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAfCAYAAACxmAC4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYwREI0NENDRDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYwREI0NENERDNEMjExRTc4Rjc1ODQ5MDgyOUUyMjU1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjBEQjQ0Q0FEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjBEQjQ0Q0JEM0QyMTFFNzhGNzU4NDkwODI5RTIyNTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5XiabdAAAFAElEQVR42uxZW2wUVRg+M7OdvRTFdgtUsMVWvESrFG9FUWL1wUtMBEyoVhI1xGg0RqNGJD4YHzQ+EN7UZ21sQo2XB68BAipiDZJ4qaCoUagriUAr4E53dmfn+P/022aczMyeaUmbbviTL53OOTPnnO/8l+/MakLNeghbCSOidqzeMIw1CxfM70ul6//XoCs8nCI8S+gUtWXSrKtrpb8vENJxSekifEVYVmOkaELXimRry2WnycqfFBWokHI94XVCltBUQ6TUFe3iX7puPGAXxm6l/y9V9RQm4mzCj4Q/QFCtmGPoukt5ZXnRtu9PZ+qLBMGoRsp1hN9w/SXhmhoh5CnCXaZpziN8TNc3En5V9ZSrQAbbT4QMobkGSLmEcI/jlI96yVCx+YRXfMQ9RFhdA6Rw/riP0Fu5saS9Xan6LCccILiee18QrpztCZZLcErTPuX19zc2ivezWbHp+PGJDomIh7kEb/Hd+5mQJJxLODxLSVnYk8nYK01T7nMcE4XkhLdDGCkLINoO+AUPKhFXoXdmExNz9ImgyN6WSj7erOmFNsNYXJCyke4NEA5VC58ueIUb0LaLsHS2uce/risaDYPRdJamC0vKg3kpB2iBF1A8bVh17Bh3m0u4O4wUXvTukDYu0Qa7Ycx5pTBovULfdIy+fuM1tRAuQ5WZEJyHSiVuNPXxFJDVhGhHGvgHXXjM1YmQ0OH7v0QMPERYQXg7pJ0nVEYZ70S4ZU5J63Fz8Y6tPm/sRi5LIFQl7rNwZD2Rj5gTh8E6QgcLM/RNgNgxFIl3hx1naG+pNNBtmt22lHNtIefQIE/jHUzOliBSrsViZMQEWLs8HEHKRZjUMpT2Dwm/gygNXnYT4TnCZuzuk4QjPHHEt+uRBhzOGwmvhiR4Fl+PErYTXiTkPG3sFU8QLuY1laU88VY+/3mbru/fWSz23pFM7jZ17bDHmy8MWtDzhMUKbvpyRL+bcV56sMo7+MzxCNBdpW8n5qYHqO43Q+bCp+D16FOxWwgvaeNVp6e/oUF8kM1W2uYxgf4BmpEvDiqQ8gPhhpC2DOL6jSrvYK1wBWJ5R5W+38J7Wjz3zoGg3OibswaS2Rs/wim/Yn+yR1AYXM2apXd0VNw5nmRDxRsLtn2KCY1D6HJPnvALpPdCqlfQYocUxzziS/BrEDI538auw3VfQLhxVeUcYkOjBGZqr/Gufa04wUrctwedQLEjKjaCRKhiJY+2SiD+P/G0c/K/nfAZPC8qL+6PKl8Va8GuD8cof98RViq8u/oHnxhfzDyfNSwC+34D4V7sfJ9XiEV9OkAeSkdNnM8038TUBNv4LBWTgNNlJsJpKQ6pe1G2nRh6ZgSHw0V+md+EpLQKgm1tjJ1zIJBaoSWm0yzkNB63n1CI+bwLYjdDGuS8pFjQFKOIRT0GKfzindAJM0FKzpdT4knsVIodYkOhUBj0e4qFarEtRhXwGmf3Z0CmO42kjEGBdkxy3qZhGMPlcnkwLKew8twzyckdhactmYG8sgtleTKWsCzrb9u2A5PN+ZDfuSlMbo9PNU6XfQ9v6Z3Es5qUUicEktKF0joVG4RemYkq9BoE3Xoo4zgyQIaVpQ4sairGu3US4q+SqxIxPg+q9jUD+nIF3ITcxge/x4Ta71NGwCbqGONUWTsddp7nXLIoxveWVqH+C0EbDm1RpK3AybqaJQPGTeE0fcbOmIL9J8AAQfk57W5r/hcAAAAASUVORK5CYII=
                                                                                           alt=ameli> </a> <a href=""><img class=item-pic src=/assets/static/jpg/1_c71ed62174742891e2007fc623d3641c.jpg alt=""></a> <a
                                        class=item-title href="">402 / Chanel Chance Tendre / Coco Chanel</a> <a class=item-reviews href=""><span>0</span> отзывов</a>
                                <ul class=item-price__wrapper>
                                    <li class="item-price__section item-vol">
                                        <div class=volume-slider>
                                            <div>100 мл</div>
                                            <div>200 мл</div>
                                            <div>300 мл</div>
                                        </div>
                                    </li>
                                    <li class="item-price__section price-usd">4.8 <span>$</span></li>
                                    <li class="item-price__section price-uah">129.6 <span>грн</span></li>
                                </ul>
                                <button class=item-btn-tocart>Добавить в корзину</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection