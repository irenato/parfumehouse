@extends('public.layouts.main')
@section('meta')
    <title>Оформление заказа</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    {{--<meta name="keywords" content="{!! $settings->meta_keywords !!}">--}}
@endsection
@section('content')

    <section class="checkout-main">
        <div class="container">
            <nav class="breadcrumbs">
                <a href="/" class="breadcrumbs__link">Главная</a> >
                <span class="breadcrumbs__link breadcrumbs__link_active">Оформление заказа</span>
            </nav>
            <a href="#" class="main-btn main-btn_accent" onclick="window.history.back()">Продолжить покупки</a>
        </div>

        <div class="container">
            <div class="checkout-title">
                <h1 class="section-title">Оформление заказа</h1>
            </div>
            @if(!isset($user))
                <span class="autorization-message">Вы не авторизованы! Авторизуйтесь или зарегистрируйтесь, чтобы воспользоваться преимуществами зарегистрированных покупателей <i class="autorization-message__close">&#xe806;</i></span>
            @endif

            <div class="row">
                <div class="col-md-6">
                    <form id="order_form" class="checkout-form">
                        <span class="error-message hidden" style="margin-bottom: 15px;">
                        </span>
                    {{ csrf_field() }}
                        <div class="checkout-step checkout-step_1" id="checkout-personal-info">
                            <div class="checkout-step__title clearfix">
                                <span class="checkout-step__number active">1</span>
                                <span class="checkout-step__name active">Личная информация</span>
                                <a href="javascript:void(0)" class="checkout-step__title-link hidden checkout-form__toggle-btn" id="edit-first-step" onclick="editFirstStep()">
                                    <i class="checkout-step__title-icon">&#xe815</i>Редактировать
                                </a>
                            </div>
                            <div class="checkout-step__main checkout-step_1 clearfix active">
                                @if(!isset($user))
                                    <ul class="checkout-form__registration">
                                        <li class="checkout-form__registration-item">
                                            <input type="radio" name="registration" id="with-registration" class="checkout-form__radio" value="on">
                                            <label for="with-registration" class="checkout-form__radio-label">Зарегистрировать меня автоматически</label>
                                        </li>
                                        <li class="checkout-form__registration-item">
                                            <input type="radio" name="registration" id="without-registration" class="checkout-form__radio" value="off" checked>
                                            <label for="without-registration" class="checkout-form__radio-label">Без регистрации</label>
                                        </li>
                                    </ul>
                                @endif
                                <div class="checkout-form__input-wrap">
                                    <label for="first_name" class="checkout-form__label">Имя*</label>
                                    <input type="text" class="checkout-form__input" name="first_name" value="{{ !empty($info['first_name']) ? $info['first_name'] : (isset($user) ? $user->first_name : old('first_name')) }}" id="first_name" required>
                                </div>
                                <div id="checkout-form__label-last_name" class="checkout-form__input-wrap checkout-form__input-wrap_right  hidden">
                                    <label for="last_name" class="checkout-form__label">Фамилия</label>
                                    <input type="text" class="checkout-form__input" value="{{ !empty($info['last_name']) ? $info['last_name'] : (isset($user) ? $user->last_name : old('last_name')) }}" name="last_name" id="surname">
                                </div>
                                <div id="checkout-form__label-email" class="checkout-form__input-wrap  hidden">
                                    <label for="email" class="checkout-form__label">E-mail*<!--<span style="font-size:10px;"> Отправим информацию о заказе</span>--></label>
                                    <input type="text" class="checkout-form__input" name="email" value="{{ !empty($info['email']) ? $info['email'] : (isset($user) ? $user->email : old('email')) }}" id="email">

                                </div>
                                <div class="checkout-form__input-wrap checkout-form__input-wrap_right">
                                    <label for="phone" class="checkout-form__label">Телефон*</label>
                                    <input type="text" class="checkout-form__input" name="phone" value="{{ isset($user) && isset($user->phone) ? $user->phone : (!empty($info['phone']) ? $info['phone'] : old('phone')) }}" id="phone" required>
                                </div>
                                @if(!isset($user))
                                    <div class="checkout-form__input-wrap checkout-form__input-wrap_password">
                                        <input name="password" type="password" class="checkout-form__password hidden" placeholder="Пароль*">
                                        <input name="password_confirmation" type="password" class="checkout-form__password checkout-form__password_right hidden" placeholder="Повторите пароль*">
                                    </div>
                                @endif
                                <div class="checkout-form__input-wrap">
                                    <span class="checkout-form__hint">*Поля обязательные для заполнения</span>
                                </div>

                                <a id="from_cart_to_order_button" href="javascript:void(0)" class="checkout-form__btn checkout-form__toggle-btn" onclick="saveFirstStep()">Далее</a>
                            </div>
                        </div>

                        <div class="checkout-step checkout-step_2" id="checkout-delivery-payment">
                            <div class="checkout-step__title clearfix">
                                <span class="checkout-step__number">2</span>
                                <span class="checkout-step__name">Доставка и оплата</span>
                            </div>

                            <div class="checkout-step__main clearfix">
                                <div class="checkout-form__select-wrap clearfix">
                                    <label for="delivery" class="checkout-form__select-label">Способ доставки</label>
                                    <select name="delivery" id="delivery" class="checkout-form__checkout-select checkout-form__delivery-select" onchange="deliveryChange($(this).val())">
                                        <option value="newpost"{{ !empty($info['delivery']) && $info['delivery'] == 'newpost' ? ' selected' : ''}}>Новая почта</option>
                                        <option value="courier"{{ !empty($info['delivery']) && $info['delivery'] == 'courier' ? ' selected' : ''}}>Доставка курьером по Харькову</option>
                                    </select>
                                </div>
                                <div id="newpost-wrap">
                                    <div class="checkout-form__select-wrap clearfix">
                                        <label for="region" class="checkout-form__select-label">Выберите область</label>
                                        <select name="region" id="region" class="checkout-form__checkout-select checkout-form__delivery-select choosen-select" onchange="newpostUpdate('city', $(this).val())">
                                            <option value="0">Выберите...</option>
                                            @forelse($regions as $region)
                                                <option value="{!! $region->id !!}"{{ !empty($info['region']) && $info['region'] == $region->id ? ' selected' : ''}}>{!! $region->name !!}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="checkout-form__select-wrap clearfix">
                                        <label for="city" class="checkout-form__select-label">Выберите город</label>
                                        <select name="city" id="city" class="checkout-form__checkout-select checkout-form__delivery-select choosen-select" onchange="newpostUpdate('warehouse', $(this).val())">
                                            @if(empty($cities))
                                            <option value="0">Сначала выберите область!</option>
                                            @else
                                                <option value="0">Выберите...</option>
                                                @foreach($cities as $city)
                                                    <option value="{!! $city->id !!}"{{ !empty($info['city']) && $info['city'] == $city->id ? ' selected' : ''}}>{!! $city->name_ru !!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="checkout-form__select-wrap clearfix">
                                        <label for="department" class="checkout-form__select-label">Выберите отделение</label>
                                        <select name="department" id="department" class="checkout-form__checkout-select checkout-form__delivery-select choosen-select">
                                            @if(empty($warehouses))
                                            <option value="0">Сначала выберите город!</option>
                                            @else
                                                <option value="0">Выберите...</option>
                                                @foreach($warehouses as $warehouse)
                                                    <option value="{!! $warehouse->id !!}"{{ !empty($info['department']) && $info['department'] == $warehouse->id ? ' selected' : ''}}>{!! $warehouse->address_ru !!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div id="courier-wrap" class="hidden">
                                    <div class="checkout-form__select-wrap clearfix">
                                        <label for="street" class="checkout-form__select-label">Адрес доставки</label>
                                        <div class="checkout-form__adrress-wrap">
                                            <input type="text" class="checkout-form__delivery-input checkout-form__delivery-input_big"
                                                   name="street" id="street" placeholder="Улица" value="{{ !empty($info['street']) ? $info['street'] : '' }}">
                                            <input type="text" class="checkout-form__delivery-input checkout-form__delivery-input_small"
                                                   name="house" id="house" placeholder="№ дома" value="{{ !empty($info['house']) ? $info['house'] : '' }}">
                                            <input type="text" class="checkout-form__delivery-input checkout-form__delivery-input_small"
                                                   name="flat" id="flat" placeholder="№ квартиры" value="{{ !empty($info['flat']) ? $info['flat'] : '' }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="checkout-form__select-wrap clearfix">
                                    <label for="payment" class="checkout-form__select-label">Способ оплаты</label>
                                    <select name="payment" id="payment" class="checkout-form__checkout-select checkout-form__payment-select">
                                        <option value="card"{{ !empty($info['payment']) && $info['payment'] == 'card' ? ' selected' : ''}}>Онлайн-оплата картой</option>
                                        <option value="cash"{{ !empty($info['payment']) && $info['payment'] == 'cash' ? ' selected' : ''}}>Наложенный платеж</option>
                                    </select>
                                    <span class="checkout-form__del-info hidden">
                                        <strong>Внимание!</strong> При оплате наложенным платежом с вас взымается сумма за пересылку товара. Сумма варьируется в интервале — 30-40 грн.
                                    </span>
                                </div>

                                <div class="checkout-form__select-wrap clearfix">
                                    <label for="comment" class="checkout-form__select-label">Ваш комментарий</label>
                                    <textarea name="comment" id="comment" rows="4" class="checkout-form__comment">{{ old('comment') }}</textarea>
                                </div>

                                <button type="submit" href="#" class="checkout-form__btn checkout-form__btn-next-1">Оформить заказ</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="order_process" class="col-md-6">
                    <form class="cart-form">
                        <div class="cart-table-wrap checkout-cart-table-wrap">
                            <h4 class="checkout-cart-table-wrap__title">Информация о заказе</h4>
                            <table id="cart-table" class="cart-table">

                            </table>
                        </div>
                        <div class="cart-result clearfix">
                            <a id="order_reload-btn" href="javascript:void(0)" class="cart-result__reload-btn" style="line-height:36px; text-align: center;">Обновить корзину</a>
                            <table class="cart-result__table">
                                <tr class="cart-result__table-row">
                                    <td class="cart-result__item">Сумма к оплате:</td>
                                    <td id="cart-result__item" class="cart-result__item cart-result__item_right">0 грн</td>
                                </tr>
                                <tr class="cart-result__table-row">
                                    <td class="cart-result__item">Доставка:</td>
                                    <td class="cart-result__item cart-result__item_right">0 грн</td>
                                </tr>
                                <tr class="cart-result__table-row">
                                    <td class="cart-result__item cart-result__item_accent">Итого:</td>
                                    <td id="cart-result__item_accent" class="cart-result__item cart-result__item_accent cart-result__item_right">840 грн</td>
                                </tr>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    @if(!empty($user) && strtotime($user->quest_time) < time() - 7884000)
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.css">
        <style>
            .swal2-modal .jq-selectbox{
                display: none !important;
            }
        </style>

        <script>
            var steps = [
                {
                    text: '1. Оцените уровень качества и наличия товара',
                    inputOptions: {
                        'отлично':'отлично',
                        'хорошо':'хорошо',
                        'не доволен':'не доволен'
                    },
                    html:
                    '<div id="swal2-content" class="swal2-content" style="display: block;">1. Оцените уровень качества и наличия товара</div>' +
                    '<textarea class="swal2-textarea" id="swal2-textarea1" style="display: block; resize: none;" placeholder=""></textarea>',
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            resolve([
                                $('#swal2-textarea1').val(),
                                $('[name="swal2-radio"]:checked').val()
                            ])
                        })
                    }
                },
                {
                    text: '2. Оцените качество обслуживания (удобство заказа, компетентность менеджера)',
                    inputOptions: {
                        'отлично':'отлично',
                        'хорошо':'хорошо',
                        'не доволен':'не доволен'
                    },
                    html:
                    '<div id="swal2-content" class="swal2-content" style="display: block;">2. Оцените качество обслуживания (удобство заказа, компетентность менеджера)</div>' +
                    '<textarea class="swal2-textarea" id="swal2-textarea2" style="display: block; resize: none;" placeholder=""></textarea>',
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            resolve([
                                $('#swal2-textarea2').val(),
                                $('[name="swal2-radio"]:checked').val()
                            ])
                        })
                    }
                },
                {
                    text: '3. Оцените работу службы доставки',
                    inputOptions: {
                        'отлично':'отлично',
                        'хорошо':'хорошо',
                        'не доволен':'не доволен'
                    },
                    html:
                    '<div id="swal2-content" class="swal2-content" style="display: block;">3. Оцените работу службы доставки</div>' +
                    '<textarea class="swal2-textarea" id="swal2-textarea3" style="display: block; resize: none;" placeholder=""></textarea>',
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            resolve([
                                $('#swal2-textarea3').val(),
                                $('[name="swal2-radio"]:checked').val()
                            ])
                        })
                    }
                },
                {
                    text: '4. Оцените качество сервиса (решение возникающих проблем)',
                    inputOptions: {
                        'отлично':'отлично',
                        'хорошо':'хорошо',
                        'не доволен':'не доволен'
                    },
                    html:
                    '<div id="swal2-content" class="swal2-content" style="display: block;">4. Оцените качество сервиса (решение возникающих проблем)</div>' +
                    '<textarea class="swal2-textarea" id="swal2-textarea4" style="display: block; resize: none;" placeholder=""></textarea>',
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            resolve([
                                $('#swal2-textarea4').val(),
                                $('[name="swal2-radio"]:checked').val()
                            ])
                        })
                    }
                },
                {
                    text: '5. Оставьте комментарий по улучшению качества работы Компании',
                    input: 'textarea'
                }
            ];

            swal({
                type: 'question',
                text: 'Просим Вас оценить качество сотрудничества с компанией ParfumHouse',
                html: '<div id="swal2-content" class="swal2-content" style="display: block;">Просим Вас оценить качество сотрудничества с компанией ParfumHouse</div><div id="swal2-content" class="swal2-content" style="display: block;">Опрос займёт у Вас не более минуты</div>',
                showCancelButton: true,
                cancelButtonText: 'В следующий раз'
            }).then(function () {
                swal.setDefaults({
                    input: 'radio',
                    confirmButtonText: 'Дальше &rarr;',
                    showCancelButton: false,
                    animation: false,
                    allowOutsideClick: false,
                    progressSteps: ['1', '2', '3', '4', '5']
                });

                swal.queue(steps).then(function (result) {

                    var data = {
                        q1r: result[0][0],
                        q1c: result[0][1],
                        q2r: result[1][0],
                        q2c: result[1][1],
                        q3r: result[2][0],
                        q3c: result[2][1],
                        q4r: result[3][0],
                        q4c: result[3][1],
                        q5c: result[4]
                    };

                    $.post('/quest', data, function(){
                        swal.resetDefaults();
                        swal({
                            title: 'Благодарим за ответы!',
                            confirmButtonText: 'Оформить заказ',
                            showCancelButton: false
                        });
                    });

                }, function () {
                    swal.resetDefaults();
                });
            });

        </script>
    @endif
@endsection