@extends('public.layouts.main')
@section('meta')
    <title>{!! $content->meta_title !!}</title>
    <meta name="description" content="{!! $content->meta_description !!}">
    {{--<meta name="keywords" content="{!! $content->meta_keywords !!}">--}}
@endsection
<main class="main-container main-about">
    <section class=about>
        <div class=container>
            @section('breadcrumbs')
                {!! Breadcrumbs::render('html', $content) !!}
            @endsection

            @section('content')
                <span class=about-title>{!! $content->meta_title !!}</span>
                <div class=about-wrapper>
                    @if($content->thumbnail)
                        <img class="about-pic" src="/img/{{ $content->thumbnail }}" alt="{{ $content->meta_title }}">
                    @endif
                    <div class=about-text>
                        {!! html_entity_decode($content->content) !!}
                    </div>
                </div>
        </div>
    </section>
</main>

@endsection