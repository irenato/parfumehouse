@extends('public.layouts.main')
@section('meta')
    <title>{!! $category->meta_title or $category['meta_title'] !!}</title>
    <meta name="description" content="{!! $category->meta_description or '' !!}">
    {{--<meta name="keywords" content="{!! $category->meta_keywords or '' !!}">--}}
    {!! !empty($category->url_alias) ? '<link rel="canonical" href="https://parfumhouse.com.ua/categories/'.$category->url_alias.'"/>' : '' !!}
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('categories', $category) !!}
@endsection

@section('content')
    <div class="container">
        <main class="catalog-content">
            <h1 class="section-title">{!! $category->name or $category['name'] !!}</h1>
            <div class="row">
                @if(!$attributes->isEmpty())
                    <div class="col-sm-3">
                        <aside class="catalog-sidebar">
                            @if(!empty($filter))
                                <div class="choice clearfix">
                                    <h4 class="sidebar-title">Ваш выбор</h4>
                                    @foreach($attributes as $attribute)
                                        @foreach($attribute->values as $attribute_value)
                                            @if(isset($filter[$attribute->id]) && in_array($attribute_value->id, $filter[$attribute->id]))
                                                <span class="choice__result">{!! $attribute_value->name !!}
                                                    <i class="header-middle__search-icon choice__reset-icon choice__reset-icon_result"
                                                        onclick="removeFilter('{!! $attribute->id !!}', '{!! $attribute_value->id !!}')"
                                                    >&#xe806;</i>
                                                </span>
                                            @endif
                                        @endforeach
                                    @endforeach
                                    <a href="/categories/{!! $category->url_alias or $category['url_alias'] !!}" class="choice__reset">
                                        <i class="header-middle__search-icon choice__reset-icon">&#xe806;</i> <span>сбросить все фильтры</span>
                                    </a>
                                </div>
                            @endif
                            <form method="get" class="product-filter-form">
                                {!! csrf_field() !!}
                                @foreach($attributes as $key => $attribute)
									@if($attribute->name != 'Фото оригинала')
                                    <div class="product-filter">
                                        <h4 class="product-filter__title">
                                            <i class="header-middle__search-icon sidebar-title__icon">&#xe808;</i>{!! $attribute->name !!}
                                        </h4>
                                        <ul class="product-filter__list">
                                             <?php
                                                if($attribute->name == 'Бренд'){
                                                    $values = $attribute->values->sortBy('name');
                                                }elseif($attribute->name == 'Производитель'){
                                                    $values = $attribute->values->sortBy(function($val, $key){
                                                        $orders = array(
                                                            1 => 2,
                                                            2 => 5,
                                                            3 => 3,
                                                            124 => 1,
                                                            133 => 4
                                                        );

                                                        return isset($orders[$val->id]) ? $orders[$val->id] : $val->id;
                                                    });
                                                }else{
                                                    $values = $attribute->values;
                                                }
                                            ?>
                                            @foreach($values  as $i => $attribute_value)
                                                <li class="product-filter__list-item">
                                                    <input type="checkbox"
                                                           name="filter_attributes[{!! $attribute->id !!}][value][{!! $attribute_value->id !!}]"
                                                           id="product-filter-{!! $key !!}__check-{!! $i !!}"
                                                           class="product-filter__checkbox"
                                                           @if(isset($filter[$attribute->id]) && in_array($attribute_value->id, $filter[$attribute->id]))
                                                                   checked
                                                           @endif
                                                           hidden>
                                                    <label for="product-filter-{!! $key !!}__check-{!! $i !!}" class="product-filter__check-label">{!! $attribute_value->name !!}</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
									@endif
                                @endforeach

                                @if(!$capacities->isEmpty())
                                    <div class="product-filter">
                                        <h4 class="product-filter__title">
                                            <i class="header-middle__search-icon sidebar-title__icon">&#xe808;</i>Объём
                                        </h4>
                                        <ul class="product-filter__list">
                                            @foreach($capacities as $c)
                                                <li class="product-filter__list-item">
                                                    <input type="checkbox"
                                                           name="capacity[]"
                                                           value="{{ $c->capacity }}"
                                                           id="capacity_{{ $c->capacity }}"
                                                           class="product-filter__checkbox"
                                                           @if(in_array($c->capacity, $capacity))
                                                           checked
                                                           @endif
                                                           hidden>
                                                    <label for="capacity_{{ $c->capacity }}" class="product-filter__check-label">{{ $c->capacity }}мл</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                        {{--<select name="capacity">--}}
                                            {{--<option value=""{{ empty($capacity) ? ' selected' : '' }}>Объём</option>--}}
                                            {{--@foreach($capacities as $c)--}}
                                                {{--<option value="{{ $c->capacity }}"{{ $capacity == $c->capacity ? ' selected' : '' }}>{{ $c->capacity }}мл</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    </div>
                                @endif

                                {{--<div class="btn-wrap clearfix">--}}
                                    {{--<button class="product-filter__filter-btn">Применить</button>--}}
                                {{--</div>--}}
                                <br>
                                <br>
                                <br>
                            </form>
                        </aside>
                    </div>
                @endif
                <div class="@if(!$attributes->isEmpty()) col-sm-9 @else col-sm-12 @endif">
                    <section class="cards">
                        <form class="sorting-form" method="get" @if(!empty($category['url_alias']) && $category['url_alias'] == 'new') style="display:none" @endif>
                            {{--<label for="sorting-form__select" class="sorting-form__label">Сортировка по </label>--}}
                            {{--<select name="sorting" onchange="sortBy($(this).val())">--}}
                                {{--@foreach($sort_array as $sort)--}}
                                    {{--<option value="{!! $sort['value'] !!}" @if($sort['value'] == $current_sort) selected @endif>{!! $sort['name'] !!}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            <label for="sorting-form__select" class="sorting-form__label">Отобржение </label>
                            <select name="viwe" onchange="setView($(this).val())">
                                <option value="tile" @if($view == 'tile') selected @endif>Плитка</option>
                                <option value="list" @if($view == 'list') selected @endif>Список</option>
                            </select>
                        </form>

						<div class="row" style="padding-top: 25px">
                            <div class="col-sm-6" style="padding: 0 5px;">
                                <a href="/categories/sale">
                                    <img src="/img/lof_ban.jpg" alt="LOF" style="border: 1px solid #DBC572; width: 100%">
                                </a>
                            </div>
                            <div class="col-sm-6" style="padding: 0 5px;">
                                <a href="/categories/parfumes?filter_attributes[1][value][573]">
                                    <img src="/img/ameli_ban.jpg" alt="Ameli" style="border: 1px solid #DBC572; width: 100%">
                                </a>
                            </div>
                        </div>
						
                        @forelse($products as $product)
                            @if($view == 'list')
                                @include('public.layouts.product-list', ['product' => $product, 'attributes' => $attributes])
                            @else
                                @include('public.layouts.product', ['product' => $product, 'attributes' => $attributes])
                            @endif
                        @empty
                            <article class="order">
                                <h5 class="order__title">В этой категории пока нет товаров!</h5>
                            </article>
                        @endforelse
                    </section>
                </div>

                {!! $paginator ? $products->links() : '' !!}
            </div>
        </main>
    </div>

@endsection