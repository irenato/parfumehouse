@extends('public.layouts.main')
@section('meta')
    <title>Блог</title>
    <meta name="description" content="{!! $settings->meta_description !!}">
    {{--<meta name="keywords" content="{!! $settings->meta_keywords !!}">--}}
@endsection

@section('content')

    <main class=main-container>
        <section class=news>
            <div class=product-wrapper>
                @section('breadcrumbs')
                    {!! Breadcrumbs::render('articles') !!}
                @endsection
                @if($articles !== null)
                    <span class=news-title>Статьи</span>
                    <div class="row news-wrapper">
                        @foreach($articles as $article)
                            <div class="news-item col-sm-4">
                                <img class=news-item-pic src="{!! $article->image->get_current_file_url('blog') !!}" alt="{{ $article->title }}">
                                <a href="/articles/{!! $article->url_alias !!}" class=news-item-btn>ЧТО НОВОГО</a>
                                <div class=news-item-date>{{ $article->date }}</div>
                                <div class=clearfix></div>
                                <div class=news-item-info><span class=news-item-title>{{ $article->title }}</span>
                                    <div class=news-item-txt>{{ strip_tags(html_entity_decode($article->text)) }}
                                    </div>
                                    <a class="news-item-link icon-arrow" href="/articles/{!! $article->url_alias !!}">ПЕРЕЙТИ</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
                @include('public.layouts.pagination', ['paginator' => $articles])
            </div>
        </section>
    </main>

@endsection