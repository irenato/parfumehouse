@extends('public.layouts.main')
@section('meta')
    <title>Спасибо за заказ</title>
@endsection
@section('content')
    {{--<main >--}}
        {{--<section class="content-404">--}}
            {{--<div class="container">--}}
                {{--<div class="content-404__img-wrap">--}}
                    {{--<img src="/assets/images/404.png" alt="" class="content-404__img">--}}
                {{--</div>--}}
                {{--<span class="content-404__slogan content-404__slogan_main">Спасибо за заказ!</span>--}}
                {{--<span class="content-404__slogan">Наш менеджер свяжется с вами в ближайшее время.</span>--}}
                {{--<a href="/" class="main-btn">Перейти на главную</a>--}}
            {{--</div>--}}
        {{--</section>--}}
    {{--</main>--}}
    <main >

        <section class="succes">
            <div class="container">
                <div class="succes__card">
                    <h1 class="succes__thx">Спасибо!</h1>
                    <span class="succes__descr">Ваш заказ успешно оформлен</span>
                </div>
                <span class="succes__wait-call">Ожидайте звонок от нашего менеджера в течении ближайших 5 минут</span>
                <a href="#" class="main-btn main-btn_accent" onclick="window.history.back()">Продолжить покупки</a>
            </div>
        </section>

        <section class="new-items">
            <div class="container">
                <h2 class="section-title">Новинки</h2>
                <div class="new-slider">
                    @forelse($latest_products as $product)
                        @include('public.layouts.product', $product)
                    @empty

                    @endforelse
                </div>
            </div>
        </section>

    </main>
@endsection