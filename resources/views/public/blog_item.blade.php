@extends('public.layouts.main')
@section('meta')
    <title>{!! $article->meta_title !!}</title>
    <meta name="description" content="{!! $article->meta_description !!}">
    {{--<meta name="keywords" content="{!! $article->meta_keywords !!}">--}}
@endsection

@section('content')

    <main class=main-container>
        <section class=news>
            <div class=product-wrapper>
                @section('breadcrumbs')
                    {!! Breadcrumbs::render('articles_item', $article) !!}
                @endsection
                <span class=news-title>{!! $article->title !!}</span>
                <div class="row news-wrapper">
                    <div class="news-item-main col-md-8">
                        <div class=news-item-main__title-wrap>
                            <h1 class=news-item-main__title>{!! $article->subtitle !!}</h1>
                        </div>
                        <img class=news-item-main__pic src="{!! $article->image->get_current_file_url('blog') !!}" alt="{!! $article->title !!}">
                        <article class=news-item-main__text>{!! html_entity_decode($article->text) !!}</article>
                    </div>

                    <div class="col-md-4 more-news">
                        @if($articles !== 'null')
                            <span class=more-news__title>Вам также может быть интересно:</span>
                            @foreach($articles as $article)
                                <div class="news-item more-news-item">
                                    <img class=news-item-pic src="{!! $article->image->get_current_file_url('blog') !!}" alt="{{ $article->title }}">
                                    <a href="/articles/{!! $article->url_alias !!}" class=news-item-btn>ЧТО НОВОГО</a>
                                    <div class=news-item-date>{{ $article->date }}</div>
                                    <div class=clearfix></div>
                                    <div class=news-item-info><span class=news-item-title>{{ $article->title }}</span>
                                        <div class=news-item-txt>{{ strip_tags(html_entity_decode($article->text)) }}
                                        </div>
                                        <a class="news-item-link icon-arrow" href="/articles/{!! $article->url_alias !!}">ПЕРЕЙТИ</a></div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection