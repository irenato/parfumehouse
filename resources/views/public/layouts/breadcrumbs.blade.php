@if ($breadcrumbs)
    <nav class="breadcrumbs">
        <ul class=breadcrumbs-list>
            @foreach ($breadcrumbs as $breadcrumb)
                @if (!$breadcrumb->last)
                    <li class=breadcrumbs-item>
                        <a class=breadcrumbs-link href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                    </li>
                    <li class="breadcrumbs-item separator"> ></li>
                @else
                    <li class=breadcrumbs-item>{{ $breadcrumb->title }}</li>
                @endif
            @endforeach
        </ul>
    </nav>
@endif
