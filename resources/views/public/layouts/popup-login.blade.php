<div class=mfp-hide>
    <div id=register-popup action="" class=register-popup>
        <div class=log-reg-wrapper>
            <div class=reg-wrapper>
                <div class=popup-form-title>Регистрация</div>
                <span class=popup-form-descr-bold>Создав учетную запись Вы сможете:</span>
                <ul class=popup-reg-list>
                    <li class=popup-reg-list__item><i class="popup-reg-list__icon icon-eye"></i> <span class=popup-reg-list__text>Отслеживать статус заказа</span></li>
                    <li class=popup-reg-list__item><i class="popup-reg-list__icon icon-doc"></i> <span class=popup-reg-list__text>Просматривать историю покупок</span></li>
                    <li class=popup-reg-list__item><i class="popup-reg-list__icon icon-letter"></i> <span class=popup-reg-list__text>Узнавать о новинках</span></li>
                </ul>
                <button class="popup-form-btn yellow reg-btn">ЗАРЕГИСТРИРОВАТЬСЯ</button>
            </div>
            <div class=log-wrapper>
                <form action="" class="popup-form pbz_form clear-styles" data-error-title="Ошибка отправки!" data-error-message="Попробуйте отправить заявку через некоторое время." data-success-title="Спасибо за заявку!"
                      data-success-message="Наш менеджер свяжется с вами в ближайшее время.">
                    <div class=popup-form-title>Вход</div>
                    <ul class="popup-form-fields reg-fields">
                        <li class="popup-form-composition reg-composition"><input type=email name=email class=popup-form-input placeholder=E-mail data-validate-required="Обязательное поле" data-title=email></li>
                        <li class="popup-form-composition reg-composition"><input type=password name=password class=popup-form-input placeholder="Ваш пароль" data-validate-required="Обязательное поле" data-title=пароль>
                        </li>
                        <li class="popup-form-composition reg-composition btn-composition">
                            <button class=popup-form-btn>ВОЙТИ</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <div class=regisration-wrapper>
            <div class=popup-form-title>Регистрация</div>
            <span class=popup-form-descr>Если Вы уже зарегистрированы, перейдите на страницу <a href="">Входа в систему</a></span>
            <ul class="popup-reg-list reverse">
                <li class=popup-reg-list__item><i class="popup-reg-list__icon icon-eye"></i> <span class=popup-reg-list__text>Отслеживать статус заказа</span></li>
                <li class=popup-reg-list__item><i class="popup-reg-list__icon icon-doc"></i> <span class=popup-reg-list__text>Просматривать историю покупок</span></li>
                <li class=popup-reg-list__item><i class="popup-reg-list__icon icon-letter"></i> <span class=popup-reg-list__text>Узнавать о новинках</span></li>
            </ul>
            <form action="" class="popup-form pbz_form clear-styles" data-error-title="Ошибка отправки!" data-error-message="Попробуйте отправить заявку через некоторое время." data-success-title="Спасибо за заявку!"
                  data-success-message="Наш менеджер свяжется с вами в ближайшее время.">
                <div class="row reg-form-row">
                    <div class="col-sm-6 reg-form-cell"><label class=reg-form-label for=name>Имя</label> <input class=reg-form-input type=text name=name data-validate-required="Обязательное поле"></div>
                    <div class="col-sm-6 reg-form-cell"><label class=reg-form-label for=last-name>Фамилия</label> <input class=reg-form-input type=text name=last-name data-validate-required="Обязательное поле"></div>
                </div>
                <div class="row reg-form-row">
                    <div class="col-sm-6 reg-form-cell"><label class=reg-form-label for=phone>Телефон</label> <input class=reg-form-input type=text name=phone data-validate-required="Обязательное поле"></div>
                    <div class="col-sm-6 reg-form-cell"><label class=reg-form-label for=email>E-mail</label> <input class=reg-form-input type=text name=email data-validate-required="Обязательное поле"></div>
                </div>
                <div class="row reg-form-row">
                    <div class="col-sm-6 reg-form-cell"><label class=reg-form-label for=password>Пароль</label> <input class=reg-form-input type=password name=password data-validate-required="Обязательное поле"></div>
                    <div class="col-sm-6 reg-form-cell"><label class=reg-form-label for=re-password>Повторите пароль</label> <input class=reg-form-input type=password name=re-password
                                                                                                                                    data-validate-required="Обязательное поле"></div>
                </div>
                <div class=agreement><input type=checkbox name=""> <span class=agreement-text>Я прочитал <a href="">Пользовательское соглашение</a> и согласен с ними</span></div>
                <div class="popup-form-composition btn-composition center">
                    <button class="popup-form-btn register-btn">ЗАРЕГИСТРИРОВАТЬСЯ</button>
                </div>
            </form>
        </div>
        <button title="Close (Esc)" type=button class=mfp-close>×</button>
    </div>
</div>