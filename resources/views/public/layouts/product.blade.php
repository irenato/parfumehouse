@if(!is_null($product))
    <div class="item">
        <div class="item-inner">
            <a href="/product/{{ $product->url_alias }}" class="item-logo-wrapper">
                <img class="item-logo" src="/assets/attributes_images/{{ $product->label }}.png"
                     alt="{{ $product->name }}">
            </a>
            <a href="/product/{{ $product->url_alias }}">
                <img class="item-pic" src="{{ $product->get_product_image->get_current_file_url('product_list') }}" alt="{{ $product->name }}"></a>
                {{--<img class="item-pic" src="/assets/images/A5N7BUdk6e_254x234.png" alt="{{ $product->name }}"></a>--}}
            <a class="item-title" href="">{{ $product->name }}</a>
            <a class="item-reviews" href="/product/{{ $product->url_alias }}"><span>{{ count($product->reviews) }}</span> отзывов</a>
            <ul class="item-price__wrapper">
                <li class="item-price__section item-vol">
                    <div class="volume-slider">
                        <div>{!! $product->capacity !!} {!! $product->capacity ? $product->measures->name : '' !!}</div>
                    </div>
                </li>
                <li class="item-price__section price-usd">{{ round($product->price, 2) }} <span>$</span></li>
                <li class="item-price__section price-uah">{{ round($product->price * $rate, 2) }} <span>грн</span></li>
            </ul>
            <button class="item-btn-tocart">Добавить в корзину</button>
        </div>
    </div>
@endif
