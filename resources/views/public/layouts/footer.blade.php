<footer class=main-footer id=contacts>
    <div class=wrapper>
        <div class=top-footer>
            <div class=footer-phones__wrapper><span class=footer-phones__title>Связаться с нами:</span>
                <ul class=footer-phones__list>
                    <li class=footer-phone><a class=footer-phone__link href="tel:+{{ preg_replace('/[^0-9]/', '', $settings->main_phone_1) }}"> {!! $settings->main_phone_1 !!}</a></li>
                    <li class=footer-phone><a class=footer-phone__link href="tel:+{{ preg_replace('/[^0-9]/', '', $settings->main_phone_2) }}"> {!! $settings->main_phone_2 !!}</a></li>
                </ul>
            </div>
            <div class=footer-subscribe__wrapper>
                <form class="subscribe-form footer-top__form">
                    {!! csrf_field() !!}
                    <input class="subscribe-form__input" type="email" name="email" placeholder="Ваш e-mail">
                    <button type="submit" class=subscribe-form__btn>Подписаться</button>
                </form>
            </div>
            <div class=footer-socials__wrapper><span class=footer-socials__title>Присоединяйтесь:</span>
                <ul class=footer-socials__list>

                    <li class=footer-socials><a href="{!! $socials->facebook or '' !!}" target=_blank class="footer-socials__link icon-facebook"></a></li>
                    <li class=footer-socials><a href="{!! $socials->instagram or '' !!}" target=_blank class="footer-socials__link icon-insta"></a></li>
                </ul>
            </div>
            <div class=footer-menu__wrapper>
                <ul class=footer-menu__list>
                    @forelse($categories as $category)
                        <li class=footer-menu__item><a href="/categories/{!! $category->url_alias !!}">{!! $category->name !!}</a></li>
                    @empty
                    @endforelse
                    @forelse($html_pages as $page)
                        <li class=footer-menu__item><a href="/page/{!! $page->url_alias !!}">{!! $page->name !!}</a></li>
                    @empty
                    @endforelse
                </ul>
            </div>
            <div class=footer-payment__wrapper><span class=footer-payment__title>Способы оплаты:</span>
                <ul class=footer-payment__list>
                    <li class=footer-payment__item><img
                                src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAYCAIAAAAZLxgkAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjUyMzA5MjIzRDNFQjExRTc4RUMwRUYwRUEyNkM1Rjc2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjUyMzA5MjI0RDNFQjExRTc4RUMwRUYwRUEyNkM1Rjc2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTIzMDkyMjFEM0VCMTFFNzhFQzBFRjBFQTI2QzVGNzYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTIzMDkyMjJEM0VCMTFFNzhFQzBFRjBFQTI2QzVGNzYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7PjpvzAAAEtElEQVR42sRWX2xTVRg/557T29u7tevWru3Y2OjW1W1sY8KMYMQxYChsgeCDYAwGo5A48MFIfPKZGA3xQRMTA8S9aIgPoAKLgMofR8SxlU3m/nfrRruyrl3//7l//dpFIIzn+t37cM93vpPfOb/v+37n4uNn/jp7fRrlxQiDu3c5MffWd2lRRvmysmIdQwlGeTTKYAbl3SjKyNk3byYodEujJSXkD9JSxOHpBwFFVbMjIBlThDGCEeRXkZCa3Qo4KAOGIUxWVEgGeCQ5+42yU5jAHMSrSFZVUVZWluf84MXgURT1MavgTSRiCCCJFscm2LkenPYxODsWbXtF625KUCQp9dya6xtbaq0x7lhvOffng1BceP2F8q6NtnBCvORa+HHQH4im9Zymra70/Z12VsMIonrxnu/yPX8iI3/U6XzJUfKoKWC/FPYLhuQ04qoEx8eah1dSQydNwqhm8VaqvYOl7P35yKcXRkO+2IZ1z88Gkt/e9MRS0u4NNm8ofeysq7ffS3RUQxlJVGJp6XBbJceSwdnQkW8Gk2kJpcSWyqLmtUVZ1nJcAiP0Mc3gwlrecfDE9cotyc8OaX+j4kKGrO2bCIa8UUO5YU+ztW8ylMhI0FdOm/7K8MNLNz3YoD26o3rfxrLxhThLMUuZeFo6P+BLRtNIp0EED81FwwnBVMg+YvfpJpGk9Kbq0nfuHu7xdvKxAV9YOHfHi1Tc1WJrqTYNeyKSIPNaWmnS+cIpJCsAz1HGWaY/vrv23W12gPQtp7+/4TGWFqyvMDBaOuAJh5MieaL7n4bMiEp7g8lSgN/r6+x3x0YXYiNjS7xJt6vJptWyU4txJCnVpXyhjrbXWyrtRjGUOvXDP3s/7zt9zZ3MSHCU3iF/wBttrjAc66gxG7m5ufByQngSZtUpZdVeWlBr5SWZfHLT8lP/PMpIm54z7WmxLgajy0kBYhrKDVCurdXFPR+8eKCjhujZ4dFA9+mBG2MBYPWrq9NYr91aZ35zS0WVmUcJETgX5Gx5/ycFqwzqvK3BfHsyeG1c1HseECO3s9FSWlL4+7AvmpQQg+vL9bCe58i2JuvLTvPG3smT50cjwdQf48HFqDDnj2OWXLjru+tedi8mEK8ZmFne17qG15BcAz0LUpDk9gbLF71T8aQYDcl1juL9rWug4mB9NCVBmdeXG25PBL/8Zeq1ZqvVyEFJJ0FMMIKa+vqaG+4LVZJHJoIjkoJ0FLHENRtJZeQCluQQnwUpyuqmdUazXguQSEu2Os2NVUVIUWYCiVhKBEiHteCiy3/xjvfq/UVQAcggSObB7XYNxX/PR7QaprvD8cbmCpCIC/3eU5cnB2dCyYyMDY+IxatRVYYwJ7rqXZ6InqMHNldkBAY6r9ZW/Habo5jXWIoK2+ptH+6X4dyJtFxWzHW2lL3aZPnZtXDolRqDjh7dbq8s4QFSQ1kBEf8yiDgB4cqlE+PokltVlVWgoEw4p2NQoeqKYoGH5JQJNCwrIjnlg0dFKsSsaOHKEghY0dBs4+fCBCknOBg8FIu/7kJyKn83CWehNHAbSfH8QfJrKSIIqXm8oAn6H/4KGMh9XgFViao1R/D0mbydULUf/leAAQD2iDkMUdCfTAAAAABJRU5ErkJggg==
                                alt=visa></li>
                    <li class=footer-payment__item><img
                                src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAAYCAYAAABnRtT+AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkNDNzFBMzM3RDNFQjExRTdCNTBCQjhCQjk2RjM3NDk3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkNDNzFBMzM4RDNFQjExRTdCNTBCQjhCQjk2RjM3NDk3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Q0M3MUEzMzVEM0VCMTFFN0I1MEJCOEJCOTZGMzc0OTciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Q0M3MUEzMzZEM0VCMTFFN0I1MEJCOEJCOTZGMzc0OTciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4TZtlhAAAHC0lEQVR42qSXC2wcVxWG/5nZp73e9dr7qt92SmwHByI7TomTqKlC3YSUNAppaROomqJKUCGUIiRCAVGBCA8hWokQCKhSqkpUqgK0NC2tSlCBiBCk1M2rie0kTmyvY3u9D+/Lu7Pz4J+doW1KAvZypU87Mztz73/POffccwU9tAK3aAJxkU+Q7WSQtFj/TZI/kZfJCZIlOuZF4OspYEMByBufgw9wG7mHbCNriYcoZIi8ajFiPbu5kFuI9JG95Cksrj1N9lPknCVSpMiP8tlBsn4R3yfIw+SNm4kVb2K9O0lqCQKN9gTtGOPvVlJPfkTOLFKg0erIUfIX0vjfLGkIfoz8EkttOlE5v366eU8aiGhAWjSfL70ZVr3LmuR/iHyQvFBRtyqhh9EjA2F6q14bxeZ8M9y6q0Khc6SPjH/Q3V3k2Yq6U2hBLy23OQ8so0CR97Lgp2UL5eCprAWsRekwbmzWxfdJVUVuNri9BNybM11uWjaABUHhOs5X1K/ZVpHPkucNd6+Crp8iplVF/miaFQyCyS1FWr40ZLj5jSbcOIHdmVfRK2+Frr3/riBa6/N/tbIGBrnos/HjbarDIWqeatgoTp/PQK/zlzuTskx/ubzZpySZglXVHNCYjEAntDMOH02Wu4PK5zbJsiYH8el3Q1e0glYjuqp8ZigU+a6SsWYhmaJ11RQumLPTNR2qrY5DiC5RSfdJ39GrDozesyU0f/gAqjeuxcnTV+H+zUFou3fCNTICtSBDCNZDKBYppIhSMAghEkLR4YS6+zOQusMQxi9AKYUg2CO85ntJO40aQjZmF2bqG2JjDT/wlFY9g3zLl+AqzUDMXYLubKAmpWwx1R4yHQgnZNGHa3ER6a6DkBoG4Uy8GbfNldCT6FyOntYw00YSZ3fsRO+yFjjzWZxr64T0xF7Ue1wIHfkdspcnkP/cA0g3NSH54lG0fe8rcJ67DP1JGdld98OxvAPBZw/j3ak0mj65E9PpPI5LTvejA+vwj2PPIZFKobmuEXroEIKRVoRKJzAy9Bo6+vdCzA9h6noKrqa7IScyaGztg3vuJSpXNtui9mpIzU0QFA1jyQVsum8AU7F5BLUSzg1PojU6C6xeCeWhB1AdCcBbW4OrPzmMscQC+mndM+cvQ933ZXS0hxGdiqPmW4/j1D/HEdm2GlMnR8VV9QFv+sob6Jt9hOMBbyUfhK2tHQtOCf6ePThx0ouO0DqcPNcMf4MDspZFRnfD4fXDfo07py7fLuZCQZTaWjB+dRZafT1CNS7MxdOQ4yn0r10Bj9uBYjYPibH5yrEzSCZzWNlYi/naOsjTc8j9/nU4ejpRTKQRK2gYnUhwUA/UaAzv7Psp2iPVUFLDsNvpKOk29HR1ojniRVFmRlAUuD1B5BJRTJ5/Gc1NDJd3vorzbx3ABEMXC2O0pMxQDgdha2nE2NkrqK5x49rrx9HicyOWk9G0dw8Kk9PIFkoIqzm0P/MzHD30ErRd29E6OFBeB1WchMhgV8Yn4fn1c5j+8SE0e50QRy5jRTyK6WQRvrYBILQeY1W7UFj+FOr0EfikeRQKBVTVBJBLzbBEGUUVJ+IJdWPDho2o4bWaidKS2iWxqil80eH3IjYVQ4Gr88rbw/B73Zi+MIYc71d8egN6elox5g2j+4dfw/aHB5GOZ3BxNlueXGrLII688Fc0rO9F777HUL2+H96gH7UU2Z2Yxs+ff0XW/WuAdX/ATO1ujE9OItC5BU1t3ZhPzqCznUXSwjjk1CX8+XQKdWu+i9beXXDkR6E2PMQ6LPCm8MeuLfuLoeA3HMkUPNUuZJiC4pEG3Dl1CUdaVqKhxgnv7AymRCdSwTCCTgGbLryNv8lOKFxwd0VPY9ixgKFlqxHyuRCYGMF1yYP788O0xvW5M59yBY7NrEc44EOHfQjvxgLw19YikSmgxTOLiWwAOz5yFnIujt9eXQdPTS2KRRl3hK9gZSTGrBIfEFQjmWv6KaZA0Uh/Ai80Xkh0paL/O+3q5TymlxM7/zNf5EIQINUxx30+Q8GMMVksu76cRo2052UWF3VRUc1+RUFn3+8ne6O7crYUzI1D1fT3cr4klh8xUQv1xo7jsAqLHRVti8YAPibuoGZuErq1WYSVWTyS9cJRcZFhtMfJL4y9m1sGvkk2L3mfFSxRSU57GS15R9EsWXP8o0uZgF3/+P8h8JRVX75XBV0kX6ioK0NoSTDrR6Hco8bKPI+2Up9VwFRaqt1HRj9cmb9IvlhRl06a6yKjZn8tTy7OGEXbUKrYgkbRu4lEjel/WKQRSb8iGyvq2qYbrt+KNcVOxufTN1REi29/Jx/7YFV+szOObsWBf4lnHOMgFiSvkXlO90lrsONLsN691vkqutjTYmVHWmMBfZtnuLU3HGkj1qKs+Ej7LwEGAIY3xCopqZE2AAAAAElFTkSuQmCC
                                alt=mastercard></li>
                </ul>
            </div>
            <div class=clearfix></div>
        </div>
        <div class=copyright-footer><a class=footer-logo__wrapper href=""><img src=/assets/static/png/logo-footer_b8c67e3b0de818a0eb766713e8dafcbb.png></a> <span class=copyright-info>© PARFUM HOUSE <span>Все права защищены</span></span>
            <div class=bz-logo><span class=bz-logo__title>Разработка сайта:</span> <a href=//zavodbiz.com/ target=_blank class=bz-logo__link>Первый бизнес завод</a></div>
        </div>
        <div class=clearfix></div>
    </div>
</footer>

<div class=mfp-hide>
    <div id=price-popup action="" class=price-popup>
        <div class=price-popup__top-title>Добро пожаловать на Parfum House</div>
        <img class=price-popup-bg src=/assets/static/jpg/price-popup-bg_7063c2bf91483561a71ff22d466347f3.jpg alt="">
        <form action="" class="price-popup-form popup-form pbz_form clear-styles" data-error-title="Ошибка отправки!" data-error-message="Попробуйте отправить заявку через некоторое время."
              data-success-title="Спасибо за заявку!" data-success-message="Наш менеджер свяжется с вами в ближайшее время.">
            <div class=price-popup-title>Скачайте прайс</div>
            <span class=price-popup-descr>с полным списком наших товаров</span>
            <ul class="popup-form-fields price-fields">
                <li class="popup-form-composition price-composition"><input type=text name=phone class=popup-form-input placeholder="Ваш телефон" data-validate-required="Обязательное поле"
                                                                            data-validate-uaphone="Неправильный номер" data-title=Телефон></li>
                <li class="popup-form-composition price-composition btn-composition">
                    <button class=popup-form-btn>СКАЧАТЬ ПРАЙС</button>
                </li>
            </ul>
        </form>
    </div>
</div>
<script type="text/javascript" src="/assets/js/vendors.bb69a3610cdf69c67b1e.js"></script>
<script type="text/javascript" src="/app.js"></script>