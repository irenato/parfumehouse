<nav class=pagination>
    @if ($paginator->lastPage() > 1)
        <ul class=pagination-list>
            @if($paginator->currentPage() != 1)
                <li class="pagination-item prev icon-arr">
                    <a href="{{ $paginator->url(1) }}"></a>
                </li>
            @endif
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                @if($paginator->currentPage() == $i)
                    <li class="pagination-item active">{{ $i }}</li>
                @else
                    <li class=pagination-item><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
                @endif
            @endfor
            @if($paginator->currentPage() != $paginator->lastPage())
                <li class="pagination-item next icon-arr"><a href="{{ $paginator->url($paginator->currentPage()+1) }}"></a></li>
            @endif
        </ul>
    @endif
</nav>