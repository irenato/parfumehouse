<header class=main-header>
    <button class="toggle-switch main-menu__btn"><span></span></button>
    <button class="search-btn search-btn__main icon-search"><span></span></button>
    <ul class="header-menu header-menu__left">
        @forelse($categories as $category)
            <li class="header-menu__item header-menu__item-left item1"><a href="/categories/{!! $category->url_alias !!}">/categories/{!! $category->name !!}</a></li>
        @empty
        @endforelse
    </ul>
    <div class=logo-wrapper>
        <a @if(url()->current() == url('/'))
           href="javascript:void(0)"
           @else
           href="/"
                @endif >
            <img class=logo-pic
                 src=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHcAAAA3CAYAAAA2YdXmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwgMjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjg2Nzg5QzNDRDNBRTExRTc5MDc3QTc0NDc3OEZBODI2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjg2Nzg5QzNERDNBRTExRTc5MDc3QTc0NDc3OEZBODI2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ODY3ODlDM0FEM0FFMTFFNzkwNzdBNzQ0Nzc4RkE4MjYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6ODY3ODlDM0JEM0FFMTFFNzkwNzdBNzQ0Nzc4RkE4MjYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7D+X7wAAAKG0lEQVR42uxcCZBVxRV9gwMqu4LlEhIVEhNBcS+D4kCp0RiDRAG3UoZFk1CiojKUBhSMWiIDBCWlgQIUjUY0AiOKqIiMuJQlKsEEF3BkRBKSYZTFDYHBc+qfLm61/f7AOMzy07fqVv//eu/TfZd+3S9vx44dSS5Rk8Li0xHMB++9m1lnVs0sGpBTY5HkHhXUAFhSz1wbiFwEN1IEN4IbKYLboGhLDfNty7WByM9BcB8AHwreZzfzzc61gcjLNVcoUiNcufBfO6i9m8jwSbfVYd2st7V4G+r+JIJbu3Q7uNDpRwz4KoT/BJeCn8eAv1+LYP4UwS/k+3YB/9iM1UzwgAjunm33z8R9Bcgy6dvpAPrzGgDaEsFgAXdsFMsNiwjIJPBoAHU3wmKA/OUugNocQRF4GLhtdIUaNu0HHgNeAeDOrQZYxq9Q+ra5NhCNxloGEO0RHABuKR3YGXwauBu4WZasXMUjsIq/MWUx/TjwtVnyMf1r4CWaANTxFPcVKGt9BLduQG+D4CLwkCy6kgD1AigblX6eJkaIqLvvA89i+rhyGwbIeQh6gyeAOwaSLAdfAv4buGsgvgx8A7gEoObEoOTl4Ptc7kzdAb4+EM3O5gWeTwSPBKhfR51bP6CN18qsBH8AfhU8H4B8nJK+t1bpvlmK/YqrGWWUpJTxIwS/Ap8CPgLcTit7eHSFapfay5Ainwy+nCsRALyAcCwG/AWbmIAh7kz8fCbJ7Cz5xJ2uc5Du1QCoZyC4EXxGYKW3j65QHUkeMAFcCECeBB/iAUzghqbkHeoDy/wsh+Wp3Lzo5zYM6kVLF+AUeM/7pKTv4wFbIEu5V9zEqHuaBr5Oxs8rXJiBNPSDFwCoXwqw06SnQ9Rb8YnSL1B+n6pU30TVPy0aVHvewDpEmxDkvQOGEjc3bqtmJdLfvVmbFb7htUUbIHdDfP87Wsv1AzKt2McDvusacAdPb673DCJ2nq/vfhjwifsB1A+iK1T/AHPX6WnwqVmSbQd3B78M3itLOorgcxv77lRj83PpAjUFl2HgtwTi+cLgLfBhKUXMRb7zkW4Ofv8mJc1q8PFI91mgfIp+7nxtRfyqaFDVLo1KMhv4/8NA3ws+wHN7CMjgLPnnemGIBvvAsh7Wx3pV/6hoLe854oYEXxIslb61AC+Sjxqi+V7o00Ll9/X5UtXXOrpCdUfcGizR6ztLUwNpPwFwFZoAFTKikmz5VG6J6ol+bj0Qj9kM8p49J+PJ0opq/m9XPkuDVH7cxKhH6uuJ5o0yjCz9p5r/qwPWcd/GPjCN6cUBz0OF3JMDA89Wev7sOi9+nVfWypRyN6a0I7pCkaJYjhTBjRTBjRTBjeBGiuBGiuBGiuBGiuBGiuD+P1NeXv9xk7xnPAw2r2pm0bvfmQmFxZwMj4GHI351IJ6nJew54a1J5nbcbPfKTel4GO1OLzuf8bTDPUj7FtLcgt/7g9/B/+kmL69ajtHfOYgr1fMfJJk7uZemzuTC4v6sB2mmpMS7OpcjzQwvjv1i/9YgboIXx3NavA56AfjwJHNDkG+eHkTat720tu8LEL/AxPFG/xD9/bM78aHDgLzl8PMkcybsI/AtjEdcD/w+31TBI0StwOv54iB0jXEsMvHa43jvOa9W8Lwv36pcHcjXIaW8YpR3oenI10n69UmelOBxGQLRiQCCp3tpXF4eYCvV76PBl6CeqahncQA4DirvED0PnpJS98XgI5PMpxFmeHF8S9RD9U0w5R6UZA7odffSnwUexvZwrMwV0h2m/RuSzJFaRwebOPZ7lcpfqjhHBHmqFs5xKWNZ3kSrK1FnLksyd3A4O8ah4C5ehiKFg3Vf1id7keou8E1J5ngKZ9JfNcCJd4uOYPI8MI+Y3gp+T8/dpw82ZZE89g1NZ4V3pKT9nSbf4VnK+2Z34tCfFgieFbA83/yAJgjfBb+oZL8F/8Xl8S6bZeubeyM1RMDy3Bivtxymcd0eKOMPWuEcx0n5GqA2En8Po8Gc2esE8Nngf6kjJyaZj2Y6ETrUiMcQuBQ5i5GP4oknGtpplvl3c0qRblItqJijFJ7CQ+aeuOOF7ZH627GG5X8ZGExOSnek9mLU+bipk0BPBl8FHsj/iH/JK7NqF+rtZCb7S/qKz1ipAr+M+xC/wf3JT+lElWS3PcJyg9HJ1AFDUQF13Be7OCh7mjqb32M8cUex5SRNB7S7Kdq9NUtZByFNT+9Z+8BgulMgT1lgnXRCGZR0FyaZmwwDCU4N+uVsGy6OR1DmIH7QJeUO8dGI3+zG3VrL+yPiGOkbd673Fc1CfpGtX5L5hN55Egntku8ecbHUHfkGGP1WIV3qEyfJYvEjtQAuB/9klPVrY4AVSdc5D+HQaso6W2LV8qmeSG5uRPwzoUIAAG8+LDI2QU1oqpEWxOAN1J12/IeT523xOAsudd4yWXyk+9G4JWbmE3Ce/X1TYpY0XB/gChGvctwvMchb6xekXG7uJEOlh7f6dt2fy9yjbaW/k139El0jpHbuNVmqE83/leFkudJL02w3dXWzmvRN94/PSnYeDSKwCzVpfeIti3JJ1y8sMB9KBKwVeHPMzL9Sabrpe09ORHFQ+T2KhwMV0UDrJZE0A418OaX9TDdPv7cE9HdTL31+YODcpKBEoTvDzyMcq3ZfIxviVum/pBqjytkLA7wJRMOvtxn0DXhWof7xQtm0FNfRrfj3vb7tEwB8r9CEQV2vo6yussqpLuj2sX2+rdLV6ly7culXnQkuBM82Mp0D1FK/WegxCh2NMMrd0kPG4Pqju3kXIBpyc8VWvH1kxLsF1F7RLPPALUcZFGF/0n9aqS3k/xKIzzwj5fvSLIV0wU4IxF9lpMSsQLt9vV5gJukaTg43tvqCTl+j84+ornH5yc7bbW0CM6+Z8aFGeSu0v1ZDV+nhkoCBxsG9XH4ZXaETA5sfTVPa9qjcCkqHR5GXviUvbN1jJM0bnj77UCFF8I3qU2Wy80wyRRavnfzkewBqbxTeLj3Iw3SL0MbRMuSaa3zc2NHvfsLke0wT/xz1i7ZGN6mQRJtImyU1pyAcqQ2NjmZBlqfYOV85FdXEiIbQ6ivUKqXFO5nAOOZKN2L0Nomglp6+qBK4n8sAm4t0rbw6WqQM4pNmMvWRCzVLA8kODET52z03qEz1blL7SBPN5wLdKcguNQC1rXEDXf+omymReMi9tSQGd/beNMBSHfXzrNtibUyQrtfvyRLV1K3DTFpa2ysxbpQ6rxubYEZgUVK9LZRKndPEGAyrA1tqBW5HRgNmgfsUwXjF8/dJ2nFx5W1WOs64K/SM8dfJ0gzWa10JTYzfq1OVMhg4y08yxp6zxEs1kI6oj5Z7htQSpVurNvj0nuLLAnHLFLfCa+cyTZaR8gYqNfiLpBd7+h8l02cLaUCOVhs5Lis1IXkRrdxs37Lv6zW5mO/v1ONmO3etZ/jRw/kH+VsBBgCYoazxk+r/IwAAAABJRU5ErkJggg==
                 alt="">
        </a>
    </div>
    <div class="cart-wrapper icon-basket">
        <div class=cart><span class=cart-items>0</span></div>
    </div>
    <button data-mfp-src=#register-popup class="login-btn icon-person popup-btn"></button>
    <div data-mfp-src=#price-popup class="price-btn popup-btn">СКАЧАТЬ ПРАЙС <span class=icon-dowl></span></div>
    <ul class="header-phones__list icon-phone">
        <li class=header-phones__item><a class=header-phones__link href=tel:+{{ preg_replace('/[^0-9]/', '', $settings->main_phone_1) }}> {!! $settings->main_phone_1 !!} </a></li>
        <li class="header-phones__item hidden-phone"><a class=header-phones__link href=tel:+{{ preg_replace('/[^0-9]/', '', $settings->main_phone_1) }}> {!! $settings->main_phone_1 !!}</a></li>
        <li class=header-phones__item><a class=header-phones__link href=tel:+{{ preg_replace('/[^0-9]/', '', $settings->main_phone_2) }}> {!! $settings->main_phone_2 !!}</a></li>
    </ul>
    <ul class="header-menu header-menu__right">
        <li class="header-menu__item header-menu__item-right item4"><a href="">ПАРТНЕРАМ</a></li>
        <li class="header-menu__item header-menu__item-right item5"><a href="">АКЦИИ</a></li>
    </ul>
</header>

<nav class=main-menu>
    <ul class=main-menu-list>
        @forelse($categories as $category)
            <li class="main-menu-list__item"><a class="main-menu-list__item-link" href="/categories/{!! $category->url_alias !!}">{!! $category->name !!}</a></li>
        @empty
        @endforelse
        @forelse($html_pages as $page)
            <li class="main-menu-list__item"><a class="main-menu-list__item-link" href="/page/{!! $page->url_alias !!}"> {!! $page->name !!} </a></li>
        @empty
        @endforelse
    </ul>
</nav>
{!! Form::open(['route' => 'search', 'class' => 'main-search']) !!}
<div class=search-wrapper>
    {!! Form::input('search', 'text', null, ['class'=>'search-field', 'placeholder' => 'Поиск по названию'] ) !!}
    <button type="submit" class="search-field-btn icon-search"></button>
</div>
{!! Form::close()!!}
<div class=main-cart><span class=main-cart__close>×</span>
    <div class=clearfix></div>
    <ul class=main-cart__orders-list>
        <li class=main-cart__order><span class=main-cart__del-order>×</span>
            <div class=main-cart__pic-wrapper><img class=main-cart__pic src=/assets/static/png/1_23846990b3d19d17d08ada659442465f.png alt=""></div>
            <div class=main-cart__order-info><span class=main-cart__order-type>Парфюмерия</span> <a class=main-cart__order-name href="">402 / Chanel Chance Tendre / Coco Chanel</a></div>
            <div class=main-cart__order-buy><span class=main-cart__order-vol> <div class=volume-slider> <div>100 мл</div> <div>200 мл</div> <div>300 мл</div> </div> </span>
                <div class=main-cart__order-price><span class=main-cart__order-usd>9,6 $</span> <span class=main-cart__order-uah>259.2 грн</span></div>
            </div>
        </li>
        <li class=main-cart__order><span class=main-cart__del-order>×</span>
            <div class=main-cart__pic-wrapper><img class=main-cart__pic src=/assets/static/png/2_12c10c9bc6f5b30e2955f527be3ca575.png alt=""></div>
            <div class=main-cart__order-info><span class=main-cart__order-type>Парфюмерия</span> <a class=main-cart__order-name href="">393 / See By Chloe / Chloe + флакон</a></div>
            <div class=main-cart__order-buy><span class=main-cart__order-vol> <div class=volume-slider> <div>100 мл</div> <div>200 мл</div> <div>300 мл</div> </div> </span>
                <div class=main-cart__order-price><span class=main-cart__order-usd>9,6 $</span> <span class=main-cart__order-uah>259.2 грн</span></div>
            </div>
        </li>
        <li class=main-cart__total>
            <div class=main-cart__total-info><span class=main-cart__total-text>Итого 2 товарa на сумму:</span>
                <div class=main-cart__total-price><span class=main-cart__total-usd>19,2 $</span> <span class=main-cart__total-uah>518.4 грн</span></div>
            </div>
            <a class=order-btn href="">ОФОРМИТЬ ЗАКАЗ</a></li>
    </ul>
    <div class=empty-cart><img src=/assets/static/png/empty_cart_6dd28b6c03271c08152a9c917527fbc8.png alt="Корзина пуста"> Ваша корзина пуста</div>
</div>