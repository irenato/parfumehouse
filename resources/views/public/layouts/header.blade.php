<!DOCTYPE html>
<html>
<head>
    <meta charset=UTF-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    @yield('meta')
    <link rel="shortcut icon"
          href=data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCYDF+gmAxfoJgMX6CYDEEAAAAAIJgMXiCYDF+gmAxDIJgMQyCYDF+gmAxeAAAAACCYDEEgmAxfoJgMX6CYDF+gmAx/4JgMf+CYDHaAAAAAIJgMQyCYDH8gmAx/4JgMSiCYDEogmAx/4JgMfyCYDEMAAAAAIJgMdqCYDH/gmAx/4JgMf+CYDHygmAxQgAAAACCYDFagmAx/4JgMf+CYDFUgmAxVIJgMf+CYDH/gmAxWgAAAACCYDFCgmAx8oJgMf+CYDFagmAxGAAAAACCYDESgmAx4IJgMf+CYDH/gmAxnoJgMZ6CYDH/gmAx/4JgMeCCYDESAAAAAIJgMRiCYDFaAAAAAAAAAACCYDEqgmAx0IJgMf+CYDHWgmAx/IJgMfSCYDH0gmAx/IJgMdaCYDH/gmAx0IJgMSoAAAAAAAAAAIJgMZKCYDG+gmAx/IJgMf+CYDHYgmAxHIJgMbCCYDH/gmAx/4JgMbCCYDEcgmAx2IJgMf+CYDH8gmAxvoJgMZKCYDH/gmAx/4JgMfiCYDGYgmAxEAAAAACCYDF+gmAx/4JgMf+CYDF+AAAAAIJgMRCCYDGYgmAx+IJgMf+CYDH/gmAxcoJgMVKCYDESAAAAAIJgMQSCYDGCgmAx/4JgMf+CYDH/gmAx/4JgMYKCYDEEAAAAAIJgMRKCYDFSgmAxcgAAAAAAAAAAgmAxCIJgMVaCYDHQgmAx/4JgMfyCYDFsgmAxbIJgMfyCYDH/gmAx0IJgMVaCYDEIAAAAAAAAAACCYDGugmAxxoJgMfSCYDH/gmAx/4JgMeCCYDFEAAAAAAAAAACCYDFEgmAx4IJgMf+CYDH/gmAx9IJgMcaCYDGugmAx/4JgMf+CYDH/gmAxzoJgMWyCYDEIAAAAAAAAAAAAAAAAAAAAAIJgMQiCYDFsgmAxzoJgMf+CYDH/gmAx/4JgMVSCYDFCgmAxFgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCYDEWgmAxQoJgMVQAAAAAAAAAAAAAAAAAAAAAgmAxHIJgMZ6CYDGegmAxnoJgMZ6CYDGegmAxnoJgMTgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIJgMUiCYDH/gmAx/4JgMf+CYDH/gmAx/4JgMf+CYDF4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCYDFIgmAx/4JgMf+CYDH/gmAx/4JgMf+CYDH/gmAxeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgmAxFIJgMX6CYDF+gmAxfoJgMX6CYDF+gmAxfoJgMSwAAAAAAAAAAAAAAAAAAAAA//8AABmYAAA5nAAA8A8AAOAHAAAEIAAADnAAAPgfAADxjwAAA8AAAA/wAAD//wAA+B8AAPgfAAD4HwAA//8AAA==>
    <link rel="shortcut icon" href=/assets/static/png/favicon_a4786da901c22be712617764f9b44197.png type=image/png>
    <link href="/assets/css/application.d719c6fa3eecb1304730.css" rel="stylesheet">
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MZM2JG');
    </script>
    <!-- End Google Tag Manager -->
    <script>
        window.min_price = parseInt('{{ 30 * $settings->rate }}');
    </script>
</head>