<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModuleBestsellerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('module_bestsellers')->insert([
            [
                'product_id'            => 1,
                'created_at'            => Carbon::now(),
                'updated_at'            => Carbon::now()
            ],
            [
                'product_id'            => 2,
                'created_at'            => Carbon::now(),
                'updated_at'            => Carbon::now()
            ],

        ]);
    }
}
