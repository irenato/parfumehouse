<?php

use Illuminate\Database\Seeder;

class AttributeValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attribute_values')->insert([
            array('id' => '1','attribute_id' => '1','name' => 'Reni','image_href' => 'RENI.png'),
            array('id' => '2','attribute_id' => '1','name' => 'LOF','image_href' => 'LOF.png'),
            array('id' => '3','attribute_id' => '1','name' => 'Refan','image_href' => 'REFAN.png'),
            array('id' => '4','attribute_id' => '2','name' => 'духи','image_href' => NULL),
            array('id' => '5','attribute_id' => '2','name' => 'флаконы','image_href' => NULL),
            array('id' => '6','attribute_id' => '2','name' => 'аксессуары','image_href' => NULL),
            array('id' => '7','attribute_id' => '3','name' => 'для женщин','image_href' => NULL),
            array('id' => '8','attribute_id' => '3','name' => 'для мужчин','image_href' => NULL),
            array('id' => '9','attribute_id' => '4','name' => 'ванильные','image_href' => 'Vanilnue.png'),
            array('id' => '10','attribute_id' => '4','name' => 'восточные','image_href' => 'Ambrovue_.png'),
            array('id' => '11','attribute_id' => '4','name' => 'альдегидные','image_href' => 'Aldegidnue.png'),
            array('id' => '12','attribute_id' => '4','name' => 'древесные','image_href' => 'Drevesnue.png'),
            array('id' => '13','attribute_id' => '4','name' => 'травянистые','image_href' => 'travyanistue.png'),
            array('id' => '14','attribute_id' => '4','name' => 'цветочные','image_href' => 'Cvetochnue.png'),
            array('id' => '15','attribute_id' => '4','name' => 'фруктовые','image_href' => 'Fryktovue.png'),
            array('id' => '16','attribute_id' => '4','name' => 'шипровые','image_href' => 'Shuprovue.png'),
            array('id' => '17','attribute_id' => '4','name' => 'пряные','image_href' => 'Pryanue.png'),
            array('id' => '18','attribute_id' => '4','name' => 'океанические','image_href' => 'Okeanicheskie.png'),
            array('id' => '19','attribute_id' => '4','name' => 'фужерные','image_href' => 'fygernue.png'),
            array('id' => '20','attribute_id' => '4','name' => 'мускусные','image_href' => 'Myskysnue.png'),
            array('id' => '21','attribute_id' => '4','name' => 'цитрусовые','image_href' => 'Cutrusovue.png'),
            array('id' => '22','attribute_id' => '4','name' => 'пудровые','image_href' => 'Pydrovue.png'),
            array('id' => '23','attribute_id' => '5','name' => 'Стекло','image_href' => NULL),
            array('id' => '24','attribute_id' => '5','name' => 'Пластик','image_href' => NULL),
            array('id' => '25','attribute_id' => '5','name' => 'Металл','image_href' => NULL)
        ]);
    }
}