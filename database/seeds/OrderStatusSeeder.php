<?php

use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Db::table('order_status')->insert([
            [
                'id' => 1,
                'status' => 'Новый'
            ],
            [
                'id' => 2,
                'status' => 'В обработке'
            ],
            [
                'id' => 3,
                'status' => 'Оплачен'
            ],
            [
                'id' => 4,
                'status' => 'Потерян'
            ],
            [
                'id' => 5,
                'status' => 'В пути'
            ],
            [
                'id' => 6,
                'status' => 'Закрыт'
            ],
        ]);
    }
}
