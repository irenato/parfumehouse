<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attributes')->insert([
            array('id' => '1','name' => 'Бренд','enable_image_overlay' => '1','image_overlay_settings' => 'a:5:{s:11:"coordinates";s:8:"left_top";s:13:"image_percent";s:3:"0.3";s:8:"offset_x";s:2:"10";s:8:"offset_y";i:60;s:12:"max_quantity";i:1;}','created_at' => '2016-11-01 17:34:53','updated_at' => '2016-11-01 17:34:53','deleted_at' => NULL),
            array('id' => '2','name' => 'Вид продукции','enable_image_overlay' => '0','image_overlay_settings' => NULL,'created_at' => '2016-11-01 17:34:53','updated_at' => '2016-11-01 17:34:53','deleted_at' => NULL),
            array('id' => '3','name' => 'Пол','enable_image_overlay' => '0','image_overlay_settings' => NULL,'created_at' => '2016-11-01 17:34:53','updated_at' => '2016-11-01 17:34:53','deleted_at' => NULL),
            array('id' => '4','name' => 'Группа ароматов','enable_image_overlay' => '1','image_overlay_settings' => 'a:5:{s:11:"coordinates";s:11:"left_bottom";s:13:"image_percent";s:3:"0.6";s:8:"offset_x";s:1:"0";s:8:"offset_y";s:2:"25";s:12:"max_quantity";s:1:"2";}','created_at' => '2016-11-01 17:34:53','updated_at' => '2016-11-02 12:52:31','deleted_at' => NULL),
            array('id' => '5','name' => 'Материал','enable_image_overlay' => '0','image_overlay_settings' => NULL,'created_at' => '2016-11-02 18:04:46','updated_at' => '2016-11-02 18:04:46','deleted_at' => NULL)

       ]);
    }
}
