<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class HTMLContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('html_content')->insert([
            array('id' => '1','name' => 'О нас','url_alias' => 'about','meta_title' => 'О нас','meta_keywords' => 'ключевые, слова','meta_description' => 'Мета описание','content' => '&lt;h1&gt;О нас&lt;/h1&gt;

&lt;p&gt;Компания Parfum House является официальным дистрибьютором&amp;nbsp;разливной парфюмерии торговых марок Reni, Refan и Library of Fragrances&amp;nbsp;в Украине уже&amp;nbsp;&lt;strong&gt;на протяжении 10 лет.&lt;/strong&gt;Компания следует&amp;nbsp;&lt;strong&gt;всем мировым тенденциям&lt;/strong&gt;&amp;nbsp;и на постоянной основе дополняет свой каталог&amp;nbsp;&lt;strong&gt;новинками&lt;/strong&gt;.&lt;/p&gt;

&lt;p&gt;Для наших клиентов компания предоставляет мужскую и женскую парфюмерию различных мировых брендов,&amp;nbsp;&lt;strong&gt;высшего класса&lt;/strong&gt;, по самым&lt;strong&gt;доступным ценам&lt;/strong&gt;, за счет того, что вся парфюмерия продается на разлив, по миллилитрам.&lt;/p&gt;

&lt;p&gt;Цель нашей компании заключается в том, что бы любой житель Украины смог насладиться идеальными ароматами элитной, всемирной известной парфюмерии по&amp;nbsp;&lt;strong&gt;доступным ценам&lt;/strong&gt;. Вся представленная парфюмерия неоднократно опробована нашими клиентами и получила&amp;nbsp;&lt;strong&gt;только положительные отзывы&lt;/strong&gt;.&lt;/p&gt;
','status' => '1','sort_order' => '1','created_at' => '2016-11-01 17:34:55','updated_at' => '2016-11-02 07:29:33','deleted_at' => NULL),
            array('id' => '2','name' => 'Оплата и доставка','url_alias' => 'payment','meta_title' => 'Оплата и доставка','meta_keywords' => 'ключевые, слова','meta_description' => 'Мета описание','content' => '&lt;h1&gt;Оплата и доставка&lt;/h1&gt;

&lt;p&gt;&amp;nbsp;&lt;/p&gt;

&lt;p&gt;Оплата&lt;/p&gt;

&lt;p&gt;&lt;strong&gt;Наличная&lt;/strong&gt;&lt;/p&gt;

&lt;ul&gt;
	&lt;li&gt;&amp;mdash; при доставке курьером;&lt;/li&gt;
	&lt;li&gt;&amp;mdash; наложенным платежом при получении заказа в представительстве службы доставки &amp;laquo;&lt;a href=&quot;http://www.novaposhta.com.ua/frontend/brunchoffices&quot; target=&quot;_blank&quot;&gt;Новая Почта&lt;/a&gt;&amp;raquo;, &amp;laquo;&lt;a href=&quot;https://meest-express.com.ua/ua/&quot; target=&quot;_blank&quot;&gt;Міст Експрес&lt;/a&gt;&amp;raquo;, Интайм&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;strong&gt;Безналичная&lt;/strong&gt;&lt;/p&gt;

&lt;ul&gt;
	&lt;li&gt;&amp;mdash; для физических лиц через Банк (при доставке по всей территории Украины). Вы можете оплатить высланный вам счет безналичным переводом в любом банке (при этом взимается банковская комиссия). Как только оплата поступает на наш расчетный счет (как правило, деньги поступают на следующий день), товар передается в службу доставки. Просьба после совершения оплаты информировать менеджеров;&lt;/li&gt;
	&lt;li&gt;&amp;mdash; для юридических лиц с оплатой по безналичному расчету оформление заказа с предоставлением пакета необходимых документов доступно при заказе от 500 грн.&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&amp;nbsp;&lt;/p&gt;

&lt;p&gt;&lt;strong&gt;Картами Visa, MasterCard&lt;/strong&gt;&lt;/p&gt;

&lt;p&gt;Наш интернет-магазин подключен к системе безопасных электронных платежей ПриватБанка, которая позволяет оплачивать наши товары и услуги с помощью любых карт Visa и MasterCard, выпущенных украинскими банками.&amp;nbsp;&lt;br /&gt;
&lt;br /&gt;
Поле &amp;laquo;Оплатить заказ&amp;raquo; картой VISA/MASTERCARD доступно сразу в Корзине заказа. Либо вы можете оплатить ваш заказ в Личном кабинете, если в момент заказа возникли проблемы с оплатой.&amp;nbsp;&lt;br /&gt;
&lt;br /&gt;
Вы можете оставаться спокойными за ваши платежи в Интернете благодаря современной технологии 3D Secure, которая является частью программы Visa &amp;quot;Verified by Visa&amp;quot; и MasterCard &amp;laquo;MAsterCardSecureCode&amp;raquo;.&amp;nbsp;&lt;br /&gt;
&lt;br /&gt;
Внимание! Получателем товара, оплаченного картой Visa/MasterCard, может быть только владелец карты.&amp;nbsp;&lt;br /&gt;
&amp;nbsp;&lt;/p&gt;

&lt;p&gt;Доставка&lt;/p&gt;

&lt;p&gt;&lt;strong&gt;Курьером до двери по всей Украине&lt;/strong&gt;&lt;/p&gt;

&lt;p&gt;Адресная доставка товаров осуществляется курьерами служб доставки &amp;laquo;Міст Експрес&amp;raquo;, &amp;laquo;Новая почта&amp;raquo; по всей территории Украины.&amp;nbsp;&lt;br /&gt;
&amp;nbsp;&lt;/p&gt;

&lt;p&gt;&lt;strong&gt;Самовывоз из нашего офиса в г.Харьков.&lt;/strong&gt;&lt;br /&gt;
&amp;nbsp;&lt;/p&gt;

&lt;p&gt;&lt;strong&gt;Доставка в отделения компаний перевозчиков.&lt;/strong&gt;&lt;/p&gt;

&lt;p&gt;Доставка по Украине через службу доставки &amp;laquo;Новая почта&amp;raquo;, &amp;laquo;Міст Експрес&amp;raquo;, Интайм.&amp;nbsp;&lt;br /&gt;
&lt;br /&gt;
Доставка оплачивается клиентом перевозчику по факту получения товара.&amp;nbsp;&lt;br /&gt;
&lt;br /&gt;
Перед отправкой заказанный Вами товар проверяют на качество и упаковку наши менеджеры, заполняют документы и отправляют Вам. &amp;nbsp;&lt;br /&gt;
&lt;br /&gt;
Проверку заказа на предмет целостности упаковки и отсутствия внешних повреждений товара мы рекомендуем осуществлять на месте &amp;mdash; в отделении службы доставки в Вашем городе. При наличии повреждения товара при перевозке требуйте от работников службы доставки составить акт с указанием характера повреждения товара и сообщите об этом нам!&lt;/p&gt;
','status' => '1','sort_order' => '2','created_at' => '2016-11-01 17:34:55','updated_at' => '2016-11-02 07:46:03','deleted_at' => NULL),
            array('id' => '3','name' => 'Контакты','url_alias' => 'contacts','meta_title' => 'Контакты','meta_keywords' => 'ключевые, слова','meta_description' => 'Мета описание','content' => '&lt;h1&gt;Контакты&lt;/h1&gt;','status' => '1','sort_order' => '3','created_at' => '2016-11-01 17:34:55','updated_at' => '2016-11-01 17:34:55','deleted_at' => NULL)
        ]);
    }
}
