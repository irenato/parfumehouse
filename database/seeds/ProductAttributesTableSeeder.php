<?php

use Illuminate\Database\Seeder;

class ProductAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_attributes')->insert([
            /**
             * pruduct_id = 1
             */
            [
                'product_id'            => 1,
                'attribute_id'          => 1, // бренд
                'attribute_value_id'    => 1
            ],
            [
                'product_id'            => 1,
                'attribute_id'          => 2, // вид продукции
                'attribute_value_id'    => 4
            ],
            [
                'product_id'            => 1,
                'attribute_id'          => 3, // пол
                'attribute_value_id'    => 7
            ],
            [
                'product_id'            => 1,
                'attribute_id'          => 4, // группа аромата
                'attribute_value_id'    => 14
            ],
            [
                'product_id'            => 1,
                'attribute_id'          => 4, // группа аромата
                'attribute_value_id'    => 11
            ],
            /**
             * pruduct_id = 2
             */
            [
                'product_id'            => 2,
                'attribute_id'          => 1, // бренд
                'attribute_value_id'    => 1
            ],
            [
                'product_id'            => 2,
                'attribute_id'          => 3, // вид продукции
                'attribute_value_id'    => 4
            ],
            [
                'product_id'            => 2,
                'attribute_id'          => 3, // пол
                'attribute_value_id'    => 7
            ],
            /**
             * pruduct_id = 3
             */
//            [
//                'product_id'            => 3,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 1
//            ],
//            [
//                'product_id'            => 3,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 3,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 8
//            ],
//            [
//                'product_id'            => 3,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 12
//            ],
//            [
//                'product_id'            => 3,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 10
//            ],
//            /**
//             * pruduct_id = 4
//             */
//            [
//                'product_id'            => 4,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 1
//            ],
//            [
//                'product_id'            => 4,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 4,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 8
//            ],
//            [
//                'product_id'            => 4,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 17
//            ],
//            [
//                'product_id'            => 4,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 12
//            ],
//            /**
//             * pruduct_id = 5
//             */
//            [
//                'product_id'            => 5,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 2
//            ],
//            [
//                'product_id'            => 5,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 5,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 7
//            ],
//            [
//                'product_id'            => 5,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 9
//            ],
//            [
//                'product_id'            => 5,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 10
//            ],
//            /**
//             * pruduct_id = 6
//             */
//            [
//                'product_id'            => 6,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 2
//            ],
//            [
//                'product_id'            => 6,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 6,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 7
//            ],
//            [
//                'product_id'            => 6,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 11
//            ],
//            [
//                'product_id'            => 6,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 12
//            ],
//            /**
//             * pruduct_id = 7
//             */
//            [
//                'product_id'            => 7,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 2
//            ],
//            [
//                'product_id'            => 7,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 7,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 8
//            ],
//            [
//                'product_id'            => 7,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 13
//            ],
//            [
//                'product_id'            => 7,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 14
//            ],
//            /**
//             * pruduct_id = 8
//             */
//            [
//                'product_id'            => 8,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 2
//            ],
//            [
//                'product_id'            => 8,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 8,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 8
//            ],
//            [
//                'product_id'            => 8,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 15
//            ],
//            [
//                'product_id'            => 8,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 16
//            ],
//            /**
//             * pruduct_id = 9
//             */
//            [
//                'product_id'            => 9,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 3
//            ],
//            [
//                'product_id'            => 9,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 9,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 7
//            ],
//            [
//                'product_id'            => 9,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 17
//            ],
//            [
//                'product_id'            => 9,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 18
//            ],
//            /**
//             * pruduct_id = 10
//             */
//            [
//                'product_id'            => 10,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 3
//            ],
//            [
//                'product_id'            => 10,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 10,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 7
//            ],
//            [
//                'product_id'            => 10,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 19
//            ],
//            [
//                'product_id'            => 10,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 20
//            ],
//            /**
//             * pruduct_id = 11
//             */
//            [
//                'product_id'            => 11,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 3
//            ],
//            [
//                'product_id'            => 11,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 11,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 8
//            ],
//            [
//                'product_id'            => 11,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 21
//            ],
//            [
//                'product_id'            => 11,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 22
//            ],
//            /**
//             * pruduct_id = 12
//             */
//            [
//                'product_id'            => 12,
//                'attribute_id'          => 1, // бренд
//                'attribute_value_id'    => 3
//            ],
//            [
//                'product_id'            => 12,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 4
//            ],
//            [
//                'product_id'            => 12,
//                'attribute_id'          => 3, // пол
//                'attribute_value_id'    => 8
//            ],
//            [
//                'product_id'            => 12,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 9
//            ],
//            [
//                'product_id'            => 12,
//                'attribute_id'          => 4, // группа аромата
//                'attribute_value_id'    => 10
//            ],
//            /**
//             * pruduct_id = 13
//             */
//            [
//                'product_id'            => 13,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 5
//            ],
//            [
//                'product_id'            => 13,
//                'attribute_id'          => 5, // материал
//                'attribute_value_id'    => 23
//            ],
//            [
//                'product_id'            => 13,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 26
//            ],
//            /**
//             * pruduct_id = 14
//             */
//            [
//                'product_id'            => 14,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 5
//            ],
//            [
//                'product_id'            => 14,
//                'attribute_id'          => 5, // материал
//                'attribute_value_id'    => 24
//            ],
//            [
//                'product_id'            => 14,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 27
//            ],
//            /**
//             * pruduct_id = 15
//             */
//            [
//                'product_id'            => 15,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 5
//            ],
//            [
//                'product_id'            => 15,
//                'attribute_id'          => 5, // материал
//                'attribute_value_id'    => 25
//            ],
//            [
//                'product_id'            => 15,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 28
//            ],
//            /**
//             * pruduct_id = 16
//             */
//            [
//                'product_id'            => 16,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 5
//            ],
//            [
//                'product_id'            => 16,
//                'attribute_id'          => 5, // материал
//                'attribute_value_id'    => 23
//            ],
//            [
//                'product_id'            => 16,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 26
//            ],
//            /**
//             * pruduct_id = 17
//             */
//            [
//                'product_id'            => 17,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 5
//            ],
//            [
//                'product_id'            => 17,
//                'attribute_id'          => 5, // материал
//                'attribute_value_id'    => 24
//            ],
//            [
//                'product_id'            => 17,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 27
//            ],
//            /**
//             * pruduct_id = 18
//             */
//            [
//                'product_id'            => 18,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 5
//            ],
//            [
//                'product_id'            => 18,
//                'attribute_id'          => 5, // материал
//                'attribute_value_id'    => 25
//            ],
//            [
//                'product_id'            => 18,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 28
//            ],
//            /**
//             * pruduct_id = 19
//             */
//            [
//                'product_id'            => 19,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 6
//            ],
//            [
//                'product_id'            => 19,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 28
//            ],
//            /**
//             * pruduct_id = 20
//             */
//            [
//                'product_id'            => 20,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 6
//            ],
//            [
//                'product_id'            => 20,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 27
//            ],
//            /**
//             * pruduct_id = 21
//             */
//            [
//                'product_id'            => 21,
//                'attribute_id'          => 2, // вид продукции
//                'attribute_value_id'    => 5
//            ],
//            [
//                'product_id'            => 21,
//                'attribute_id'          => 6, // цвет
//                'attribute_value_id'    => 26
//            ],
        ]);
    }
}
