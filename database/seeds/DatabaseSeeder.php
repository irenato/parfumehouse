<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SentinelUsersTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(ImageTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(AttributesTableSeeder::class);
        $this->call(AttributeValuesTableSeeder::class);
        $this->call(BlogTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(ProductAttributesTableSeeder::class);
        $this->call(ProductDescriptionTableSeeder::class);
        $this->call(UserDataSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(ModuleBestsellerTableSeeder::class);
        $this->call(ModuleRecommendedTableSeeder::class);
        $this->call(ModuleSlideshowTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(ProductsInOrderSeeder::class);
        $this->call(WishListSeeder::class);
        $this->call(ProductsReviewSeeder::class);
        $this->call(HTMLContentTableSeeder::class);
        $this->call(MeasuresTableSeeder::class);
        $this->call(OrderStatusSeeder::class);
    }
}
