<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name'              => 'Парфюмерия',
                'description'       => null,
                'meta_title'        => 'Парфюмерия',
                'meta_description'  => 'Парфюмерия',
                'meta_keywords'     => 'парфюм',
                'url_alias'         => 'parfumes',
                'parent_id'         => 0,
                'related_attribute_id' => 1,
                'sort_order'        => 1,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Флаконы',
                'description'       => null,
                'meta_title'        => 'Флаконы',
                'meta_description'  => 'Флаконы',
                'meta_keywords'     => 'Флаконы',
                'url_alias'         => 'bottle',
                'parent_id'         => 0,
                'related_attribute_id' => null,
                'sort_order'        => 2,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Аксессуары',
                'description'       => null,
                'meta_title'        => 'Аксессуары',
                'meta_description'  => 'Аксессуары',
                'meta_keywords'     => 'Аксессуары',
                'url_alias'         => 'accessories',
                'parent_id'         => 0,
                'related_attribute_id' => null,
                'sort_order'        => 3,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Другая продукция',
                'description'       => null,
                'meta_title'        => 'Другая продукция',
                'meta_description'  => 'Другая продукция',
                'meta_keywords'     => 'Другая продукция',
                'url_alias'         => 'other',
                'parent_id'         => 0,
                'related_attribute_id' => null,
                'sort_order'        => 4,
                'status'            => 1,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]
        ]);
    }
}
