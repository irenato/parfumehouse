<?php

use Illuminate\Database\Seeder;

class ProductCartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Db::table('products_cart')->insert([
            [
                'cart_id' => 1,
                'product_id' => 1,
                'product_quantity' => 5
            ],
            [
                'cart_id' => 1,
                'product_id' => 12,
                'product_quantity' => 3
            ],
            [
                'cart_id' => 1,
                'product_id' => 4,
                'product_quantity' => 1
            ],
            [
                'cart_id' => 1,
                'product_id' => 16,
                'product_quantity' => 2
            ],
            [
                'cart_id' => 1,
                'product_id' => 6,
                'product_quantity' => 4
            ],
        ]);
    }
}
