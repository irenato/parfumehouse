<?php

use Illuminate\Database\Seeder;

class ProductsInOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Db::table('products_in_order')->insert([
            [
                'order_id' => 1,
                'product_id' => 1,
                'product_quantity' => 2
            ],
            [
                'order_id' => 1,
                'product_id' => 2,
                'product_quantity' => 1
            ],
            [
                'order_id' => 2,
                'product_id' => 2,
                'product_quantity' => 4
            ],
            [
                'order_id' => 2,
                'product_id' => 1,
                'product_quantity' => 5
            ],
            [
                'order_id' => 3,
                'product_id' => 2,
                'product_quantity' => 21
            ],
        ]);
    }
}
