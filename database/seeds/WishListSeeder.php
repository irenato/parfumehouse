<?php

use Illuminate\Database\Seeder;

class WishListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Db::table('wish_list')->insert([
            [
                'user_id' => 3,
                'product_id' => 1,
            ],
            [
                'user_id' => 3,
                'product_id' => 2,
            ],
            [
                'user_id' => 4,
                'product_id' => 1,
            ],
            [
                'user_id' => 4,
                'product_id' => 2,
            ],
        ]);
    }
}
