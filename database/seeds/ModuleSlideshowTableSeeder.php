<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModuleSlideshowTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('module_slideshow')->insert([
            [
                'image_id'      => 2,
                'sort_order'    => 1,
                'status'        => 1,
                'link'          => 'categories/accessories',
                'enable_link'   => 1,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ],
            [
                'image_id'      => 3,
                'sort_order'    => 2,
                'status'        => 1,
                'link'          => 'categories/sale',
                'enable_link'   => 1,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ],
        ]);
    }
}
