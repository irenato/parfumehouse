<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            [
                'name'              => 'Слайдшоу на главной',
                'alias_name'        => 'slideshow',
                'status'            => 1,
                'settings'          => json_encode(['image_width' => 1170, 'image_height' => 383, 'quantity' => 6]),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Новинки товаров',
                'alias_name'        => 'latest',
                'status'            => 1,
                'settings'          => json_encode(['quantity' => 6]),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Лидеры продаж',
                'alias_name'        => 'bestsellers',
                'status'            => 1,
                'settings'          => json_encode(['quantity' => 6]),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Рекомендуемые товары',
                'alias_name'        => 'recommended',
                'status'            => 1,
                'settings'          => json_encode(['quantity' => 6]),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]
        ]);
    }
}
