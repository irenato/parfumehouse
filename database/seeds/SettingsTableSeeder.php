<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            array('id' => '1','meta_title' => 'Parfum House - официальный дистрибьютор разливной парфюмерии','meta_description' => 'Meta Description','meta_keywords' => '','about' => '&lt;h3&gt;О компании&lt;/h3&gt;

&lt;p&gt;Компания Parfum House является официальным дистрибьютором&amp;nbsp;разливной парфюмерии торговых марок Reni, Refan и Library of Fragrances&amp;nbsp;в Украине уже&amp;nbsp;&lt;strong&gt;на протяжении 10 лет.&lt;/strong&gt;Компания следует&amp;nbsp;&lt;strong&gt;всем мировым тенденциям&lt;/strong&gt;&amp;nbsp;и на постоянной основе дополняет свой каталог&amp;nbsp;&lt;strong&gt;новинками&lt;/strong&gt;.&lt;/p&gt;

&lt;p&gt;Для наших клиентов компания предоставляет мужскую и женскую парфюмерию различных мировых брендов,&amp;nbsp;&lt;strong&gt;высшего класса&lt;/strong&gt;, по самым&lt;strong&gt;доступным ценам&lt;/strong&gt;, за счет того, что вся парфюмерия продается на разлив, по миллилитрам.&lt;/p&gt;

&lt;p&gt;Цель нашей компании заключается в том, что бы любой житель Украины смог насладиться идеальными ароматами элитной, всемирной известной парфюмерии по&amp;nbsp;&lt;strong&gt;доступным ценам&lt;/strong&gt;. Вся представленная парфюмерия неоднократно опробована нашими клиентами и получила&amp;nbsp;&lt;strong&gt;только положительные отзывы&lt;/strong&gt;.&lt;/p&gt;
','terms' => '&lt;p&gt;Terms of use&lt;/p&gt;
','main_phone_1' => '+38 (067) 514-18-18','main_phone_2' => '+38 (050) 364-23-80','other_phones' => NULL,'notify_emails' => '["info@imsmedia.net.ua","client@imsmedia.net.ua"]','socials' => '{"facebook":"https:\\/\\/www.facebook.com\\/Parfum-House-\\u041f\\u0430\\u0440\\u0444\\u044e\\u043c\\u0435\\u0440\\u0438\\u044f-\\u043d\\u0430-\\u0440\\u0430\\u0437\\u043b\\u0438\\u0432-\\u043e\\u043f\\u0442\\u043e\\u043c-912249678907679\\/","vkontakte":"https:\\/\\/vk.com\\/parfum_house_kh","instagram":"https:\\/\\/www.instagram.com\\/parfumhouse.ua"}','product_list_image_width' => '254','product_list_image_height' => '301','product_image_width' => '490','product_image_height' => '570','blog_image_width' => '555','blog_image_height' => '182','slide_image_width' => '1170','slide_image_height' => '383')
        ]);
    }
}
