<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_description', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->text('product_description')->nullable();
            $table->integer('product_description_image_id')->nullable();
            $table->string('upper_note')->nullable();
            $table->string('heart_note')->nullable();
            $table->string('base_note')->nullable();
            $table->string('character')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_description');
    }
}
