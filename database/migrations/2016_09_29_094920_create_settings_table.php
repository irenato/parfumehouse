<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_keywords');
            $table->longText('about');
            $table->longText('terms');
            $table->string('main_phone_1');
            $table->string('main_phone_2');
            $table->text('other_phones')->nullable();
            $table->text('notify_emails')->nullable();
            $table->text('socials')->nullable();
            $table->integer('product_list_image_width');
            $table->integer('product_list_image_height');
            $table->integer('product_image_width');
            $table->integer('product_image_height');
            $table->integer('blog_image_width');
            $table->integer('blog_image_height');
            $table->integer('slide_image_width');
            $table->integer('slide_image_height');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
