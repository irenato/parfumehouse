<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleSlideshowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_slideshow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id');
            $table->string('link')->nullable();
            $table->tinyInteger('enable_link');
            $table->integer('sort_order');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_slideshow');
    }
}
