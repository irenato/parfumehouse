<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_data', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('image_id');
            $table->string('adress')->nullable();
            $table->string('company')->nullable();
            $table->string('other_data')->nullable();
            $table->tinyInteger('subscribe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_data');
    }
}
