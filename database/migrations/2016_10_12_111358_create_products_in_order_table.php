<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsInOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_in_order', function(Blueprint $table){
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('product_quantity');
            $table->integer('product_sum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_in_order');
    }
}
