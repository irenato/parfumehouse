<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('meta_title');
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('url_alias');
            $table->float('price');
            $table->string('articul');
            $table->integer('product_category_id');
            $table->integer('product_related_category_id')->nullable();
            $table->integer('original_image_id');
            $table->integer('image_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('measure_id');
            $table->tinyInteger('stock');
            $table->float('rating')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
